from graph import adjacencyList, edges, matrix, node

# for index, arr in enumerate(adjacencyList):
#     print(index, end=" || ")
#     print( '-'.join(arr) )

visited = [False for i in range(node)]

def dfs(node):
    visited[node] = True
    print(node)

    for neighbor in adjacencyList[node]:
        if not visited[neighbor]:
            dfs(neighbor)
    
    print(f'{node} completed')

# dfs(6)

def bfs(node):
    candidates = [ node ]
    visited[node] = True
    while len(candidates):
        n = candidates[0]
        candidates = candidates[1:]
        print(n, end=', ')

        for neighbor in adjacencyList[n]:
            if not visited[neighbor]:
                visited[neighbor] = True
                candidates.append(neighbor)

        print(f'{n} completed')


dfs(0)


# 0,1,2,3,4,8,5,6,7,9,10,11,12,%
# 0, 1, 2, 5, 3, 8, 6, 7, 4, 9, 10, 11, 12, %
