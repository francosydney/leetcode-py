# edges -> matrix / adjacency list
node = 13
edges = [(0, 1), (1, 2), (2, 3), (3, 4), (1, 5),
         (5, 6), (6, 7), (5, 7), (5, 8), (2, 8), (8, 9), (9, 10), (10, 11), (11, 12), (10, 12)]
matrix = [[0] * node for i in range(node)]

for x, y in edges:
    matrix[x][y] = 1
    matrix[y][x] = 1

# for row in matrix:
#     print(row)

adjacencyList = [[] for i in range(node)]
for x, y in edges:
    adjacencyList[x].append(y)
    adjacencyList[y].append(x)

# for index, arr in enumerate(adjacencyList):
#     print(index, end=" || ")
#     print( '-'.join(arr) )








