

class ListNode:
    def __init__(self, x):
        self.data = x
        self.next = None

def printList(l: ListNode):
    '''
    >>> a = ListNode(1)
    >>> a.next = ListNode(2)
    >>> a.next.next = ListNode(4)
    >>> printList(a)
    124
    '''

    arr = []
    p = l
    while p is not None:
        arr.append(str(p.val))
        p = p.next
    print(''.join(arr))

def makeList(arr):
    '''
    >>> a = makeList([1, 4, 5])
    >>> printList(a)
    145
    '''
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next

    return header.next




