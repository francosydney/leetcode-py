n = 5

for i in range(2**n, 2**(n + 1)):
    # generate bitmask, from 0..00 to 1..11
    bitmask = bin(i)[3:]
    print(bitmask)

# nth_bit = 1 << n
# for i in range(2**n):
#     # generate bitmask, from 0..00 to 1..11
#     bitmask = bin(i | nth_bit)[3:]
#     print(f'i={i}, i | nth_bit={i | nth_bit}, bitmask={bitmask}')