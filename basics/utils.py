class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

def printList(l: ListNode):
    '''
    >>> a = ListNode(1)
    >>> a.next = ListNode(2)
    >>> a.next.next = ListNode(4)
    >>> printList(a)
    124
    '''
    arr = []
    p = l
    while p is not None:
        arr.append(str(p.val))
        p = p.next
    print(''.join(arr))
