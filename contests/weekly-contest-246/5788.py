class Solution:
    def largestOddNumber(self, num: str) -> str:
        l = len(num) - 1
        while l >= 0:
            if int(num[l]) % 2:
                break
            l -= 1
        return '' if l < 0 else num[0:l+1]

# print(Solution.largestOddNumber(Solution, '4206'))
print(Solution.largestOddNumber(Solution, '52'))
