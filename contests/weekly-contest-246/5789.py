class Solution:
    def numberOfRounds(self, startTime: str, finishTime: str) -> int:
        def translate(time):
            hours, minutes = list(map(int, time.split(':')))
            return hours * 4 + minutes / 15

        def getPrevPoint(time):
            hours, minutes = list(map(int, time.split(':')))
            # print('@@@@', hours, minutes)
            if minutes < 15:
                minutes = 0
            else:
                minutes = (minutes // 15) * 15

            a = str(hours % 24).rjust(2, '0') + ':' + str(minutes).rjust(2, '0')
            # print('###', a)
            return a

        def getNextPoint(time):
            hours, minutes = list(map(int, time.split(':')))
            if minutes > 45:
                hours += 1
                minutes = 0
            else:
                minutes = (minutes // 15 + (1 if minutes % 15 else 0)) * 15
            
            return str(hours % 24).rjust(2, '0') + ':' + str(minutes).rjust(2, '0')
        timestamp1 = translate(getNextPoint(startTime))
        timestamp2 = translate(getPrevPoint(finishTime))
        return int(timestamp2 - timestamp1) % 96



print(Solution.numberOfRounds(Solution, '12:01', '12:44'))
print(Solution.numberOfRounds(Solution, '00:00', '23:59'))
print(Solution.numberOfRounds(Solution, '20:00', '06:00'))
