class Solution:
    def minDifference(self, nums, queries):
        def findSmallestDiff(nums):
            print('nums=', nums)
            nums.sort()
            sm = None
            for i in range(0, len(nums) - 1):
                diff = abs(nums[i+1] - nums[i])
                if diff > 0 and ((sm is None) or (diff < sm)):
                    sm = diff
            return sm if sm is not None else -1
        
        return list(map(lambda query: findSmallestDiff(nums[query[0]: query[1]+1]), queries))

    # return nums

# def 


print(Solution.minDifference(
    Solution, [4, 5, 2, 2, 7, 10], [[2, 3], [0, 2], [0, 5], [3, 5]]))
print(Solution.minDifference(
    Solution, [1, 3, 4, 8], [[0, 1], [1, 2], [2, 3], [0, 3]]))
