import math

class Solution:
    def areNumbersAscending(self, s: str) -> bool:
        lastNum = -math.inf
        currentNum = 0
        l = len(s)

        for i in range(l):
            if s[i].isdigit():
                currentNum = currentNum * 10 + int(s[i])
            
            if not s[i].isdigit() or i == l - 1:
                if currentNum:
                    # print(f'comparison', currentNum, lastNum)
                    if currentNum <= lastNum:
                        return False
                    lastNum = currentNum
                    currentNum = 0
        
        return True

print(Solution.areNumbersAscending(Solution, "1 box has 3 blue 4 red 6 green and 12 yellow marbles"))