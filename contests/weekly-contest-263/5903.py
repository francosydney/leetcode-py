class Bank:

    def __init__(self, balance):
        self.accounts = [0] + balance
        self.accountSize = len(balance)

    def transfer(self, account1: int, account2: int, money: int) -> bool:
        withdrawSuccess = self.withdraw(account1, money)
        if withdrawSuccess:
            if self.deposit(account2, money):
                return True
            else:
                self.deposit(account1, money)
                return False
        else:
            return False

    def deposit(self, account: int, money: int) -> bool:
        if account > self.accountSize:
            return False

        self.accounts[account] += money
        return True

    def withdraw(self, account: int, money: int) -> bool:
        if account > self.accountSize:
            return False

        if self.accounts[account] >= money:
            self.accounts[account] -= money
            return True
        else:
            return False


# Your Bank object will be instantiated and called as such:
obj = Bank([10,100,20,50,30])
print(obj.withdraw(3,10))
print(obj.transfer(2,111,20))
