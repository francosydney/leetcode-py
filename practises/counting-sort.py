from random import randint

def countsort(arr):
    '''
    >>> countsort([3,1,2,3,1,5,3,5,7,4,7,3,9,0,7,8,5,4,6,3,2,1,6,5,3,5])
    [0, 1, 1, 1, 2, 2, 3, 3, 3, 3, 3, 3, 4, 4, 5, 5, 5, 5, 5, 6, 6, 7, 7, 7, 8, 9]
    >>> countsort([2,3,1,4,0,5])
    [0, 1, 2, 3, 4, 5]
    '''
    # 3 step
    # ------
    # 0. create counting slots with min & max <- ignorable if range is givn (e.g. 0-9)
    # 1. mapping to counting slots
    # 2. aggregate the counting slots ("the last position of this value should be in the output array")
    # 3. place the array items onto the output

    l = len(arr)
    minVal = min(arr)
    maxVal = max(arr)
    slotCount = maxVal - minVal + 1

    count = [0] * slotCount
    output = [0] * l

    for num in arr:
        count[num - minVal] += 1
    for i in range(1, slotCount):
        count[i] = count[i - 1] + count[i]

    for i in range(l - 1, -1, -1):
        output[count[arr[i] - minVal] - 1] = arr[i]
        count[arr[i] - minVal] -= 1

    return output



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
