from random import randint

def partition(nums, pivot_index, left, right):
    pivot = nums[pivot_index]
    print('pivot picked:', pivot)
    nums[pivot_index], nums[right] = nums[right], nums[pivot_index]
    divide = left
    for i in range(left, right):
        if nums[i] < pivot:
            nums[i], nums[divide] = nums[divide], nums[i]
            divide += 1
    nums[right], nums[divide] = nums[divide], nums[right]
    return divide


def quicksort(arr, left, right):
    '''
    >>> quicksort([13,123,4,15,2,3452,35,234,51,234,123,4], 0, 11)
    121212
    '''
    if right <= left:
        return
    pivot_index = randint(left, right)
    
    sorted_pivot_index = partition(arr, pivot_index, left, right)
    quicksort(arr, left, sorted_pivot_index - 1)
    quicksort(arr, sorted_pivot_index + 1, right)
    return arr


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
