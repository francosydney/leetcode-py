from random import randint

def radixsort(arr):
    '''
    >>> radixsort([13,123,4,15,2,3452,35,234,51,234,123,4])
    [2, 4, 4, 13, 15, 35, 51, 123, 123, 234, 234, 3452]
    '''
    exp = 1
    l = len(arr)
    maxVal = max(arr)

    while maxVal // exp:
        count = [0] * 10
        output = [0] * l

        for num in arr:
            count[(num // exp) % 10] += 1
        for i in range(1, 10):
            count[i] = count[i] + count[i-1]
        for i in range(l-1, -1, -1):
            indexForVal = (arr[i] // exp) % 10
            output[count[indexForVal] - 1] = arr[i]
            count[indexForVal] -= 1
        for i in range(l):
            arr[i] = output[i]
        exp *= 10
    
    return arr



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
