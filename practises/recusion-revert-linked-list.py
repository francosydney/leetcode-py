class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

def printList(l: ListNode):
    arr = []
    p = l
    while p is not None:
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))

def makeList(arr):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def revert(node: ListNode):
    if node.next is None:
        return node
    rest = revert(node.next)
    
    node.next.next = node
    node.next = None

    return rest
    

l = makeList([4,5,3,8,9])
reverted = revert(l)
printList(reverted)