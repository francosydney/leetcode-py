
from types import *
from typing import List
from collections import deque

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


def buildTree(items, index=0):
    if index >= len(items) or items[index] is None:
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node


INDENT_SIZE = 4


def printTree(root, lv=0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)

def isSameTree(node1, node2):
    if not node1 and not node2:
        return True
    elif node1 and node2:
        return isSameTree(node1.left, node2.left) and isSameTree(node1.right, node2.right)
    else:
        return False


root = TreeNode(1)
root.left = TreeNode(2)
root.left.left = TreeNode(4)
root.right = TreeNode(3)
root.right.left = TreeNode(5)
root.right.right = TreeNode(6)
root.right.left.left = TreeNode(7)
root.right.left.right = TreeNode(8)

def serialise(root):
    result = []
    nodes = deque([root])
    while nodes:
        node = nodes.popleft()
        if node is None:
            result.append('#')
        else:
            result.append(str(node.val))
            nodes.append(node.left)
            nodes.append(node.right)
    return ','.join(result)

def deserialise(data):
    nums = data.split(',')
    if nums[0] == '#':
        return None

    root = TreeNode(nums[0])
    nodes = deque([root])
    
    i = 1
    while i < len(nums):
        parent = nodes.popleft() # 凡是进入了nodes的 就一定有两个子树(虽然其中可能有#)在serielised string中

        if nums[i] != '#':
            n = TreeNode(nums[i])
            nodes.append(n)
            parent.left = n
        if nums[i+1] != '#':
            n = TreeNode(nums[i + 1])
            nodes.append(n)
            parent.right = n
        i += 2
    return root


print(printTree(deserialise(serialise(root))))

# printTree(root)

# print(isSameTree(root, root.left))
