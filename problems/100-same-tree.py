class Solution:
    def isSameTree(self, p: TreeNode, q: TreeNode) -> bool:
        def isSame(p, q):
            if bool(p) != bool(q):
                return False
            if not p:
                return True
            if p.val != q.val:
                return False
            return isSame(p.left, q.left) and isSame(p.right, q.right)
        return isSame(p, q)
            