from collections import deque

class Solution:
    def levelOrder(self, root: TreeNode) -> List[List[int]]:
        if not root:
            return []

        nodes = deque([(root, 0)])
        result = []

        while nodes:
            levelSize = len(nodes)
            result.append([])
            for i in range(levelSize):
                node = nodes.popleft()
                result[-1].append(node.val)
                if node.left:
                    nodes.append(node.left)
                if node.right:
                    nodes.append(node.right)
        return result