from types import *
from typing import List
from collections import deque

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items) or items[index] is None:
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)



class Solution:
    def zigzagLevelOrder(self, root: TreeNode) -> List[List[int]]:
        '''
        >>> (Solution.zigzagLevelOrder(Solution, buildTree([1,2,3,4,None,None,5])))
        to_replace
        '''
        if not root:
            return []

        nodesByLevel = [[root]]
        zigZapNodes = []
        lv = 0

        while lv < len(nodesByLevel):
            nodes = []
            zigZapNodes.append(deque([]))
            for n in nodesByLevel[lv]:
                
                if lv % 2 == 1:
                    zigZapNodes[-1].appendleft(n.val)
                else:
                    zigZapNodes[-1].append(n.val)

                for child in filter(bool, [n.left, n.right]):
                    nodes.append(child)
            if nodes:
                nodesByLevel.append(nodes)
            lv += 1
        return zigZapNodes


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
