from types import *
from typing import List
from binarytree import Node as TreeNode

def buildTree(preorder: List[int], inorder: List[int]) -> TreeNode:
    l = len(preorder)
    if l == 0:
        return None
    rootVal = preorder[0]
    root = TreeNode(rootVal)
    leftSize = inorder.index(rootVal)
    
    root.left = buildTree(preorder[1: leftSize + 1], inorder[0: leftSize])
    root.right = buildTree(preorder[leftSize + 1: l], inorder[leftSize + 1: l])

    return root


class Solution:
    def buildTree(self, preorder: List[int], inorder: List[int]) -> TreeNode:
        '''
        >>> print(Solution.buildTree(Solution, [3,9,20,15,7], [9,3,15,20,7]))
        11111
        >>> print(Solution.buildTree(Solution, [3], [3]))
        11111
        '''
        return buildTree(preorder, inorder)


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
