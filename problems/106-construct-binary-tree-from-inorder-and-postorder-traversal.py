from types import *
from typing import List
from binarytree import Node as TreeNode



def buildTree(inorder: List[int], postorder: List[int], l1: int, h1: int, l2: int, h2: int) -> TreeNode:
    if l1 >= h1:
        return None
    rootVal = postorder[h2 - 1]
    root = TreeNode(rootVal)
    leftSize = inorder.index(rootVal) - l1
    root.left = buildTree(inorder, postorder, l1, l1 + leftSize, l2, l2 + leftSize)
    root.right = buildTree(inorder, postorder, l1 + leftSize + 1, h1, l2 + leftSize, h2 - 1)
    return root


class Solution:
    def buildTree(self, inorder: List[int], postorder: List[int]) -> TreeNode:
        '''
        >>> print(Solution.buildTree(Solution, [9,3,15,20,7], [9,15,7,20,3]))
        11111
        '''
        l = len(inorder)
        return buildTree(inorder, postorder, 0, l, 0, l)


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
