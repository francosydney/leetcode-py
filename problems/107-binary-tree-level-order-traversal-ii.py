from types import *
from typing import List
from collections import deque

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items):
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)



class Solution:
    def levelOrderBottom(self, root: TreeNode) -> List[List[int]]:
        '''
        >>> (Solution.levelOrderBottom(Solution, buildTree([3,9,20,None,None,15,7])))
        to_replace
        '''
        if not root:
            return []
        nodesByLevel = [[root]]
        result = deque([[root.val]])
        lv = 0
        while lv < len(nodesByLevel):
            nodes = []
            r = []
            for node in nodesByLevel[lv]:
                for n in filter(bool, [node.left, node.right]):
                    nodes.append(n)
                    r.append(n.val)
            if nodes:
                nodesByLevel.append(nodes)
                result.appendleft(r)
            lv += 1
        return result


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
