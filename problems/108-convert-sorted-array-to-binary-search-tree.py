from types import *
from typing import List

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items) or items[index] is None:
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)



class Solution:
    def sortedArrayToBST(self, nums: List[int]) -> TreeNode:
        '''
        >>> printTree(Solution.sortedArrayToBST(Solution, [-10,-3,0,5,9]))
        xxxxxxxxx
        '''
        def subListToTree(l, i, j):
            if i > j:
                return None
            if i == j:
                return TreeNode(l[i])
            mid = (i + j) // 2
            node = TreeNode(l[mid])
            node.left = subListToTree(l, i, mid - 1)
            node.right = subListToTree(l, mid + 1, j)

            return node

        return subListToTree(nums, 0, len(nums) - 1)


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
