from types import *
from typing import List

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items) or items[index] is None:
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
def makeList(arr: ListNode):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next


class Solution:
    def sortedListToBST(self, head: ListNode) -> TreeNode:
        '''
        >>> printTree(Solution.sortedListToBST(Solution, makeList([-10,-3,0,5,9])))
        xxxxxxxxx
        '''
        def getMiddleNode(root):
            slow = fast = root
            prev = None
            while fast and fast.next:
                fast = fast.next.next
                prev = slow
                slow = slow.next

            if prev:
                prev.next = None
            return slow
        
        def constructBST(curr):
            if not curr:
                return None
            if not curr.next:
                return TreeNode(curr.val)

            mid = getMiddleNode(curr)
            
            node = TreeNode(mid.val)
            node.left = constructBST(curr)
            node.right = constructBST(mid.next)
            return node
        
        return constructBST(head)


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
