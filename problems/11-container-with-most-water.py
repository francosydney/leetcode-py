from types import *
from typing import List

class Solution:
    def maxArea(self, height: List[int]) -> int:
        '''
        >>> Solution.maxArea(Solution, [1,8,6,2,5,4,8,3,7])
        49
        >>> Solution.maxArea(Solution, [1,1])
        1
        >>> Solution.maxArea(Solution, [4,3,2,1,4])
        16
        >>> Solution.maxArea(Solution, [1,2,1])
        2
        '''
        
        i = 0
        j = len(height) - 1
        maxVolume = 0
        while j > i:
            maxVolume = max( min(height[i], height[j]) * (j - i), maxVolume )
            if height[i] > height[j]:
                j -= 1
            else:
                i += 1
        return maxVolume


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
