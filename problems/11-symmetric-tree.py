class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items):
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)



class Solution:        
    def isSymmetric(self, root: TreeNode) -> bool:
        '''
        >>> Solution.isSymmetric(Solution, buildTree([1,2,2,3,4,4,3]))
        True
        >>> Solution.isSymmetric(Solution, buildTree([1,2,2,None,3,None,3]))
        False
        '''
        def isMirror(node1, node2):
            if not (node1 or node2):
                return True
            if bool(node1) != bool(node2):
                return False
            return (node1.val == node2.val) and isMirror(node1.left, node2.right) and isMirror(node1.right, node2.left)

        return isMirror(root, root)


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
