class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items):
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)



class Solution:
    def isBalanced(self, root: TreeNode) -> bool:
        '''
        >>> (Solution.isBalanced(Solution, buildTree([3,9,20,None,None,15,7])))
        True
        '''
        def getBalancedAndDepth(root):
            if not root:
                return (True, 0)
            leftTreeBalanced, leftTreeDepth = getBalancedAndDepth(root.left)
            rightTreeBalanced, rightTreeDepth = getBalancedAndDepth(root.right)
            
            if leftTreeBalanced and rightTreeBalanced and abs(leftTreeDepth-rightTreeDepth) <= 1:
                return (True, max(leftTreeDepth, rightTreeDepth) + 1)
            else:
                return (False, 0)
            
        return getBalancedAndDepth(root)[0]



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
