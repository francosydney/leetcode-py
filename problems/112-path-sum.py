class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items):
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)



class Solution:
    def hasPathSum(self, root: TreeNode, targetSum: int) -> bool:
        '''
        >>> Solution.hasPathSum(Solution, buildTree([1,2]), 3)
        True
        '''
        if not root:
            return False

        # printTree(root)

        def includePathEqualTo(root, target):
            # print('whether', root.val, '===~', target)
            if not (root.left or root.right):
                return root.val == target
            includedInLeft = includePathEqualTo(root.left, target - root.val) if root.left else False
            includedInRight = includePathEqualTo(root.right, target - root.val) if root.right else False
            return includedInLeft or includedInRight

        
        return includePathEqualTo(root, targetSum)



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
