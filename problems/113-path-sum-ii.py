class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items):
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)



class Solution:
    def pathSum(self, root: TreeNode, targetSum: int):
        '''
        >>> Solution.pathSum(Solution, buildTree([1,2,3]), 4)
        [[5,4,11,2],[5,8,4,5]]
        '''
        if not root:
            return []

        paths = []
        def countPath(root, target, path):
            newTarget = target - root.val
            if not (root.left or root.right):
                if newTarget == 0:
                    paths.append([*path, root.val])
                return

            path.append(root.val)
            if root.left:
                countPath(root.left, newTarget, path)
            if root.right:
                countPath(root.right, newTarget, path)
            path.pop()

        
        countPath(root, targetSum, [])

        return paths



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
