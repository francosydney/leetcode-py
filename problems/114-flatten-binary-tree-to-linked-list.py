from types import *
from typing import List
from binarytree import Node

def flatten(root: Node):
    if not root:
        return (None, None)
    flattenedLeftHead, flattenedLeftTail = flatten(root.left)
    flattenedRightHead, flattenedRightTail = flatten(root.right)
    
    root.left = None
    tail = root
    if flattenedLeftHead:
        tail.right = flattenedLeftHead
        tail = flattenedLeftTail
    if flattenedRightHead:
        tail.right = flattenedRightHead
        tail = flattenedRightTail
    return (root, tail)


class Solution:
    def flatten(self, root: Node) -> None:
        '''
        >>> root = Node(1)
        >>> root.left = Node(2)
        >>> root.left.left = Node(3)
        >>> root.left.right = Node(4)
        >>> root.right = Node(5)
        >>> root.right.right = Node(6)
        >>> Solution.flatten(Solution, root)
        >>> print(root)
        111
        '''
        flatten(root)


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
