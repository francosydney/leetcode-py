from types import *
from typing import List
from binarytree import Node


def connectNodes(node1: Node, node2: Node):
    if not node1 or not node2:
        return
    node1.next = node2
    connectNodes(node1.left, node1.right)
    connectNodes(node2.left, node2.right)
    connectNodes(node1.right, node2.left)


class Solution:
    def connect(self, root: 'Node') -> 'Node':
        '''
        >>> Solution.to_replace(Solution, to_replace)
        to_replace
        >>> Solution.to_replace(Solution, to_replace)
        to_replace
        >>> Solution.to_replace(Solution, to_replace)
        to_replace
        '''



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
