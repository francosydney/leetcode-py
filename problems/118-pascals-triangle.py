from types import *
from typing import List
from collections import defaultdict

class Solution:
    def generate(self, numRows: int) -> List[List[int]]:
        '''
        >>> Solution.generate(Solution, 5)
        to_replace
        '''
        rows = []
        rows.append([1])


        for i in range(1, numRows + 1):
            lastRow = rows[i - 1]
            row = [None] * i
            row[0] = row[-1] = 1
            for i in range(1, len(row) - 1):
                row[i] = lastRow[i - 1] + lastRow[i]
            rows.append(row)
    
        return rows[1:]

        # return [rows[i] for i in range(1, numRows + 1)]
        
        # return getRow(numRows)


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
