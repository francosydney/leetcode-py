from types import *
from typing import List

class Solution:
    def getRow(self, rowIndex: int) -> List[int]:
        '''
        >>> Solution.getRow(Solution, 3)
        [1, 3, 3, 1]
        >>> Solution.getRow(Solution, 2)
        [1, 2, 1]
        >>> Solution.getRow(Solution, 0)
        [1]
        '''
        lastRow = [1]
        for l in range(2, rowIndex + 2):
            row = [None] * l
            row[0] = row[-1] = 1
            for i in range(1, l - 1):
                row[i] = lastRow[i-1] + lastRow[i]
            lastRow = row
        return lastRow


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
