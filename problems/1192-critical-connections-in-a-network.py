from types import *
from typing import List
from collections import defaultdict

class Solution:
    def criticalConnections(self, n: int, connections: List[List[int]]) -> List[List[int]]:
        # for x, y in connec
        '''
        >>> Solution.criticalConnections(Solution, 4, [[0,1],[1,2],[2,0],[1,3]])
        [[1, 3]]
        >>> Solution.criticalConnections(Solution, 1, [])
        []
        >>> Solution.criticalConnections(Solution, 6, [[0, 1], [1, 2], [0, 2], [2, 3], [3, 4], [4, 5], [3, 5]])
        [[2, 3]]
        '''
        if n is 1:
            return []

        criticals = []
        graph = defaultdict(set)
        for node1, node2 in connections:
            graph[node1].add(node2)
            graph[node2].add(node1)
        
        nodeColors = [-1] * n
        nodeColorLocalArea = [-1] * n
        color = 1

        def dfs(node, parent):
            nonlocal color
            nodeColors[node] = nodeColorLocalArea[node] = color

            color += 1
            
            for neighbor in graph[node]:
                if neighbor is parent:
                    continue
                if nodeColors[neighbor] is -1:
                    dfs(neighbor, node)
                nodeColorLocalArea[node] = min(nodeColorLocalArea[node], nodeColorLocalArea[neighbor])
                if nodeColors[node] < nodeColorLocalArea[neighbor]:
                    criticals.append([node, neighbor])


        dfs(0, None)
        
        return criticals


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
