from types import *
from typing import List
import math

class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        '''
        >>> Solution.maxProfit(Solution, [7,1,5,3,6,4])
        5
        >>> Solution.maxProfit(Solution, [7,6,4,3,1])
        0
        '''
        maxBenefit = 0
        smallest = math.inf
        l = len(prices)

        for price in prices:
            maxBenefit = max(maxBenefit, price - smallest)
            smallest = min(smallest, price)

        return maxBenefit




if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
