from types import *
from typing import List

class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        '''
        >>> Solution.maxProfit(Solution, [7, 1, 5, 3, 6, 4])
        5
        >>> Solution.maxProfit(Solution, [7, 6, 4, 3, 1])
        0
        '''
        if len(prices) is 0:
            return 0

        min = prices[0]
        maxProfit = 0
        for price in prices:
            if price > min:
                maxProfit = max(price - min, maxProfit)
            else:
                min = price
        return maxProfit


if __name__ == "__main__":
    import doctest
    doctest.testmod()
