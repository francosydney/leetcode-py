from types import *
from typing import List

class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        '''
        >>> Solution.maxProfit(Solution, [7,1,5,3,6,4])
        7
        >>> Solution.maxProfit(Solution, [1,2,3,4,5])
        4
        >>> Solution.maxProfit(Solution, [7,6,4,3,1])
        0
        >>> Solution.maxProfit(Solution, [7,7,7,7,9])
        2
        '''
        benefit = 0
        i = 0
        buyPrice = None
        while i < len(prices) - 1:
            currentPrice, nextPrice = prices[i], prices[i + 1]
            if buyPrice is not None and nextPrice < currentPrice:
                benefit += currentPrice - buyPrice
                buyPrice = None
            elif buyPrice is None and nextPrice > currentPrice:
                buyPrice = currentPrice
            i += 1
        
        benefit += max(0, 0 if buyPrice is None else prices[i] - buyPrice)
        return benefit


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
