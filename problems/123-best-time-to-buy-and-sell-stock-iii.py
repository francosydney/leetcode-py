from types import *
from typing import List
import math

class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        '''
        >>> Solution.maxProfit(Solution, [1,2,3,4,5])
        4
        >>> Solution.maxProfit(Solution, [3,3,5,0,0,3,1,4])
        6
        >>> Solution.maxProfit(Solution, [7,6,4,3,1])
        0
        >>> Solution.maxProfit(Solution, [1])
        0
        '''
        l = len(prices)

        transaction1 = [0] * l
        transaction2 = [0] * (l + 1)

        minFromLeft = prices[0]
        maxFromRight = prices[-1]

        for i in range(1, l):
            transaction1[i] = max(transaction1[i-1], prices[i] - minFromLeft)
            minFromLeft = min(minFromLeft, prices[i])
            
            j = l - 1 - i
            transaction2[j] = max(transaction2[i+1], maxFromRight - prices[j])
            maxFromRight = max(maxFromRight, prices[j])

        max_profit = 0
        for i in range(l):
            max_profit = max(max_profit, transaction1[i] + transaction2[i+1])
            
        return max_profit

if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
