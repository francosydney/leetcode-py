class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items):
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)



class Solution:
    def maxPathSum(self, root: TreeNode) -> int:
        '''
        >>> (Solution.maxPathSum(Solution, buildTree([1,2,3])))
        6
        >>> (Solution.maxPathSum(Solution, buildTree([-3])))
        -3
        >>> (Solution.maxPathSum(Solution, buildTree([2,-1,0])))
        2
        '''
        maximum = float('-inf')
        def getLargestSumFrom(root):
            nonlocal maximum
            if not root:
                return 0
            largestSumFromLeft = max(getLargestSumFrom(root.left), 0)
            
            largestSumFromRight = max(getLargestSumFrom(root.right), 0)

            maxSumPathFromHere = root.val + largestSumFromLeft + largestSumFromRight
            maximum = max(maxSumPathFromHere, maximum)

            return max(largestSumFromLeft, largestSumFromRight) + root.val


        getLargestSumFrom(root)
        
        return maximum



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
