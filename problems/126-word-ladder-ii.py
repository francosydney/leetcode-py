from types import *
from typing import List
from collections import defaultdict, deque

class Solution:
    def findLadders(self, beginWord: str, endWord: str, wordList: List[str]) -> List[List[str]]:
        '''
        >>> Solution.findLadders(Solution, 'hit', 'cog', ["hot","dot","dog","lot","log","cog"])
        [["hit","hot","dot","dog","cog"],["hit","hot","lot","log","cog"]]
        >>> Solution.findLadders(Solution, 'hit', 'cog', ["hot","dot","dog","lot","log"])
        []
        '''

        # build up cache
        l = len(beginWord)
        adjacencyWordsOf = defaultdict(list)
        for word in wordList:
            for i in range(l):
                adjacencyWordsOf[ word[:i] + '*' + word[i+1:] ].append(word)

        def getAdjacencyWordsOf(word):
            words = set()
            for i in range(l):
                for adjacentWord in adjacencyWordsOf[word[:i] + '*' + word[i+1:]]:
                    words.add(adjacentWord)

            # words.remove(word)
            return words
        
        currentActive = 0
        visiteds = [set(), set()]
        queues = [deque([[beginWord]]), deque([[endWord]])]

        # [[beginWord]], {}, {}
        def BFSAgainstQueue(queue, visited, visitedByOtherSide):
            print('calling with', queue, visited, visitedByOtherSide)
            sequence = queue.popleft()
            lastWord = sequence[-1]

            neightWords = getAdjacencyWordsOf(lastWord)

            # n = len()

            # for word in neightWords:
            #     if word in visitedByOtherSide:
            #         return True
            #     if word not in visited:
            #         visited.add(word)
            #         queue.append( [*sequence, word] )


        for i in range(len(wordList)):
            nextActive = 1 - currentActive
            if BFSAgainstQueue(queues[currentActive], visiteds[currentActive], visiteds[nextActive]):
                return 'YYYY'
            
            if BFSAgainstQueue(queues[nextActive], visiteds[nextActive], visiteds[currentActive]):
                return 'NNNN'

        return []
        


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
