from types import *
from typing import List
from collections import deque, defaultdict

class Solution:
    def ladderLength(self, beginWord: str, endWord: str, wordList: List[str]) -> int:
        l = len(wordList[0])
        adjacency = defaultdict(set)
        for word in wordList:
            for i in range(l):
                adjacency[ word[:i] + '*' + word[i+1:] ].add(word)
        
        steps = 1
        nodes = deque([beginWord])
        visited = {beginWord}
        while nodes:
            steps += 1
            nodesLength = len(nodes)
            for i in range(nodesLength):
                word = nodes.popleft()

                for i in range(l):
                    for distance1word in adjacency[word[:i] + '*' + word[i+1:]]:
                        if distance1word == endWord:
                            return steps
                        if distance1word not in visited:
                            visited.add(distance1word)
                            nodes.append(distance1word)

        return 0


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
