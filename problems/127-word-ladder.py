from types import *
from typing import List
from collections import defaultdict, OrderedDict

class Solution:
    def ladderLength(self, beginWord: str, endWord: str, wordList: List[str]) -> int:
        """
        >>> Solution.ladderLength(Solution, "hit", "cog", ["hot","dot","dog","lot","log","cog"])
        5
        >>> Solution.ladderLength(Solution, "hit", "cog", ["hot","dot","dog","lot","log"])
        0
        """
        adjacentWords = defaultdict(list)

        if beginWord not in wordList:
            wordList.append(beginWord)
        if endWord not in wordList and beginWord != endWord:
            return 0

        dictionarySize = len(wordList)
        for word in wordList:
            for i in range(len(beginWord)):
                adjacentWords[word[:i] + "*" + word[i+1:]].append(word)

        # print(adjacentWords)

        depth = 0
        visited = set()
        candidates = OrderedDict()
        candidates[beginWord] = 0

        while candidates:
            current_word, depth = candidates.popitem(0)

            if current_word == endWord:
                return depth + 1
            visited.add(current_word)

            for i in range(len(current_word)):
                for word in adjacentWords[current_word[:i] + "*" + current_word[i+1:]]:
                    if word not in visited and word not in candidates:
                        candidates[word] = depth + 1
        
        return 0


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
