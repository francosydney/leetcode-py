from types import *
from typing import List

class Solution:
    def longestConsecutive(self, nums: List[int]) -> int:
        '''
        >>> Solution.longestConsecutive(Solution, [100,4,200,1,3,2])
        4
        >>> Solution.longestConsecutive(Solution, [0,3,7,2,5,8,4,6,0,1])
        9
        '''
        longestCount = 0
        num_bag = set(nums)

        for num in num_bag:
            if num - 1 not in num_bag:
                count = 1
                while num + 1 in num_bag:
                    count += 1
                    num += 1
                longestCount = max(longestCount, count)
        return longestCount

            # if num - 1 
            # while num + 1 in num_bag:
                
            


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
