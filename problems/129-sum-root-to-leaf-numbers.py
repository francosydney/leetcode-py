class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items):
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)



class Solution:
    def sumNumbers(self, root: TreeNode) -> int:
        '''
        >>> (Solution.sumNumbers(Solution, buildTree([4,9,0,5,1])))
        1026
        >>> (Solution.sumNumbers(Solution, buildTree([1,2,3])))
        25
        '''
        # printTree(root)
        total = 0
        def getSum(root, totalTilNow = 0):
            nonlocal total
            newTotal = root.val + totalTilNow * 10
            if not (root.left or root.right):
                total += newTotal
                return
            if root.left:
                getSum(root.left, newTotal)
            if root.right:
                getSum(root.right, newTotal)
            
        getSum(root)
        return total



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
