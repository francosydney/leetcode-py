from types import *
from typing import List
from collections import deque

class Solution:
    def solve(self, board: List[List[str]]) -> None:
        '''
        >>> Solution.solve(Solution, [["X","X","X","X"],["X","O","O","X"],["X","X","O","X"],["X","O","X","X"]])
        [['X', 'X', 'X', 'X'], ['X', 'X', 'X', 'X'], ['X', 'X', 'X', 'X'], ['X', 'O', 'X', 'X']]
        >>> Solution.solve(Solution, [["X","X","X","X"],["X","O","O","X"],["X","X","O","X"],["X","O","O","X"]])
        [['X', 'X', 'X', 'X'], ['X', 'O', 'O', 'X'], ['X', 'X', 'O', 'X'], ['X', 'O', 'O', 'X']]
        >>> Solution.solve(Solution, [["X","O","X","X"],["O","X","O","X"],["X","O","X","O"],["O","X","O","X"],["X","O","X","O"],["O","X","O","X"]])
        [['X', 'O', 'X', 'X'], ['O', 'X', 'X', 'X'], ['X', 'X', 'X', 'O'], ['O', 'X', 'X', 'X'], ['X', 'X', 'X', 'O'], ['O', 'X', 'O', 'X']]
        '''

        #1 find all regions that are connected to border
        #2 overwrite nodes not in above regions to "O"
        borderOs = set()
        
        r = len(board)
        c = len(board[0])

        if r < 3 or c < 3:
            return

        for i in range(0, r):
            if board[i][0] == 'O':
                borderOs.add((i, 0))
            if board[i][c-1] == 'O':
                borderOs.add((i, c-1))

        for i in range(0, c):
            if board[0][i] == 'O':
                borderOs.add((0, i))
            if board[r-1][i] == 'O':
                borderOs.add((r-1, i))


        deltas = [(0, -1), (0, 1), (1, 0), (-1, 0)]
        def inBound(pos):
            return 0 <= pos[0] < r and 0 <= pos[1] < c
        def neighborsOf(pos):
            return filter(inBound, [(pos[0]+d[0], pos[1]+d[1]) for d in deltas])

        nodesConnectedToBorderOs = set()
        def collectOsFrom(pos):
            if pos in nodesConnectedToBorderOs:
                return 
            
            nodesConnectedToBorderOs.add(pos)
            nodes = deque([pos])

            while nodes:
                node = nodes.popleft()
                for neighbor in neighborsOf(node):
                    if neighbor not in nodesConnectedToBorderOs:
                        if board[neighbor[0]][neighbor[1]] == 'O':
                            nodesConnectedToBorderOs.add(neighbor)
                            nodes.append(neighbor)

        for node in borderOs:
            collectOsFrom(node)

        
        # print(borderOs, '###')
        for i in range(1, r-1):
            for j in range(1, c-1):
                if (i,j) not in nodesConnectedToBorderOs:
                    board[i][j] = 'X'


        return board


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
