"""
# Definition for a Node.
class Node:
    def __init__(self, val = 0, neighbors = None):
        self.val = val
        self.neighbors = neighbors if neighbors is not None else []
"""
from collections import deque

class Node:
    def __init__(self, val=0, neighbors=None):
        self.val = val
        self.neighbors = neighbors if neighbors is not None else []

class Solution:
    def cloneGraph(self, node: 'Node') -> 'Node':
        if not node:
            return None

        converted = {node: Node(node.val)}
        nodes = deque([node])

        while nodes:
            n = nodes.popleft()
            converted_n = converted[n]
            for neighbor in n.neighbors:
                if neighbor not in converted:
                    converted_neighbor = Node(neighbor.val)
                    converted_n.neighbors.append(converted_neighbor)
                    
                    converted[neighbor] = converted_neighbor
                    nodes.append(neighbor)
                else:
                    converted_n.neighbors.append(converted[neighbor])
        
        return converted[node]

