from types import *
from typing import List

class Runner:
    def __init__(location, gas):
        self.location = location
        self.gas = gas
        self.arrival = False

class Solution:
    def canCompleteCircuit(self, gas: List[int], cost: List[int]) -> int:
        '''
        >>> Solution.canCompleteCircuit(Solution, [1,2,3,4,5], [3,4,5,1,2])
        3
        >>> Solution.canCompleteCircuit(Solution, [2,3,4], [3,4,3])
        -1
        >>> Solution.canCompleteCircuit(Solution, [4,5,2,6,5,3], [3,2,7,3,2,9])
        -1
        '''
        l = len(gas)
        for x in range(l):
            location = x
            gasLeft = gas[x]
            i = l
            # print('##### trying ', x)
            while i > 0:
                # print(f'gas left at {location} = {gasLeft}')
                nextLoc = (location + 1) % l
                if cost[location] > gasLeft:
                    break
                gasLeft = gasLeft - cost[location] + gas[nextLoc]

                location = nextLoc
                i -= 1

            if i <= 0:
                return x

        return -1



        


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
