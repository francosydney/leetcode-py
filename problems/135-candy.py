from types import *
from typing import List

class Solution:
    def candy(self, ratings: List[int]) -> int:
        '''
        >>> Solution.candy(Solution, [1,0,2])
        5
        >>> Solution.candy(Solution, [1,2,2])
        4
        >>> Solution.candy(Solution, [1, 2, 3, 4, 5, 3, 2, 1, 2, 6, 5, 4, 3, 3, 2, 1, 1, 3, 3, 3, 4, 2])
        47
        '''
        
        ups = 0
        downs = 0
        slope = 0 # 1: up | -1: down | 0: level
        candies = 0

        def calc(n):
            return int((n + 1) * n / 2)

        for i in range(1, len(ratings)):
            newSlope = 1 if ratings[i] > ratings[i-1] else (-1 if ratings[i] < ratings[i-1] else 0)

            if (slope != 0 and newSlope == 0) or (slope < 0 and newSlope > 0):
                candies += calc(ups) + calc(downs) + max(ups, downs)
                # print(f'getting candies with ups={ups} downs={downs}; now candies = {candies}')
                ups = 0
                downs = 0

            if ratings[i] > ratings[i-1]:
                ups += 1
            elif ratings[i] < ratings[i-1]:
                downs += 1
            else:
                candies += 1

            slope = newSlope

            # print(f'end of {ratings[i]}, slope={slope} ups={ups} downs={downs}')

        candies += calc(ups) + calc(downs) + max(ups, downs) + 1

        return  candies


                



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
