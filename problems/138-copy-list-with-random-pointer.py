from types import *
from typing import List

class Node:
    def __init__(self, x: int, next: 'Node' = None, random: 'Node' = None):
        self.val = int(x)
        self.next = next
        self.random = random

class Solution:
    def copyRandomList(self, head: 'Node') -> 'Node':
        if head is None:
            return None
        mapping = { None: None }
        p = head
        while p:
            mapping[p] = Node(0)
            p = p.next

        copyHead = mapping[head]
        p = head
        while p:
            cp = mapping[p]
            cp.val = p.val
            cp.next = mapping[p.next]
            cp.random = mapping[p.random]
            p = p.next
        
        return copyHead



if __name__ == "__main__":
    import doctest
    doctest.testmod()
