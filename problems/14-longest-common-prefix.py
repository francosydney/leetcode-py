from types import *
from typing import List

class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        '''
        >>> Solution.longestCommonPrefix(Solution, ["flower","flow","flight"])
        "fl"
        >>> Solution.longestCommonPrefix(Solution, ["dog","racecar","car"])
        ""
        '''
        i = 0
        
        for charsAtSamePos in zip(*strs):
            if len(set(charsAtSamePos)) == 1:
                i +=1 
            else:
                break
        
        return strs[0][:i]


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
