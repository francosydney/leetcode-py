from types import *
from typing import List
from binarytree import Node as TreeNode

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def makeList(arr):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def printList(l: ListNode):
    arr = []
    p = l
    while p is not None:
        # print('### appending', p.val)
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))


class Solution:
    def detectCycle(self, head: ListNode) -> ListNode:
        '''
        >>> printList(Solution.to_replace(Solution, makeList([])))
        to_replace
        '''
        slow = fast = head
        while fast and fast.next:
            fast = fast.next.next
            slow = slow.next

            if fast is slow:
                p = head
                while True:
                    if slow is p:
                        return p
                    else:
                        slow, p = slow.next, p.next

        return None


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
