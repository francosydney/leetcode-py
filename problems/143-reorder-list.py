from types import *
from typing import List
from binarytree import Node as TreeNode

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def makeList(arr):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def printList(l: ListNode):
    arr = []
    p = l
    while p is not None:
        # print('### appending', p.val)
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))


class Solution:
    def reorderList(self, head: ListNode) -> None:
        '''
        >>> (Solution.reorderList(Solution, makeList([1,2,3,4])))
        to_replace
        >>> (Solution.reorderList(Solution, makeList([1,2,3,4,5])))
        to_replace
        '''
        def reverse(l):
            prev = None
            curr = l
            while curr:
                nxt = curr.next
                curr.next = prev
                prev, curr = curr, nxt
            return prev

        def getMiddle(l):
            fast = slow = l
            while fast and fast.next:
                fast = fast.next.next
                slow = slow.next
            return slow

        mid = getMiddle(head)
        reversedSecondHalf = reverse(mid.next)
        mid.next = None

        # while reversedSecondHalf:
            # reversedSecondHalf length <= firstHalf
        printList(head)
        printList(reversedSecondHalf)




if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
