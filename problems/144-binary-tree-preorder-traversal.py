from types import *
from typing import List
from binarytree import Node as TreeNode

# Definition for singly-linked list.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

# def makeList(arr: ListNode):
#     header = p = ListNode(0)
#     for item in arr:
#         p.next = ListNode(item)
#         p = p.next
#     return header.next

# def printList(l: ListNode):
#     arr = []
#     p = l
#     while p is not None:
#         # print('### appending', p.val)
#         arr.append(str(p.val))
#         p = p.next
#     print(','.join(arr))


class Solution:
    def preorderTraversal(self, root: TreeNode) -> List[int]:
        '''
        >>> Solution.to_replace(Solution, to_replace)
        to_replace
        >>> Solution.to_replace(Solution, to_replace)
        to_replace
        >>> Solution.to_replace(Solution, to_replace)
        to_replace
        '''
        l = []
        def traverse(tree):
            if not tree:
                return
            l.append(tree.val)
            traverse(tree.left)
            traverse(tree.right)
        traverse(root)
        return l



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
