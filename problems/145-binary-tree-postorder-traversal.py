class Solution:
    def postorderTraversal(self, root: TreeNode) -> List[int]:
        
        def traverse(root, l):
            if not root:
                return
            traverse(root.left, l)
            traverse(root.right, l)
            l.append(root.val)

        l = []
        traverse(root, l)
        return l