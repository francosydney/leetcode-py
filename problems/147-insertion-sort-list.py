from types import *
from typing import List
from binarytree import Node as TreeNode

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def makeList(arr):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def printList(l: ListNode):
    arr = []
    p = l
    while p is not None:
        # print('### appending', p.val)
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))


class Solution:
    def insertionSortList(self, head: ListNode) -> ListNode:
        '''
        >>> printList(Solution.insertionSortList(Solution, makeList([4,2,1,3])))
        1,2,3,4
        >>> printList(Solution.insertionSortList(Solution, makeList([-1,5,3,4,0])))
        -1,0,3,4,5
        '''
        sortedList = ListNode(-1)
        nxt = None

        while head:
            nxt = head.next
            p = sortedList
            while p.next and p.next.val < head.val:
                p = p.next
            p.next, head.next = head, p.next
            
            head = nxt
        return sortedList.next





if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
