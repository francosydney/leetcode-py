from types import *
from typing import List
from binarytree import Node as TreeNode

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def makeList(arr):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def printList(l: ListNode):
    arr = []
    p = l
    while p is not None:
        # print('### appending', p.val)
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))


class Solution:
    def sortList(self, head: ListNode) -> ListNode:
        '''
        >>> printList(Solution.sortList(Solution, makeList([-1,5,3,4,0])))
        -1,0,3,4,5
        >>> printList(Solution.sortList(Solution, makeList([-1,5,4,0])))
        -1,0,4,5
        '''            
        def split(head):
            slowPrev = ListNode(-1)
            slowPrev.next = head
            fast = head
            while fast and fast.next:
                fast = fast.next.next
                slowPrev = slowPrev.next

            slow, slowPrev.next = slowPrev.next, None
            return head, slow
        
        def mergeTwoSortedList(l1, l2):
            l = p = ListNode(-1)
            while l1 and l2:
                if l1.val < l2.val:
                    p.next = l1
                    l1 = l1.next
                else:
                    p.next = l2
                    l2 = l2.next
                p = p.next
            if l1:
                p.next = l1
            if l2:
                p.next = l2
            return l.next
        
        def count(head):
            num = 0
            while head:
                num += 1
                head = head.next
            return num
        
        def mergeSort(head):
            if not head or not head.next:
                return head
            l1, l2 = split(head)
            sorted_l1, sorted_l2 = mergeSort(l1), mergeSort(l2)
            return mergeTwoSortedList(sorted_l1, sorted_l2)
            

        return mergeSort(head)




if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
