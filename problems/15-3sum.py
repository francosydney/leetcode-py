from types import *
from typing import List

class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        '''
        >>> Solution.threeSum(Solution, [-1, 0, 1, 2, -1, -4])
        [[-1, -1, 2], [-1, 0, 1]]
        >>> Solution.threeSum(Solution, [])
        []
        >>> Solution.threeSum(Solution, [1, 1, -2])
        [[-2, 1, 1]]
        >>> Solution.threeSum(Solution, [0,0,0,0])
        [[0, 0, 0]]
        >>> Solution.threeSum(Solution, [-2,0,1,1,2])
        [[-2, 0, 2], [-2, 1, 1]]
        '''

        results = set()
        nums.sort()
        l = len(nums)

        for i in range(l - 2):
            # print(f'i={i}', end=", ")
            if nums[i] > 0:
                break
            p1 = i + 1
            p2 = l - 1
            while p1 < p2:
                # print(f'p1={p1}, p2={p2}')
                if nums[p1] == nums[p1 - 1] and p1 > i + 1:
                    p1 += 1
                elif p2 < l - 1 and nums[p2] == nums[p2 + 1]:
                    p2 -= 1
                else:
                    sum_ = nums[i] + nums[p1] + nums[p2]
                    if sum_ is 0:
                        results.add((nums[i], nums[p1], nums[p2]))
                        p1 += 1
                        p2 -= 1
                    elif sum_ < 0:
                        p1 += 1
                    else:
                        p2 -= 1
        
        return [list(result) for result in results]


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
