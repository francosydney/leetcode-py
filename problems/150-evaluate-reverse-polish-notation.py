from types import *
from typing import List
from math import floor, ceil

class Solution:
    def evalRPN(self, tokens: List[str]) -> int:
        '''
        >>> Solution.evalRPN(Solution, ["10","6","9","3","+","-11","*","/","*","17","+","5","+"])
        22
        >>> Solution.evalRPN(Solution, ["2","1","+","3","*"])
        9
        >>> Solution.evalRPN(Solution, ["4","13","5","/","+"])
        6
        '''
        operators = {'+', '-', '*', '/'}

        def calc(num1, num2, operator):
            result = 0
            if operator == '+':
                result = num1 + num2
            elif operator == '-':
                result = num1 - num2
            elif operator == '*':
                result = num1 * num2
            else:
                result = num1 / num2

            result = floor(result) if result >= 0 else ceil(result)
            return result

        processed = []
        for token in tokens:
            if token in operators:
                num2AsStr, num1AsStr = processed.pop(), processed.pop()
                processed.append(calc(num1AsStr, num2AsStr, token))
            else:
                processed.append(int(token))    
        return int(processed[0])



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
