from types import *
from typing import List
from math import inf, prod

class Solution:
    def maxProduct(self, nums: List[int]) -> int:
        '''
        >>> Solution.maxProduct(Solution, [-2,0])
        0
        >>> Solution.maxProduct(Solution, [-2,0,-1])
        0
        >>> Solution.maxProduct(Solution, [0, -4, -2,0,-1])
        8
        >>> Solution.maxProduct(Solution, [-2,0,5,6,0,5,3,1,-4,-1,0,-4,-7,4,-3,-6,2,-9])
        9072
        >>> Solution.maxProduct(Solution, [2,3,-2,4])
        6
        '''
        if len(nums) == 1:
            return nums[0]

        exist0 = False
        subs = [[]]
        for num in nums:
            if num == 0:
                exist0 = True
                subs.append([])
            else:
                subs[-1].append(num)
        subs = list(filter(lambda l: len(l), subs))

        maxProduct = 0 if exist0 else -inf

        for sub in subs:
            if len(list(filter(lambda x: x < 0, sub))) % 2 == 0:
                maxProduct = max(maxProduct, prod(sub))
            else:
                firstNegativeIndex, lastNegativeIndex = 0, len(sub) - 1
                while sub[firstNegativeIndex] > 0:
                    firstNegativeIndex += 1
                if firstNegativeIndex < len(sub) - 1:
                    maxProduct = max(maxProduct, prod(sub[firstNegativeIndex + 1:]))
                                
                while sub[lastNegativeIndex] > 0:
                    lastNegativeIndex -= 1
                if lastNegativeIndex > 0:
                    maxProduct = max(maxProduct, prod(sub[:lastNegativeIndex]))


        return maxProduct




if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
