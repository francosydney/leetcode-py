from types import *
from typing import List

class Solution:
    def findMin(self, nums: List[int]) -> int:
        '''
        >>> Solution.findMin(Solution, [3,4,5,1,2])
        1
        >>> Solution.findMin(Solution, [4,5,6,7,0,1,2])
        0
        >>> Solution.findMin(Solution, [11,13,15,17])
        11
        '''
        left, right = 0, len(nums) - 1

        lastValue = nums[right]
        # find first num that is smaller than last
        while left < right:
            mid = left + (right - left) // 2
            if nums[mid] <= lastValue:
                right = mid
            else:
                left = mid + 1

        return nums[left] 




if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
