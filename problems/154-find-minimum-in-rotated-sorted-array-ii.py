from types import *
from typing import List

class Solution:
    def findMin(self, nums: List[int]) -> int:
        '''
        >>> Solution.findMin(Solution, [1,3,5])
        1
        >>> Solution.findMin(Solution, [2,2,2,0,1])
        0
        '''
        
        # find first position to be smaller than nums[right]
        left, right = 0, len(nums) - 1

        while left < right:
            mid = left + (right - left) // 2
            if nums[mid] == nums[right]:
                if nums[left] == nums[right]:
                    left = left + 1
                else:
                    right = mid
            elif nums[mid] < nums[right]:
                right = mid
            elif nums[mid] > nums[right]:
                left = mid + 1

        return nums[left]


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
