from types import *
from typing import List
import heapq
import math

class MinStack:

    def __init__(self):
        self.q = []
        self.smallest = math.inf

    def push(self, val: int) -> None:
        self.smallest = min(self.smallest, val)
        self.q.append((val, self.smallest))

    def pop(self) -> None:
        self.q.pop()
        self.smallest = self.q[-1][1]

    def top(self) -> int:
        return self.q[-1][0]
        

    def getMin(self) -> int:
        return self.q[-1][1]
        