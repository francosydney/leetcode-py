from types import *
from typing import List
from binarytree import Node as TreeNode

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def makeList(arr):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def printList(l: ListNode):
    arr = []
    p = l
    while p is not None:
        # print('### appending', p.val)
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))


class Solution:
    def getIntersectionNode(self, headA: ListNode, headB: ListNode) -> ListNode:
        '''
        >>> printList(Solution.getIntersectionNode(Solution, makeList([1,2,3,4]), makeList([4,5,6])))
        to_replace
        '''
        p1 = headA
        p2 = headB
        while p1 is not p2:
            print(p1.val if p1 else 'NA', p2.val if p2 else 'NA')
            p1 = p1.next if p1 else headB
            p2 = p2.next if p2 else headA
        return p1

        



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
