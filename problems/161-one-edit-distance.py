from types import *
from typing import List

class Solution:
    def isOneEditDistance(self, s: str, t: str) -> bool:
        '''
        >>> Solution.isOneEditDistance(Solution, "ab", "acb")
        True
        >>> Solution.isOneEditDistance(Solution, "", "")
        False
        >>> Solution.isOneEditDistance(Solution, "a", "")
        True
        '''
        l_s, l_t = len(s), len(t)
        if abs(l_s - l_t) > 1:
            return False
        
        if l_s + 1 == l_t:
            i = 0
            while i < min(l_s, l_t) and s[i] == t[i]:
                i += 1
            return s[i:] == t[i+1:]
        elif l_s - 1 == l_t:  # abcde abce
            i = 0
            while i < min(l_s, l_t) and s[i] == t[i]:
                i += 1
            return s[i+1:] == t[i:]
        else:
            diffCount = 0
            for ch_s, ch_t in zip(s, t):
                if ch_s != ch_t:
                    diffCount += 1
                    if diffCount > 1:
                        return False
            return diffCount == 1


        # 1. t 1 ch longer: find first different, jump off it in t and compare rest s vs t

        # 2. same length: 1 diff exactly
        # 3. t 1 ch shorter: similar to #1


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
