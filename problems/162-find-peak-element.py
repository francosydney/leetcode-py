from types import *
from typing import List
from math import inf

class Solution:
    def findPeakElement(self, nums: List[int]) -> int:
        '''
        >>> Solution.findPeakElement(Solution, [1,2,3,1])
        2
        >>> Solution.findPeakElement(Solution, [1,2,1,3,5,6,4])
        5
        >>> Solution.findPeakElement(Solution, [1,2])
        1
        >>> Solution.findPeakElement(Solution, [1])
        0
        >>> Solution.findPeakElement(Solution, [1,2,3])
        2
        >>> Solution.findPeakElement(Solution, [1,2,1])
        1
        '''
        l = len(nums)
        def valAtIndex(index):
            return nums[index] if 0 <= index < l else -inf

        def largerThanNeighbors(index):
            return valAtIndex(index) > valAtIndex(index - 1) and valAtIndex(index) > valAtIndex(index + 1)

        left, right = 0, l - 1
        while left <= right:
            mid = left + (right - left) // 2

            midPrevVal, midVal, midNextVal = valAtIndex(mid - 1), valAtIndex(mid), valAtIndex(mid + 1)
            if midPrevVal < midVal > midNextVal:
                return mid
            elif midPrevVal < midVal < midNextVal:
                left = mid + 1
            elif midPrevVal > midVal > midNextVal:
                right = mid - 1
            else:
                right = mid - 1
        
        # return nums[left]



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
