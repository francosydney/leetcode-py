from types import *
from typing import List
from math import inf

class Solution:
    def findMissingRanges(self, nums: List[int], lower: int, upper: int) -> List[str]:
        '''
        >>> Solution.findMissingRanges(Solution, [0,1,3,50,75], 0, 99)
        ['2', '4->49', '51->74', '76->99']
        >>> Solution.findMissingRanges(Solution, [], -3, -1)
        ['-3->-1']
        >>> Solution.findMissingRanges(Solution, [-1], -2, -1)
        ['-2']
        >>> Solution.findMissingRanges(Solution, [-2], -2, -1)
        ['-1']
        >>> Solution.findMissingRanges(Solution, [1000000000], 0, 1000000000)
        ['0->999999999']
        '''
        ranges = []
        nums = [lower-1] + nums + [upper+1]
        for i in range(1, len(nums)):
            a, b = nums[i-1], nums[i]
            if a + 1 < b:
                ranges.append(f'{a+1}->{b-1}' if a + 2 < b else str(a+1))

        return ranges


if __name__ == '__main__':
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print('--- %s seconds ---' % (time.time() - start_time))
