from types import *
from typing import List
from collections import defaultdict

# def radixsort(arr):
#     exp = 1
#     l = len(arr)
#     maxVal = max(arr)

#     while maxVal // exp:
#         count = [0] * 10
#         output = [0] * l

#         for num in arr:
#             count[(num // exp) % 10] += 1
#         for i in range(1, 10):
#             count[i] = count[i] + count[i-1]
#         for i in range(l-1, -1, -1):
#             indexForVal = (arr[i] // exp) % 10
#             output[count[indexForVal] - 1] = arr[i]
#             count[indexForVal] -= 1
#         for i in range(l):
#             arr[i] = output[i]
#         exp *= 10
    
#     return arr

# class Solution:
#     def maximumGap(self, nums: List[int]) -> int:
#         '''
#         >>> Solution.maximumGap(Solution, [3,6,9,1])
#         3
#         >>> Solution.maximumGap(Solution, [10])
#         0
#         '''
#         l = len(nums)
#         if l < 2:
#             return 0

#         radixsort(nums)

#         maxGap = 0
#         for i in range(1, l):
#             maxGap = max(maxGap, nums[i] - nums[i-1])
#         return maxGap

class Bucket:
    def __init__(self, val):
        self.min = val
        self.max = val

    def add(self, val):
        self.min = min(self.min, val)
        self.max = max(self.max, val)


class Solution:
    def maximumGap(self, nums: List[int]) -> int:
        '''
        >>> Solution.maximumGap(Solution, [3,6,9,1])
        3
        >>> Solution.maximumGap(Solution, [10])
        0
        >>> Solution.maximumGap(Solution, [1, 2])
        1
        >>> Solution.maximumGap(Solution, [1, 2, 4])
        2
        >>> Solution.maximumGap(Solution, [1, 1, 1, 2])
        1
        >>> Solution.maximumGap(Solution, [1, 1, 1, 1])
        0
        >>> Solution.maximumGap(Solution, [1, 1, 2, 2])
        1
        >>> Solution.maximumGap(Solution, [1,1,1,1,1,5,5,5,5,5])
        4
        >>> Solution.maximumGap(Solution, [13,123,4,15,2,452,35,234,51,234,123,4])
        218
        '''
        l = len(nums)
        if l < 2:
            return 0

        minVal, maxVal = min(nums), max(nums)

        bucketSize = (maxVal - minVal) // (l - 1)
        if bucketSize < 1:
            # print('here')
            count = [0] * (maxVal - minVal + 1)
            for num in nums:
                count[num - minVal] += 1
            maximumGap = 0
            indexToLastExistingValue = None
            # print(count)
            for index, countForVal in enumerate(count):
                if countForVal:
                    if indexToLastExistingValue is not None:
                        # print('updating', index, indexToLastExistingValue)
                        maximumGap = max(maximumGap, index - indexToLastExistingValue)
                    indexToLastExistingValue = index
            return maximumGap


        def getBucketId(num):
            return (num - minVal) // bucketSize
        buckets = {}

        for num in nums:
            bucketId = getBucketId(num)
            if bucketId in buckets:
                buckets[bucketId].add(num)
            else:
                buckets[bucketId] = Bucket(num)
        
        bucketList = [buckets[bucketId] for bucketId in sorted(buckets.keys())]
        # print(bucketList)

        maximumGap = bucketSize
        for i in range(1, len(bucketList)):
            maximumGap = max(maximumGap, bucketList[i].min - bucketList[i-1].max)

        return maximumGap
        # print('bucket size is', bucketSize)
        # for b in buckets.values():
            
        #     print(b.max, b.min, '####')
        # print(buckets)




if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
