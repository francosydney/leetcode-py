from types import *
from typing import List
from collections import Counter

class Solution:
    def majorityElement(self, nums: List[int]) -> int:
        '''
        >>> Solution.majorityElement(Solution, [2,2,1,1,1,2,2])
        2
        >>> Solution.majorityElement(Solution, [3,2,3])
        3
        '''
        counts = Counter(nums)
        print(counts)
        return max(counts.keys(), key=counts.get)


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
