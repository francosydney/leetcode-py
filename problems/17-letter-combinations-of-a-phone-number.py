from types import *
from typing import List


class Solution:
    def letterCombinations(self, digits: str) -> List[str]:
        '''
        >>> Solution.letterCombinations(Solution, "23")
        ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"]
        >>> Solution.letterCombinations(Solution, "7")
        ['p', 'q', 'r', 's']
        '''
        if len(digits) < 1:
            return []

        mapping = {"2": "abc", "3": "def", "4": "ghi", "5": "jkl",
                   "6": "mno", "7": "pqrs", "8": "tuv", "9": "wxyz"}

        results = list(mapping[digits[0]])
        for numChar in digits[1:]:
            _results = []
            for preResults in results:
                for ch in mapping[numChar]:
                    _results.append(preResults + ch)
            results = _results
        return results

        # s = 1
        # for ch in digits:
        #     s *= len(mapping[ch])
        # results = [[] for i in range(s)]

        # for ch in digits:
        #     mapTo = mapping[ch]
        #     itemCount = s / len(mapTo)
        #     for i in range(s):
        #         # print(ch, i % itemCount)
        #         results[i].append(mapTo[int(i/itemCount)])

        # return list(map(lambda arr: ''.join(arr), results))



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
