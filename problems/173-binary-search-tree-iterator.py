from types import *
from typing import List

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items) or items[index] is None:
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)



class BSTIterator:

    def __init__(self, root: TreeNode):
        self.stack = []
        self._leftmost_inorder(root)

    def _leftmost_inorder(self, root):
        # For a given node, add all the elements in the leftmost branch of the tree
        # under it to the stack.
        while root:
            self.stack.append(root)
            root = root.left

    def next(self) -> int:
        node = self.stack.pop()
        val = node.val
        if node.right:
            self._leftmost_inorder(node.right)

        return val


    def hasNext(self) -> bool:
        return self.stack

# class Solution:
#     def test(self, root: TreeNode):
#         '''
#         >>> (Solution.test(Solution, buildTree([7, 3, 15, None, None, 9, 20])))
#         xxxxxxxxx
#         '''
#         node = root
#         stack = []
#         while node or stack:
#             while node:
#                 stack.append(node)
#                 node = node.left
#             node = stack.pop()
#             print(node.val)
#             node = node.right
        
            



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
