from types import *
from typing import List
from itertools import zip_longest
from random import randint

class Solution:
    def largestNumber(self, nums: List[int]) -> str:
        '''
        >>> Solution.largestNumber(Solution, [10,2])
        '210'
        >>> Solution.largestNumber(Solution, [3,30,34,5,9])
        '9534330'
        >>> Solution.largestNumber(Solution, [1])
        '1'
        >>> Solution.largestNumber(Solution, [10])
        '10'
        >>> Solution.largestNumber(Solution, [0, 0])
        '0'
        '''
        numsString = [str(num) for num in nums]
        
        def isLarger(a, b):
            return a + b > b + a

        def partition(nums, pivot_index, left, right):
            pivot = nums[pivot_index]
            nums[pivot_index], nums[right] = nums[right], nums[pivot_index]
            divide = left
            for i in range(left, right):
                if isLarger(nums[i], pivot):
                    nums[i], nums[divide] = nums[divide], nums[i]
                    divide += 1
            nums[right], nums[divide] = nums[divide], nums[right]
            return divide


        def quicksort(arr, left, right):
            if right <= left:
                return arr
            pivot_index = randint(left, right)
            
            sorted_pivot_index = partition(arr, pivot_index, left, right)
            quicksort(arr, left, sorted_pivot_index - 1)
            quicksort(arr, sorted_pivot_index + 1, right)
            return arr

        sortedNumStrings = quicksort(numsString, 0, len(nums) - 1)
        if sortedNumStrings[0] == '0':
            return '0'

        return ''.join(sortedNumStrings)
        

if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
