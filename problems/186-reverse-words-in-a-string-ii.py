from types import *
from typing import List

class Solution:
    def reverseWords(self, s: List[str]) -> None:
        '''
        >>> Solution.reverseWords(Solution, ["t","h","e"," ","s","k","y"," ","i","s"," ","b","l","u","e"])
        ["b","l","u","e"," ","i","s"," ","s","k","y"," ","t","h","e"]
        >>> Solution.reverseWords(Solution, ['a'])
        ['a']
        '''
        def reverseSection(s, i, j):
            while i < j:
                s[i], s[j] = s[j], s[i]
                i += 1
                j -= 1
        
        reverseSection(s, 0, len(s) - 1)
        
        start = end = 0
        l = len(s)
        while start < l:
            while end < l and s[end] != ' ':
                end += 1
            reverseSection(s, start, end - 1)
            start = end + 1
            end += 1

        return s

if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
