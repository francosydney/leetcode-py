from types import *
from typing import List
import math

class Solution:
    def maxProfit(self, k: int, prices: List[int]) -> int:
        '''
        >>> Solution.maxProfit(Solution, 2, [2,4,1])
        2
        >>> Solution.maxProfit(Solution, 2, [3,2,6,5,0,3])
        7
        >>> Solution.maxProfit(Solution, 0, [1,3])
        0
        '''
        if k < 1:
            return 0

        cost = [math.inf] * k
        profit = [0] * k

        for price in prices:
            for i in range(k):
                cost[i] = min(cost[i], price - (0 if i == 0 else profit[i - 1]))
                profit[i] = max(profit[i], price - cost[i])

        return profit[k - 1]



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
