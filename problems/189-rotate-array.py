from types import *
from typing import List

class Solution:
    def rotate(self, nums: List[int], k: int) -> None:
        '''
        >>> Solution.rotate(Solution, [1,2,3,4,5,6,7], 3)
        [5, 6, 7, 1, 2, 3, 4]
        >>> Solution.rotate(Solution, [1], 1)
        [1]
        >>> Solution.rotate(Solution, [-1,-100,3,99], 2)
        [3,99,-1,-100]
        '''
        l = len(nums)
        k = k % l
        indexMap = [x for x in range(k + 1, l)] + [x for x in range(k + 1)]
        copy = [nums[indexMap[i]] for i in range(l)]
        for i in range(l):
            nums[i] = copy[i]

        return copy

            


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
