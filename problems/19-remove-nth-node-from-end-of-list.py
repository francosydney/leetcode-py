from types import *
from typing import List
from binarytree import Node as TreeNode

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def makeList(arr: ListNode):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def printList(l: ListNode):
    arr = []
    p = l
    while p is not None:
        # print('### appending', p.val)
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))


class Solution:
    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:
        '''
        >>> printList(Solution.removeNthFromEnd(Solution, makeList([1,2,3,4,5]), 2))
        1,2,3,5
        >>> printList(Solution.removeNthFromEnd(Solution, makeList([1,2]), 1))
        1
        >>> printList(Solution.removeNthFromEnd(Solution, makeList([1,2]), 2))
        2
        '''
        if not head.next:
            return None
        dummy = ListNode(0)
        dummy.next = head
        prevOfToDelete = p = dummy
        for _ in range(n):
            p = p.next
        while p.next:
            p = p.next
            prevOfToDelete = prevOfToDelete.next
        prevOfToDelete.next = prevOfToDelete.next.next
        return dummy.next
        

if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
