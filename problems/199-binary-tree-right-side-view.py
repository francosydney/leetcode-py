from types import *
from typing import List

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items) or items[index] is None:
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)



class Solution:
    def rightSideView(self, root: TreeNode) -> List[int]:
        '''
        >>> (Solution.rightSideView(Solution, buildTree([1,2,3,None,5,None,4])))
        [1,3,4]
        '''
        if not root:
            return []
        nodesAtNextLevel = [root]
        rightSideNodes = []

        while True:
            nodes = []
            if nodesAtNextLevel:
                rightSideNodes.append(nodesAtNextLevel[-1].val)
            else:
                break

            for node in nodesAtNextLevel:
                for child in filter(bool, [node.left, node.right]):
                    nodes.append(child)

            nodesAtNextLevel = nodes
        return rightSideNodes




if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
