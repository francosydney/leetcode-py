from types import *
from typing import List
from binarytree import Node as TreeNode

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def makeList(arr):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def printList(l: ListNode):
    arr = []
    p = l
    while p is not None:
        # print('### appending', p.val)
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))


class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        '''
        >>> printList(Solution.addTwoNumbers(Solution, makeList([1,8,3]), makeList([1,2,3,2]) ))
        2,0,7,2
        >>> printList(Solution.addTwoNumbers(Solution, makeList([1]), makeList([1]) ))
        2
        >>> printList(Solution.addTwoNumbers(Solution, makeList([1]), makeList([9]) ))
        0,1
        '''
        up = 0
        sentinel = p = ListNode(0)
        while l1 or l2:
            val = (l1.val if l1 else 0) + (l2.val if l2 else 0) + up

            if val >= 10:
                val -= 10
                up = 1
            else:
                up = 0
            p.next = ListNode(val)
            p = p.next

            if l1:
                l1 = l1.next
            if l2:
                l2 = l2.next
        if up:
            p.next = ListNode(1)
        return sentinel.next


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
