from types import *
from typing import List
from itertools import zip_longest

# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

def printList(l: ListNode):
    '''
    >>> a = ListNode(1)
    >>> a.next = ListNode(2)
    >>> a.next.next = ListNode(4)
    >>> printList(a)
    124
    '''

    arr = []
    p = l
    while p is not None:
        arr.append(str(p.val))
        p = p.next
    print(''.join(arr))

def makeList(arr):
    '''
    >>> a = makeList([1, 4, 5])
    >>> printList(a)
    145
    '''
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next

    return header.next

class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        '''
        >>> a = makeList([2, 4, 3]) # 342
        >>> b = makeList([5, 6, 4]) # 465
        >>> c = Solution.addTwoNumbers(Solution, a, b)
        >>> printList(c)
        708
        >>> a = makeList([0]) 
        >>> b = makeList([0])
        >>> c = Solution.addTwoNumbers(Solution, a, b)
        >>> printList(c)
        0
        >>> a = makeList([0, 1]) 
        >>> b = makeList([0])
        >>> c = Solution.addTwoNumbers(Solution, a, b)
        >>> printList(c)
        01
        >>> a = makeList([9,9,9,9]) 
        >>> b = makeList([9,9,9,9])
        >>> c = Solution.addTwoNumbers(Solution, a, b)
        >>> printList(c)
        89991
        '''
        def readList(p):
            arr = []
            while p:
                arr.append(p.val)
                p = p.next
            return arr

        digits1 = readList(l1)
        digits2 = readList(l2)
        incre = 0
        
        p = header = ListNode(0)
        for num1, num2 in zip_longest(digits1, digits2, fillvalue=0):
            sum_ = num1 + num2 + incre                
            p.next = ListNode(sum_ % 10)
            p = p.next
            incre = int(sum_ / 10)

        if incre is not 0:
            p.next = ListNode(1)
            p = p.next

        return header.next
        

if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
