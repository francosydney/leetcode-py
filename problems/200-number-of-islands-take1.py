from types import *
from typing import List
import operator
from collections import deque

class Solution:
    def numIslands(self, grid: List[List[str]]) -> int:
        '''
        >>> grid = [["1","1","1","1","0"],["1","1","0","1","0"],["1","1","0","0","0"],["0","0","0","0","0"]]
        >>> Solution.numIslands(Solution, grid)
        1
        >>> grid = [["1","1","0","0","0"],["1","1","0","0","0"],["0","0","1","0","0"],["0","0","0","1","1"]]
        >>> Solution.numIslands(Solution, grid)
        3
        '''
        r, c = len(grid), len(grid[0])
        count = 0

        def inBound(pos):
            return 0 <= pos[0] < r and 0 <= pos[1] < c
        def applyDelta(pos, delta):
            return (pos[0] + delta[0], pos[1] + delta[1])

        deltas = [(-1, 0), (1, 0), (0, 1), (0, -1)]

        def markByBFS(pos, grid):
            grid[pos[0]][pos[1]] = '0'
            ps = deque([pos])
            while ps:
                p = ps.popleft()
                
                for n_i, n_j in filter(inBound, [applyDelta(p, delta) for delta in deltas]):
                    if grid[n_i][n_j] == '1':
                        grid[n_i][n_j] = '0'
                        ps.append((n_i, n_j))
            

        for i in range(r):
            for j in range(c):
                if grid[i][j] == '1':
                    markByBFS((i, j), grid)
                    count += 1

        return count


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
