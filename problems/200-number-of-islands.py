from types import *
from typing import List


class Solution:
    def numIslands(self, grid: List[List[str]]) -> int:
        '''
        >>> Solution.numIslands(Solution, [[1,1,1,1,0], [1, 1, 0, 1, 0], [1, 1, 0, 0, 0], [0,0,0,0,0]] )
        1
        >>> Solution.numIslands(Solution, [[1,1,0,0,0], [1, 1, 0, 0, 0], [0, 0, 1, 0, 0], [0,0,0,1,1]] )
        3
        '''
        serial = 2
        m = len(grid)
        if m is 0:
            return 0
        n = len(grid[0])
        if n is 0:
            return 0

        def isValidCoordX(x):
            return x >= 0 and x < m

        def isValidCoordY(y):
            return y >= 0 and y < n

        def BFSPaintFrom(grid, coords, serial):
            i, j = coords
            grid[i][j] = serial
            for m, n in [(i + 1, j), (i, j + 1), (i - 1, j), (i, j - 1)]:
                if isValidCoordX(m) and isValidCoordY(n) and grid[m][n] is 1:
                    BFSPaintFrom(grid, (m, n), serial)

        for i in range(0, m):
            for j in range(0, n):
                grid[i][j] = int(grid[i][j])

        for i in range(0, m):
            for j in range(0, n):
                if grid[i][j] is 1:
                    BFSPaintFrom(grid, (i, j), serial)
                    serial += 1

        return serial - 2


      
if __name__ == "__main__":
    import doctest
    doctest.testmod()
