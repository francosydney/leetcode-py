from types import *
from typing import List
from binarytree import Node as TreeNode

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def makeList(arr: ListNode):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def printList(l: ListNode):
    arr = []
    p = l
    while p is not None:
        # print('### appending', p.val)
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))


class Solution:
    def removeElements(self, head: ListNode, val: int) -> ListNode:
        '''
        >>> printList(Solution.removeElements(Solution, makeList([1,2,6,3,4,5,6]), 6))
        1,2,3,4,5
        >>> printList(Solution.removeElements(Solution, makeList([]), 1))
        
        >>> printList(Solution.removeElements(Solution, makeList([7,7,7,7]), 7))
        
        '''
        dummy = ListNode(0)
        dummy.next = head
        p = dummy
        while p.next:
            if p.next.val == val:
                p.next = p.next.next
            else:
                p = p.next
        return dummy.next


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
