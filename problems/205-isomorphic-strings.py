from types import *
from typing import List

class Solution:
    def isIsomorphic(self, s: str, t: str) -> bool:
        '''
        >>> Solution.isIsomorphic(Solution, "egg", "add")
        True
        >>> Solution.isIsomorphic(Solution, "foo", "bar")
        False
        >>> Solution.isIsomorphic(Solution, "paper", "title")
        True
        '''
        mapping = {}
        mappedTo = set()

        if len(s) != len(t):
            return False

        for i, charS in enumerate(s):
            charT = t[i]
            if charS in mapping:
                if charT != mapping[charS]:
                    return False
            else:
                if charT in mappedTo:
                    return False

                mapping[charS] = charT
                mappedTo.add(charT)

        return True



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
