from types import *
from typing import List
from binarytree import Node as TreeNode

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def makeList(arr):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def printList(l: ListNode):
    arr = []
    p = l
    while p is not None:
        # print('### appending', p.val)
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))


class Solution:
    def reverseList(self, head: ListNode) -> ListNode:
        '''
        >>> printList(Solution.reverseList(Solution, makeList([1,2,3,4,5])))
        5,4,3,2,1
        >>> printList(Solution.reverseList(Solution, makeList([1])))
        1
        '''
        prev = None
        nxt = None
        curr = head
        while curr:
            nxt = curr.next
            curr.next = prev
            curr, prev = nxt, curr
        return prev


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
