from types import *
from typing import List
from math import inf

class Solution:
    def minSubArrayLen(self, target: int, nums: List[int]) -> int:
        '''
        >>> Solution.minSubArrayLen(Solution, 4, [1,4,4])
        1
        >>> Solution.minSubArrayLen(Solution, 11, [1,1,1,1,1,1,1,1])
        0
        >>> Solution.minSubArrayLen(Solution, 11, [1,2,3,4,5])
        3
        >>> Solution.minSubArrayLen(Solution, 7, [2,3,1,2,4,3])
        2
        '''
        leftBorder = 0 # all indice to the left are removed
        minLen = inf
        currentSum = 0

        for index, num in enumerate(nums):
            currentSum += num
            while currentSum >= target:
                minLen = min(minLen, index - leftBorder + 1)
                currentSum -= nums[leftBorder]
                leftBorder += 1

        return 0 if minLen == inf else minLen




if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
