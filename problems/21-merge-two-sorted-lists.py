from types import *

# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None
counter = 10

class Solution:
    '''
    >>> a = ListNode(1)
    >>> a.next = ListNode(2)
    >>> a.next.next = ListNode(4)
    >>> b = ListNode(1)
    >>> b.next = ListNode(3)
    >>> b.next.next = ListNode(4)
    >>> Solution.mergeTwoLists(Solution, a, b)
    1

    '''

    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:
        result = ListNode(0)
        header = result
        p1 = l1
        p2 = l2
        while p1 is not None and p2 is not None:
            if p1.val < p2.val:
                result.next = ListNode(p1.val)
                p1 = p1.next
            else:
                result.next = ListNode(p2.val)
                p2 = p2.next
            result = result.next

        p = p1 if p1 is not None else p2
        while p is not None:
            result.next = ListNode(p.val)
            result = result.next
            p = p.next
        
        header = header.next

        return header


if __name__ == "__main__":
    import doctest
    doctest.testmod()
