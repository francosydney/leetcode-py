from types import *
from typing import List
# from collections import deque
from heapq import heappush, heappop, heappushpop

class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:
        '''
        >>> Solution.findKthLargest(Solution, [3,2,1,5,6,4], 2)
        5
        >>> Solution.findKthLargest(Solution, [3,2,3,1,2,4,5,5,6], 4)
        4
        '''
        largests = []

        for num in nums:
            if len(largests) < k:
                heappush(largests, num)
            else:
                heappushpop(largests, num)

        return largests[0]


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
