from types import *
from typing import List

class Solution:
    def combinationSum3(self, k: int, n: int) -> List[List[int]]:
        '''
        >>> Solution.combinationSum3(Solution, 3, 7)
        [[1,2,4]]
        >>> Solution.combinationSum3(Solution, 3, 9)
        [[1,2,6],[1,3,5],[2,3,4]]
        >>> Solution.combinationSum3(Solution, 4, 1)
        []
        >>> Solution.combinationSum3(Solution, 3, 2)
        []
        >>> Solution.combinationSum3(Solution, 9, 45)
        [[1,2,3,4,5,6,7,8,9]]
        '''
        results = []
        
        def backtrack(selected = []):
            if sum(selected) == n and len(selected) == k:
                results.append(selected[:])
                return
            if sum(selected) >= n or len(selected) >= k:
                return
            
            for num in range(selected[-1] + 1 if len(selected) else 1, 10):
                selected.append(num)
                backtrack(selected)
                selected.pop()
            

        backtrack()
        
        return results


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
