from types import *
from typing import List

class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:
        '''
        >>> Solution.containsDuplicate(Solution, [1,2,3,1])
        True
        >>> Solution.containsDuplicate(Solution, [1,2,3,4])
        False
        >>> Solution.containsDuplicate(Solution, [1,1,1,3,3,4,3,2,4,2])
        True
        '''
        seen = set()
        for num in nums:
            if num in seen:
                return True
            seen.add(num)
        return False


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
