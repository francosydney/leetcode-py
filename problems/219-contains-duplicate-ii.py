from types import *
from typing import List
from heapq import heappush, heappop

class Solution:
    def containsNearbyDuplicate(self, nums: List[int], k: int) -> bool:
        '''
        >>> Solution.containsNearbyDuplicate(Solution, [1,2,3,1], 3)
        True
        >>> Solution.containsNearbyDuplicate(Solution, [1,0,1,1], 1)
        True
        >>> Solution.containsNearbyDuplicate(Solution, [1,2,3,1,2,3], 2)
        False
        '''
        bag = set()
        i = 0
        for i in range(len(nums)):
            if nums[i] in bag:
                return True
            bag.add(nums[i])
            if len(bag) > k:
                bag.remove(nums[i - k])
        return False



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
