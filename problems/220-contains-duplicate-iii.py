from types import *
from typing import List
from collections import defaultdict

class Solution:
    def containsNearbyAlmostDuplicate(self, nums: List[int], k: int, t: int) -> bool:
        '''
        >>> Solution.containsNearbyAlmostDuplicate(Solution, [1,2,3,1], 3, 0)
        True
        >>> Solution.containsNearbyAlmostDuplicate(Solution, [1,0,1,1], 1, 2)
        True
        >>> Solution.containsNearbyAlmostDuplicate(Solution, [1,5,9,1,5,9], 2, 3)
        False
        >>> Solution.containsNearbyAlmostDuplicate(Solution, [1,5,9,1,5,8], 2, 3)
        True
        '''
        bucketSize = t + 1
        def getBucketId(num):
            return abs(num // bucketSize)

        buckets = defaultdict(set)
        totalBucketSize = 0

        for i in range(len(nums)):
            num = nums[i]
            bucketId = getBucketId(num)
            thisBucket = buckets[bucketId]

            possibleBuckets = [buckets[bucketIndex] for bucketIndex in [bucketId + i for i in [-1, 0, 1]] ]
            
            for bucket in possibleBuckets:
                for n in bucket:
                    if abs(n - num) <= t:
                        return True
            
            thisBucket.add(num)
            totalBucketSize += 1

            if totalBucketSize > k:
                buckets[getBucketId(nums[i - k])].remove(nums[i - k])
                totalBucketSize -= 1

        return False


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
