from types import *
from typing import List

class Solution:
    def calculate(self, s: str) -> int:
        '''
        >>> Solution.calculate(Solution, "1 + 1")
        2
        >>> Solution.calculate(Solution, " 2-1 + 2 ")
        3
        >>> Solution.calculate(Solution, "(1+(4+5+2)-3)+(6+8)")
        23
        >>> Solution.calculate(Solution,"-2+ 1")
        -1
        '''
        def processStackedExpression(stack):
            if not stack:
                return 0
            if stack[-1] == '-':
                stack.pop()
                res = - stack.pop()
            else:
                res = stack.pop()

            while stack and stack[-1] != ')':
                sign = stack.pop()
                if sign == '+':
                    res += stack.pop()
                elif sign == '-':
                    res -= stack.pop()

            return res


        stack = []

        currentNum = n = 0

        for ch in reversed(s):
            if ch.isdigit():
                currentNum += 10 ** n * int(ch)
                n += 1
            else:
                if n:
                    stack.append(currentNum)
                    currentNum = n = 0

                if ch == '(':
                    val = processStackedExpression(stack)
                    stack.pop()
                    stack.append(val)
                elif ch != ' ':
                    stack.append(ch)

        if n:
            stack.append(currentNum)

        return processStackedExpression(stack)


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
