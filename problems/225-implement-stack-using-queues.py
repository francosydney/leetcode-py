from collections import deque

class MyStack:

    def __init__(self):
        self.q = deque([])
        self.buffer = deque([])
        self.topValue = None

    def push(self, x: int) -> None:
        self.topValue = x
        self.q.append(x)

    def pop(self) -> int:
        while len(self.q) > 1:
            item = self.q.popleft()
            self.buffer.append(item)
            self.topValue = item
        val = self.q.popleft()
        self.q, self.buffer = self.buffer, deque([])
        return val

    def top(self) -> int:
        return self.topValue

    def empty(self) -> bool:
        return len(self.q) == 0


# Your MyStack object will be instantiated and called as such:
obj = MyStack()
obj.push(1)
obj.push(2)

print(obj.top())
print(obj.pop())
print(obj.pop())
print(obj.empty())