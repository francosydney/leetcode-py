from types import *
from typing import List
from binarytree import Node

def invertTree(root: Node) -> Node:
    if root is None:
        return

    root.left, root.right = root.right, root.left
    invertTree(root.left)
    invertTree(root.right)


class Solution:
    def invertTree(self, root: Node) -> Node:
        '''
        >>> root = Node(4)
        >>> root.left = Node(2)
        >>> root.right = Node(7)
        >>> root.left.left = Node(1)
        >>> root.left.right = Node(3)
        >>> root.right.left = Node(6)
        >>> root.right.right = Node(9)
        >>> Solution.invertTree(Solution, root)
        >>> print(root)
        111
        '''
        invertTree(root)


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
