from types import *
from typing import List

class Solution:
    def calculate(self, s: str) -> int:
        '''
        >>> Solution.calculate(Solution, "3+2*2")
        7
        >>> Solution.calculate(Solution, " 3/2 ")
        1
        >>> Solution.calculate(Solution, " 3+5 / 2 ")
        5
        >>> Solution.calculate(Solution, "0*1")
        0
        '''

        stack = []
        currentNum = 0
        operation = '+'
        l = len(s)

        for i in range(l):
            ch = s[i]

            if ch.isdigit():
                currentNum = (currentNum * 10) + int(ch)

            if (not ch.isdigit() and ch != ' ') or i == l - 1:
                if operation == '+':
                    stack.append(currentNum)
                elif operation == '-':
                    stack.append(-currentNum)
                elif operation == '*':
                    stack.append(stack.pop() * currentNum)
                elif operation == '/':
                    stack.append(int(stack.pop() / currentNum))

                operation = ch
                currentNum = 0
                    

        return sum(stack)



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
