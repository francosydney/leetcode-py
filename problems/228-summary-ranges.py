from types import *
from typing import List
from math import inf

class Solution:
    def summaryRanges(self, nums: List[int]) -> List[str]:
        ans = []
        nums.append(inf)
        j = 0
        for i in range(1, len(nums)):
            if nums[i] != nums[i-1] + 1:
                ans.append(f'{nums[j]}->{nums[i-1]}' if i > j + 1 else str(nums[j]))
                j = i
        return ans

if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
