from types import *
from typing import List
from collections import Counter

class Solution:
    def majorityElement(self, nums: List[int]) -> List[int]:
        '''
        >>> Solution.majorityElement(Solution, [3,2,3])
        [3]
        >>> Solution.majorityElement(Solution, [1])
        [1]
        >>> Solution.majorityElement(Solution, [1,2])
        [1, 2]
        >>> Solution.majorityElement(Solution, [2,1,1,3,1,4,5,6])
        [1]
        '''
        candidate1 = candidate2 = None
        candidate1Count = candidate2Count = 0

        for num in nums:
            if candidate1 == num:
                candidate1Count += 1
            elif candidate2 == num:
                candidate2Count += 1
            elif candidate1Count == 0:
                candidate1 = num
                candidate1Count = 1
            elif candidate2Count == 0:
                candidate2 = num
                candidate2Count = 1
            else:
                candidate1Count -= 1
                candidate2Count -= 1

        elements = list(filter(lambda c: nums.count(c) > len(nums) // 3, [candidate1, candidate2]))
        
        return elements
        


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
