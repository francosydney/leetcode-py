from types import *
from typing import List
from binarytree import Node as TreeNode
import heapq

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def makeList(arr):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def printList(l: ListNode):
    arr = []
    p = l
    while p is not None:
        # print('### appending', p.val)
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))


class Solution:
    def mergeKLists(self, lists: List[ListNode]) -> ListNode:
        '''
        >>> printList(Solution.to_replace(Solution, makeList([])))
        to_replace
        '''
        serial = 0
        def heappush(headers, p):
            nonlocal serial
            serial += 1
            heapq.heappush(headers, (p.val, serial, p))

        headers = []
        for head in lists:
            if head:
                heappush(headers, head)

        sentinel = p = ListNode(-1)
        while headers:
            _, _, h = heapq.heappop(headers)
            p.next = h
            p = p.next
            
            if h.next:
                heappush(headers, h.next)
        return sentinel.next




if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
