from types import *
from typing import List
from heapq import heappush, heappop
# execfile('basics/utils')
# import sys
# sys.path.insert(1, '/Users/franco/projects/leetcode-py/basics')
# import utils


class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


def printList(l: ListNode):
    '''
    >>> a = ListNode(1)
    >>> a.next = ListNode(2)
    >>> a.next.next = ListNode(4)
    >>> printList(a)
    124
    '''

    arr = []
    p = l
    while p is not None:
        arr.append(str(p.val))
        p = p.next
    print(''.join(arr))


def makeList(arr):
    '''
    >>> a = makeList([1, 4, 5])
    >>> printList(a)
    145
    '''
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    
    return header.next

class Solution:
    def mergeKLists(self, lists: List[ListNode]) -> ListNode:
        '''
        >>> printList(Solution.mergeKLists(Solution, [ makeList([1, 4, 5]), makeList([1, 3, 4]), makeList([2, 6]) ] ) )
        11234456
        '''
        counter = 0

        header = r = ListNode(0)
        h = []
        for node in lists:
            if node is not None:
                heappush(h, (node.val, (counter, node)))
                counter += 1
        while len(h) > 0:
            val, (_, p) = heappop(h)
            r.next = p
            r = r.next

            p = p.next
            if p is not None:
                heappush(h, (p.val, (counter, p)))
                counter += 1

        return header.next


# Solution.mergeKLists(
#     Solution,
#     [ makeList([1, 4, 5]), makeList([1, 3, 4]), makeList([2, 6]) ]
# )


# printList(Solution.mergeKLists(Solution, [
#     makeList([1, 4, 5]), makeList([1, 3, 4]), makeList([2, 6])]))

if __name__ == "__main__":
    import doctest
    doctest.testmod()
