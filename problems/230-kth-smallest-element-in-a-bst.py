from types import *
from typing import List

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items) or items[index] is None:
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)



class Solution:
    def kthSmallest(self, root: TreeNode, k: int) -> int:
        '''
        >>> (Solution.kthSmallest(Solution, buildTree([3,1,4,None,2]), 2))
        2
        '''
        node = root
        stack = []
        val = None
        while k > 0 and (node or stack):
            while node:
                stack.append(node)
                node = node.left
            node = stack.pop()
            val = node.val
            node = node.right
            k -= 1
        return val
        


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
