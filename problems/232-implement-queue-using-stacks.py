from types import *
from typing import List

class MyQueue:

    def __init__(self):
        self.stackMadeQueue = []
        self.stack = []
        
    def push(self, x: int) -> None:
        while self.stackMadeQueue:
            self.stack.append(self.stackMadeQueue.pop())
        self.stackMadeQueue.append(x)
        while self.stack:
            self.stackMadeQueue.append(self.stack.pop())

    def pop(self) -> int:
        return self.stackMadeQueue.pop()

    def peek(self) -> int:
        return self.stackMadeQueue[len(self.stackMadeQueue) - 1]

    def empty(self) -> bool:
        return len(self.stackMadeQueue) == 0


# Your MyQueue object will be instantiated and called as such:
myQueue = MyQueue()
myQueue.push(1) # queue is: [1]
myQueue.push(2) # queue is: [1, 2] (leftmost is front of the queue)
print(myQueue.peek()) # return 1
print(myQueue.pop()) # return 1, queue is [2]
print(myQueue.pop()) # return 2
print(myQueue.empty()) # return True