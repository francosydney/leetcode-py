from types import *
from typing import List
from binarytree import Node as TreeNode

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def makeList(arr):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def printList(l: ListNode):
    arr = []
    p = l
    while p is not None:
        # print('### appending', p.val)
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))


class Solution:
    def isPalindrome(self, head: ListNode) -> bool:
        '''
        >>> (Solution.isPalindrome(Solution, makeList([1,2,2,1])))
        True
        >>> (Solution.isPalindrome(Solution, makeList([1,2,2,3])))
        False
        >>> (Solution.isPalindrome(Solution, makeList([1,2,3,2,1])))
        True
        >>> (Solution.isPalindrome(Solution, makeList([1])))
        True
        >>> (Solution.isPalindrome(Solution, makeList([1,2])))
        False
        '''
        # left = right = head
        # stop = False
        # result = True

        # def backtrack(r):
        #     nonlocal left, stop, result
        #     if not r:
        #         return
        #     backtrack(r.next)
        #     if left == r or r.next == left:
        #         stop = True
        #     if stop:
        #         return
        #     if left.val != r.val:
        #         result = False
        #         stop = True 
        #     left = left.next

        # backtrack(right)
        # return result
        
        self.left = head
        def backtrack(r):
            if not r:
                return True
            if not backtrack(r.next):
                return False
            if r.val != self.left.val:
                return False
            self.left = self.left.next
            return True

        return backtrack(head)




if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
