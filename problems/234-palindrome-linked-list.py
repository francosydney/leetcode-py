from types import *
from typing import List

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def makeList(arr: ListNode):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def printList(l: ListNode):
    arr = []
    p = l
    while p is not None:
        # print('### appending', p.val)
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))


def getMiddleNode(head: ListNode) -> ListNode:
    '''
    >>> printList(getMiddleNode(makeList([1,2,3,4,5])))
    4,5
    >>> printList(getMiddleNode(makeList([1,2,3,4])))
    3,4
    '''
    fast = head
    slow = head
    while fast and fast.next:
        slow = slow.next
        fast = fast.next.next
    if fast:
        slow = slow.next
    return slow


def reverseList(head: ListNode) -> ListNode:
    '''
    >>> printList(reverseList(makeList([1,2,3,4,5])))
    5,4,3,2,1
    '''
    prev = None
    curr = head
    nxt = head
    while curr:
        nxt = curr.next
        curr.next = prev
        prev, curr = curr, nxt
    return prev


class Solution:
    def isPalindrome(self, head: ListNode) -> bool:
        '''
        >>> l = makeList([1,2,2,1])
        >>> Solution.isPalindrome(Solution, l)
        True
        >>> l = makeList([1])
        >>> Solution.isPalindrome(Solution, l)
        True
        >>> l = makeList([1,2,3,2,1])
        >>> Solution.isPalindrome(Solution, l)
        True
        >>> l = makeList([1,2,3,4])
        >>> Solution.isPalindrome(Solution, l)
        False
        >>> l = makeList([1,2])
        >>> Solution.isPalindrome(Solution, l)
        False
        >>> l = makeList([1,2,3,4,3,2,1])
        >>> Solution.isPalindrome(Solution, l)
        True
        '''
        secondHalf = getMiddleNode(head)
        reversedSecondHalf = reverseList(secondHalf)
        p1 = reversedSecondHalf
        p2 = head
        while p1:
            if p1.val != p2.val:
                return False
            p1 = p1.next
            p2 = p2.next
        
        return True


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
