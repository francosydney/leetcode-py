from types import *
from typing import List

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items) or items[index] is None:
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)



class Solution:
    def lowestCommonAncestor(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
        '''
        >>> (Solution.lowestCommonAncestor(Solution, buildTree([3,5,1,6,2,0,8,None,None,7,4]), buildTree([5]), buildTree([4])))
        5
        >>> (Solution.lowestCommonAncestor(Solution, buildTree([3,5,1,6,2,0,8,None,None,7,4]), buildTree([5]), buildTree([1])))
        3
        '''
        values = {p.val, q.val}
        lowerParent = None

        def getTreeContainsCount(root):
            nonlocal lowerParent
            if not root:
                return 0
            thisTreeContainCount = getTreeContainsCount(root.left) + getTreeContainsCount(root.right) + (1 if root.val in values else 0)
            if not lowerParent and thisTreeContainCount == 2:
                lowerParent = root
            return thisTreeContainCount

        getTreeContainsCount(root)
        return lowerParent.val


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
