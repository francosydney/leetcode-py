from types import *
from typing import List

class Solution:
    def productExceptSelf(self, nums: List[int]) -> List[int]:
        '''
        >>> Solution.productExceptSelf(Solution, [1,2,3,4])
        [24,12,8,6]
        >>> Solution.productExceptSelf(Solution, [-1,1,0,-3,3])
        [0,0,9,0,0]
        '''
        l = len(nums)
        productLeft = [1] * l
        for i in range(1, l):
            productLeft[i] = productLeft[i-1] * nums[i-1]

        productRight = [1] * l
        for i in range(l-2, -1, -1):
            productRight[i] = productRight[i+1] * nums[i+1]
    
        answer = [1] * l
        for i in range(l):
            answer[i] = productLeft[i] * productRight[i]
        return answer


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
