from types import *
from typing import List

class Solution:
    def productExceptSelf(self, nums: List[int]) -> List[int]:
        '''
        >>> Solution.productExceptSelf(Solution, [1,2,3,4])
        [24, 12, 8, 6]
        >>> Solution.productExceptSelf(Solution, [1,2])
        [2, 1]
        '''
        prefixMultiply = nums.copy()
        suffixMultiply = nums.copy()
        for index in range(1, len(nums)):
            prefixMultiply[index] = prefixMultiply[index - 1] * prefixMultiply[index]
        for index in range(len(nums) - 2, -1, -1):
            suffixMultiply[index] = suffixMultiply[index + 1] * suffixMultiply[index]
        
        if len(nums) is 2:
            return nums[::-1]

        result1 = [suffixMultiply[1]]
        result2 = [prefixMultiply[i - 1] * suffixMultiply[i + 1] for i in range(1, len(nums) - 1)]
        result3 = [prefixMultiply[-2]]
        return result1 + result2 + result3
        


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
