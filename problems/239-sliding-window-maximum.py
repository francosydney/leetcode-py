from types import *
from typing import List
from collections import deque


class Solution:
    def maxSlidingWindow(self, nums: List[int], k: int) -> List[int]:
        '''
        >>> Solution.maxSlidingWindow(Solution, [1,3,-1,-3,5,3,6,7], 3)
        [3, 3, 5, 5, 6, 7]
        >>> Solution.maxSlidingWindow(Solution, [4,-2], 2)
        [4]
        >>> Solution.maxSlidingWindow(Solution, [1], 1)
        [1]
        >>> Solution.maxSlidingWindow(Solution, [9,11], 2)
        [11]
        '''
        def updateAccountableNums(index):
            if accountableNumIndice and accountableNumIndice[0] == index - k:
                accountableNumIndice.popleft()
            while accountableNumIndice and nums[accountableNumIndice[-1]] < nums[index]:
                accountableNumIndice.pop()

        accountableNumIndice = deque([])

        for i in range(k-1):
            updateAccountableNums(i)
            accountableNumIndice.append(i)

        output = []
        for i in range(k-1, len(nums)):
            updateAccountableNums(i)
            accountableNumIndice.append(i)
            output.append(nums[accountableNumIndice[0]])
            
        return (output)


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
