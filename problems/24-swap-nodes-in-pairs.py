from types import *
from typing import List
from binarytree import Node as TreeNode

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def makeList(arr: ListNode):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def printList(l: ListNode):
    arr = []
    p = l
    while p is not None:
        # print('### appending', p.val)
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))


class Solution:
    def swapPairs(self, head: ListNode) -> ListNode:
        '''
        >>> printList(Solution.swapPairs(Solution, makeList([1,2,3,4])))
        2,1,4,3
        >>> printList(Solution.swapPairs(Solution, makeList([1,2,3,4,5])))
        2,1,4,3,5
        >>> printList(Solution.swapPairs(Solution, makeList([1,2])))
        2,1
        '''
        prev = ListNode(-1)
        p = prev
        prev.next = head
        while prev.next and prev.next.next:
            node1 = prev.next
            node2 = node1.next
            rest = node2.next
            prev.next = node2
            node2.next = node1
            node1.next = rest
            prev = node1
        return p.next


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
