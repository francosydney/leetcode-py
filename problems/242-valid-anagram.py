from types import *
from typing import List
from collections import Counter

class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        '''
        >>> Solution.to_replace(Solution, to_replace)
        to_replace
        >>> Solution.to_replace(Solution, to_replace)
        to_replace
        >>> Solution.to_replace(Solution, to_replace)
        to_replace
        '''
        if len(s) != len(t):
            return False
            
        charCount = Counter(s)
        for ch in t:
            charCount[ch] -= 1
            if charCount[ch] < 0:
                return False

        return True


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
