from types import *
from typing import List
import math

class Solution:
    def shortestDistance(self, wordsDict: List[str], word1: str, word2: str) -> int:
        '''
        >>> Solution.shortestDistance(Solution, ["practice", "makes", "perfect", "coding", "makes"], "coding", "practice")
        3
        '''
        lastWord1Index = None
        lastWord2Index = None
        minDistance = math.inf

        for index, word in enumerate(wordsDict):
            if word == word1:
                lastWord1Index = index
                if lastWord2Index is not None:
                    minDistance = min(lastWord1Index - lastWord2Index, minDistance)
            elif word == word2:
                lastWord2Index = index
                if lastWord1Index is not None:
                    minDistance = min(lastWord2Index - lastWord1Index, minDistance)
        return minDistance


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
