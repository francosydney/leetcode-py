from types import *
from typing import List


class WordDistance:

    def __init__(self, wordsDict: List[str]):
        self.occurence = defaultdict(list)
        for index, word in enumerate(wordsDict):
            self.occurence[word].append(index)

    def shortest(self, word1: str, word2: str) -> int:
        minDistance = math.inf
        occurence1 = self.occurence[word1]
        occurence2 = self.occurence[word2]
        i = j = 0
        while i < len(occurence1) and j < len(occurence2):
            minDistance = min(minDistance, abs(occurence1[i] - occurence2[j]))
            if occurence1[i] > occurence2[j]:
                j += 1
            else:
                i += 1
        return minDistance



# Your WordDistance object will be instantiated and called as such:
# obj = WordDistance(wordsDict)
# param_1 = obj.shortest(word1,word2)

# class Solution:
#     def to_replace(self, s: str) -> bool:
#         '''
#         >>> Solution.to_replace(Solution, to_replace)
#         to_replace
#         >>> Solution.to_replace(Solution, to_replace)
#         to_replace
#         >>> Solution.to_replace(Solution, to_replace)
#         to_replace
#         '''


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
