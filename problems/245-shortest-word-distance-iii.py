from types import *
from typing import List
from collections import defaultdict
import math

class Solution:
    def shortestWordDistance(self, wordsDict: List[str], word1: str, word2: str) -> int:
        '''
        >>> Solution.shortestWordDistance(Solution, ["practice", "makes", "perfect", "coding", "makes"], "makes", "coding")
        1
        >>> Solution.shortestWordDistance(Solution, ["practice", "makes", "perfect", "coding", "makes"], "makes", "makes")
        3
        '''
        minDistance = math.inf
        if word1 == word2:
            indices = [i for i, x in enumerate(wordsDict) if x == word1]
            # print(indices)
            for i in range(0, len(indices) - 1):
                minDistance = min(indices[i + 1] - indices[i], minDistance)
            return minDistance
        else:
            occurance = defaultdict(list)
            for index, word in enumerate(wordsDict):
                occurance[word].append(index)

            occurance1, occurance2 = occurance[word1], occurance[word2]
            l1, l2 = len(occurance1), len(occurance2)
            i = j = 0
            while i < l1 and j < l2:
                if word1 == word2 and i == j:
                    i += 1
                else:
                    minDistance = min(abs(occurance1[i] - occurance2[j]), minDistance)
                    if occurance1[i] > occurance2[j]:
                        j += 1
                    else:
                        i += 1
            return minDistance


        



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
