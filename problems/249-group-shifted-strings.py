from types import *
from typing import List
from collections import defaultdict

class Solution:
    def groupStrings(self, strings: List[str]) -> List[List[str]]:
        '''
        >>> Solution.groupStrings(Solution, ["abc","bcd","acef","xyz","az","ba","a","z"])
        [["acef"],["a","z"],["abc","bcd","xyz"],["az","ba"]]
        >>> Solution.groupStrings(Solution, ["a"])
        [["a"]]
        '''
        stringGroups = defaultdict(list)

        for s in strings:
            offset = ord(s[0]) - 97
            stringGroups[tuple( (ord(ch) - offset) % 26 for ch in s)].append(s)

        return list(stringGroups.values())



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
