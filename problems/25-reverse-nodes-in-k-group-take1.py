from types import *
from typing import List
from binarytree import Node as TreeNode

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def makeList(arr):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def printList(l: ListNode):
    arr = []
    p = l
    while p is not None:
        # print('### appending', p.val)
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))


class Solution:
    def reverseKGroup(self, head: ListNode, k: int) -> ListNode:
        '''
        >>> printList(Solution.reverseKGroup(Solution, makeList([1,2,3,4,5]), 2))
        2,1,4,3,5
        >>> printList(Solution.reverseKGroup(Solution, makeList([1,2,3,4]), 2))
        2,1,4,3
        >>> printList(Solution.reverseKGroup(Solution, makeList([1,2,3,4,5]), 3))
        3,2,1,4,5
        '''
        def reverseLinkedList(head, k):
            
            prev, curr = None, head
            while k:
                k -= 1
                nxt = curr.next
                curr.next = prev
                prev, curr = curr, nxt
            # print('######')
            # printList(prev)
            # print('######^^')
            return prev

        def reverseKGroup(head, k):
            count = 0
            p = head
            while count < k and p:
                p = p.next
                count += 1
            # print('count=', count)
            if count == k:
                reversed_head = reverseLinkedList(head, k)

                head.next = reverseKGroup(p, k)
                return reversed_head
            return head


        return reverseKGroup(head, k)
        # head.next = rev_head.next
        # rev_head.next = head.next
        # print('###', rev_head.val, head.val)
                    

          


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
