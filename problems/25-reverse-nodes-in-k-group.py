from types import *
from typing import List

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
        

def makeList(arr):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def printList(l: ListNode):
    arr = []
    p = l
    while p is not None:
        # print('### appending', p.val)
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))


def reverseUntil(a: ListNode, b: ListNode):
    prev = None
    curr = a
    nxt = a
    while curr is not b:
        nxt = curr.next
        curr.next = prev
        prev, curr = curr, nxt
    return prev

# printList(reverseUntil(makeList([3,4]), 2))
# printList(reverseKGroup(makeList([1,2]), 2))


def reverseKGroup(head, k):
    p = head
    for _ in range(k):
        if not p:
            return head
        p = p.next
    
    reverse_start = head
    reversedList = reverseUntil(reverse_start, p)
    head.next = reverseKGroup(p, k)

    return reversedList


printList(reverseKGroup(makeList([1,2,3,4]), 2))
printList(reverseKGroup(makeList([1,2]), 2))

# class Solution:
#     def to_replace(self, s: str) -> bool:
#         '''
#         >>> Solution.to_replace(Solution, to_replace)
#         to_replace
#         >>> Solution.to_replace(Solution, to_replace)
#         to_replace
#         >>> Solution.to_replace(Solution, to_replace)
#         to_replace
#         '''


# if __name__ == "__main__":
#     import time
#     start_time = time.time()

#     import doctest
#     doctest.testmod()
#     print("--- %s seconds ---" % (time.time() - start_time))
