class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items):
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)



class Solution:
    def countUnivalSubtrees(self, root: TreeNode) -> int:
        '''
        >>> (Solution.countUnivalSubtrees(Solution, buildTree([5,1,5,5,5,None,5])))
        4
        '''
        if not root:
            return 0

        count = 0
        def isUniValueTree(root): # (True)
            nonlocal count
            
            if not (root.left or root.right):
                count += 1
                return True

            is_uni = True
            if root.left:
                ifLeftUniValueTree = isUniValueTree(root.left)
                is_uni = is_uni and ifLeftUniValueTree and root.left.val == root.val
            if root.right:
                ifRightUniValueTree = isUniValueTree(root.right)
                is_uni = is_uni and ifRightUniValueTree and root.right.val == root.val

            if is_uni:
                count += 1
            return is_uni

        
        isUniValueTree(root)

        return count


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
