from types import *
from typing import List

class Solution:
    def canAttendMeetings(self, intervals: List[List[int]]) -> bool:
        '''
        >>> Solution.canAttendMeetings(Solution, [[0,30],[5,10],[15,20]])
        False
        >>> Solution.canAttendMeetings(Solution, [[7,10],[2,4]])
        True
        '''
        intervals.sort(key=lambda interval:interval[0])
        for i in range(1, len(intervals)):
            if intervals[i][0] < intervals[i-1][1]:
                return False
        return True



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
