from types import *
from typing import List
from collections import defaultdict
import heapq 

class Solution:
    def minMeetingRooms(self, intervals: List[List[int]]) -> int:
        '''
        >>> Solution.minMeetingRooms(Solution, [[0,30],[5,10],[15,20]])
        2
        >>> Solution.minMeetingRooms(Solution, [[7,10],[2,4]])
        1
        '''
        usageDelta = defaultdict(int)
        maxCount = 0

        for startTime, endTime in intervals:
            usageDelta[startTime] += 1
            usageDelta[endTime] -= 1

        count = 0
        for time in sorted(list(usageDelta.keys())):
            count += usageDelta[time]
            maxCount = max(maxCount, count)

        return maxCount
        


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
