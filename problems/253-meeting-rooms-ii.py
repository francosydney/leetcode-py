from types import *
from typing import List
from collections import defaultdict


class Solution:
    def minMeetingRooms(self, intervals: List[List[int]]) -> int:
        '''
        >>> Solution.minMeetingRooms(Solution, [[0, 30],[5, 10],[15, 20]])
        2
        >>> Solution.minMeetingRooms(Solution, [[7,10],[2,4]])
        1
        >>> Solution.minMeetingRooms(Solution, [])
        0
        '''
        # trend = defaultdict(int)
        # maxParallel = 0
        # currentParallel = 0
        # maxT = 0
        # for start, end in intervals:
        #     trend[start] += 1
        #     trend[end] -= 1
        #     if end > maxT:
        #         maxT = end
        # for i in range(maxT + 1):
        #     currentParallel += trend[i]
        #     if currentParallel > maxParallel:
        #         maxParallel = currentParallel

        # return maxParallel

        maxParallel = 0
        currentParallel = 0
        maxT = 0

        for start, end in intervals:
            maxT = max(maxT, end)
        trend = [0] * (maxT + 1)

        for start, end in intervals:
            trend[start] += 1
            trend[end] -= 1

        for i in range(maxT + 1):
            currentParallel += trend[i]
            if currentParallel > maxParallel:
                maxParallel = currentParallel

        return maxParallel
        


if __name__ == "__main__":
    import doctest
    doctest.testmod()
