from types import *
from typing import List

class Solution:
    def getFactors(self, n: int) -> List[List[int]]:
        '''
        >>> Solution.getFactors(Solution, 1)
        []
        >>> Solution.getFactors(Solution, 12)
        [[2,6],[3,4],[2,2,3]]
        >>> Solution.getFactors(Solution, 25)
        []
        >>> Solution.getFactors(Solution, 32)
        [[2,16],[4,8],[2,2,8],[2,4,4],[2,2,2,4],[2,2,2,2,2]]
        '''
        results = []

        def backtrack(num, dividedFactors = []):
            if dividedFactors:
                results.append(dividedFactors + [num])
            
            for i in range(2 if not dividedFactors else dividedFactors[-1], int(num ** 0.5) + 1):
                if num % i == 0:
                    dividedFactors.append(i)
                    backtrack(num // i, dividedFactors)
                    dividedFactors.pop()


        backtrack(n)

        return results



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
