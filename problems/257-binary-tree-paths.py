from typing import List

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items):
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)



class Solution:
    def binaryTreePaths(self, root: TreeNode) -> List[str]:
        '''
        >>> Solution.binaryTreePaths(Solution, buildTree([1,2,3,None,5]))
        to_replace
        >>> Solution.binaryTreePaths(Solution, buildTree([1]))
        to_replace
        '''
        paths = []
        
        def findLeaves(root, path = ''):
            path += str(root.val)
            if not (root.left or root.right):
                paths.append(path)
                return
            path += '->'
            if root.left:
                findLeaves(root.left, path)
            if root.right:
                findLeaves(root.right, path)
            
        findLeaves(root)
        return paths
            


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
