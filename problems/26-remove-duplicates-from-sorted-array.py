from types import *
from typing import List

class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        '''
        >>> Solution.removeDuplicates(Solution, [0,0,1,1,1,2,2,3,3,4])
        5
        '''
        i = 0
        l = len(nums)

        lastNum = None
        for num in nums:
            if num == lastNum:
                pass
            else:
                lastNum = num
                nums[i] = num
                i += 1
        return i
            

        



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
