from types import *
from typing import List
from collections import defaultdict, deque
from itertools import groupby


class Solution:
    def alienOrder(self, words: List[str]) -> str:
        '''
        >>> Solution.alienOrder(Solution, [ "wrt", "wrf", "er", "ett", "rftt" ])
        'wertf'
        >>> Solution.alienOrder(Solution, [ "z", "x" ])
        'zx'
        >>> Solution.alienOrder(Solution, [ "z", "x", "z" ])
        ''
        >>> Solution.alienOrder(Solution, [ "z", "z" ])
        'z'
        >>> Solution.alienOrder(Solution, [ "z" ])
        'z'
        >>> Solution.alienOrder(Solution, [ "abc", "abd" ])
        'abcd'
        >>> Solution.alienOrder(Solution, [ "abc", "ab" ])
        ''
        '''
        adjacencyDict = defaultdict(set)
        
        disorder = False
        def extractOrders(words):
            nonlocal disorder

            groups = list(groupby(words, lambda x: x[0]))
            adjacencyDict[groups[0][0]]
            for i, ch1, ch2 in [(i, groups[i][0], groups[i+1][0]) for i in range(len(groups) - 1)]:
                adjacencyDict[ch1].add(ch2)
                adjacencyDict[ch2]

            for k, subWordsItr in groupby(words, lambda x: x[0]):
                subWords = list(map(lambda x: x[1:], list(subWordsItr)))
                for i in range(1, len(subWords)):
                    if len(subWords[i-1]) is 1 and len(subWords[i]) is 0:
                        disorder = True
                subWords = list(filter(lambda x: x, subWords))
                if len(subWords) >= 2:
                    extractOrders(subWords)

        extractOrders(words)

        if disorder:
            return ''

        stack = []
        visited = set()
        def pushTargetsAndSelf(ch, upperchainChars):
            visited.add(ch)
            for targetChar in adjacencyDict[ch]:
                if targetChar in upperchainChars:
                    return False
                if targetChar not in visited:
                    if not pushTargetsAndSelf(targetChar, upperchainChars.union({targetChar})):
                        return False
            stack.append(ch)
            return True

        for ch in adjacencyDict:
            if ch not in visited:
                if not pushTargetsAndSelf(ch, {ch}):
                    return ''
        
        order = ''.join(stack[::-1])
        allChars = set(''.join(words))
        order = order + ''.join(allChars.difference(set(order)))
        
        return order


if __name__ == "__main__":
    import doctest
    doctest.testmod()
