from types import *
from typing import List

class Solution:
    def removeElement(self, nums: List[int], val: int) -> int:
        '''
        >>> print(Solution.removeElement(Solution, [0,1,2,2,3,0,4,2], 2))
        xxxxx
        '''
        i = 0
        l = len(nums)
        for j in range(0, l):
            if nums[j] != val:
                nums[i] = nums[j]
                i += 1
        print(nums)
        return i



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
