from types import *
from typing import List

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items) or items[index] is None:
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)



class Solution:
    def closestValue(self, root: TreeNode, target: float) -> int:
        '''
        >>> (Solution.closestValue(Solution, buildTree([4,2,5,1,3]), 3.71))
        4
        >>> (Solution.closestValue(Solution, buildTree([1]), 4.42))
        1
        '''
        closest = root.val
        while root:
            if root.val == target:
                return root.val
            closest = min(root.val, closest, key=lambda x: abs(x - target))
            root = root.left if target < root.val else root.right
        return closest




if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
