from types import *
from typing import List

class Solution:
    def numberToWords(self, num: int) -> str:
        '''
        >>> Solution.numberToWords(Solution, 123)
        'One Hundred Twenty Three'
        >>> Solution.numberToWords(Solution, 12345)
        'Twelve Thousand Three Hundred Forty Five'
        >>> Solution.numberToWords(Solution, 1234567)
        'One Million Two Hundred Thirty Four Thousand Five Hundred Sixty Seven'
        >>> Solution.numberToWords(Solution, 1234567891)
        'One Billion Two Hundred Thirty Four Million Five Hundred Sixty Seven Thousand Eight Hundred Ninety One'
        >>> Solution.numberToWords(Solution, 100)
        'One Hundred'
        >>> Solution.numberToWords(Solution, 0)
        'Zero'
        >>> Solution.numberToWords(Solution, 1000000)
        'One Million'
        >>> Solution.numberToWords(Solution, 100000)
        'One Hundred Thousand'
        '''
        numbers = [ \
            '', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', \
            'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen' \
        ]
        tens = [ \
            '', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety' \
        ]
        units = ['', 'Thousand', 'Million', 'Billion']

        if num is 0:
            return 'Zero'

        def reverseList(l):
            return list(reversed(l))
        def filterEmpty(l):
            return list(filter(lambda item: item is not '', l))

        reversedNumCharList = reverseList(list(str(num)))
        reversedSplitedNum = [int(''.join(reverseList(reversedNumCharList[startI: startI + 3])))
                      for startI in range(0, len(reversedNumCharList), 3)]
        
        # print(reversedSplitedNum)

        results = []
        for num, unit in zip(reversedSplitedNum, units):
            # print(num, unit)
            subResults = []
            if num >= 100:
                subResults.append(numbers[int(num/100)] + ' Hundred')
            numLeft = num % 100

            if numLeft > 0:
                if numLeft < 20:
                    subResults.append(numbers[numLeft])
                else:
                    subResults.append(tens[int(numLeft/10)])
                    subResults.append(numbers[numLeft % 10])

            if num > 0:
                subResults.append(unit)

            results.append(' '.join(filterEmpty(subResults)))
        
        return ' '.join(reverseList(filterEmpty(results)))



if __name__ == "__main__":
    import doctest
    doctest.testmod()
