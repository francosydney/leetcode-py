from types import *
from typing import List

class Solution:
    def hIndex(self, citations: List[int]) -> int:
        '''
        >>> Solution.hIndex(Solution, [3,0,6,1,5])
        3
        '''
        citations.sort(reverse=True)
        i = 0
        l = len(citations)
        while i < l and citations[i] > i:
            i += 1
        return i


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
