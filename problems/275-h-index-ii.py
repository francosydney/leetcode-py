from types import *
from typing import List

class Solution:
    def hIndex(self, citations: List[int]) -> int:
        '''
        >>> Solution.to_replace(Solution, to_replace)
        to_replace
        >>> Solution.to_replace(Solution, to_replace)
        to_replace
        >>> Solution.to_replace(Solution, to_replace)
        to_replace
        '''
        l = len(citations)

        startIndex, endIndex = 0, l - 1
        while startIndex >= endIndex:
            i = (startIndex + endIndex) // 2
            # if citations[i]
            if citations[i] <= l - i - 1 and citations[i + 1] > l - i:
                return l - i
            else:
                

        
        # def findHIndex(startIndex, endIndex):
        #     if startIndex > endIndex:
        #         return
        #     midIndex = (startIndex + endIndex) // 2



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
