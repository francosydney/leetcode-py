# The knows API is already defined for you.
# return a bool, whether a knows b

grid = [[1,1,0],[0,1,0],[1,1,1]]

def knows(a: int, b: int) -> bool:
    return bool(grid[a][b])



class Solution:
    def findCelebrity(self, n: int) -> int:
        '''
        >>> (Solution.findCelebrity(Solution, 3))
        1
        '''
        i = 0
        while i < n:
            j = 0
            while j < n:
                if not (knows(j, i) and (i == j or not knows(i, j))):
                    break
                j += 1
            if j == n:
                return i
            i += 1

        return -1

if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
