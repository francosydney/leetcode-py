from types import *
from typing import List

# def isBadVersion(version):

class Solution:
    def firstBadVersion(self, n):
        '''
        >>> Solution.to_replace(Solution, to_replace)
        to_replace
        >>> Solution.to_replace(Solution, to_replace)
        to_replace
        >>> Solution.to_replace(Solution, to_replace)
        to_replace
        '''
        low = 1
        high = n

        while low <= high:
            mid = (low + high) // 2
            if isBadVersion(mid):
                if mid == 1 or not isBadVersion(mid):
                    return mid
                high = mid - 1
            else:
                low = mid + 1
        



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
