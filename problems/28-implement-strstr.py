from types import *
from typing import List

class Solution:
    def strStr(self, haystack: str, needle: str) -> int:
        '''
        >>> Solution.strStr(Solution, 'hello', 'll')
        2
        >>> Solution.strStr(Solution, 'aaaaa', 'bba')
        -1
        >>> Solution.strStr(Solution, '', '')
        0
        '''
        if not len(needle):
            return 0
        
        def isSplitFromIndexStrEqualTo(str1, index, str2):
            i, j = index, 0
            if len(str1) - index < len(str2):
                return False
            while j < len(str2):
                if str1[i] != str2[j]:
                    return False
                i += 1
                j += 1

            return True

        for i in range(len(haystack) - len(needle) + 1):
            if isSplitFromIndexStrEqualTo(haystack, i, needle):
                return i
        return -1


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
