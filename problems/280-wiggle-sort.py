from types import *
from typing import List

class Solution:
    def wiggleSort(self, nums: List[int]) -> None:
        '''
        >>> Solution.wiggleSort(Solution, [3,5,2,1,6,4])
        [3,5,1,6,2,4]
        >>> Solution.wiggleSort(Solution, [6,6,5,6,3,8])
        [6,6,5,6,3,8]
        '''
        nums.sort()
        i, l = 1, len(nums)
        while i + 1 < l:
            nums[i], nums[i+1] = nums[i+1], nums[i]
            i += 2
        return nums



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
