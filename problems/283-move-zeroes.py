from types import *
from typing import List

class Solution:
    def moveZeroes(self, nums: List[int]) -> None:
        '''
        >>> Solution.moveZeroes(Solution, [0,1,0,3,12])
        [1, 3, 12, 0, 0]
        >>> Solution.moveZeroes(Solution, [0])
        [0]
        '''
        l = len(nums)
        pointer = 0

        for i in range(l):
            if nums[i] != 0:
                nums[i], nums[pointer] = nums[pointer], nums[i]
                pointer += 1
        return nums
        


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
