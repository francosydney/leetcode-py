from types import *
from typing import List

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items) or items[index] is None:
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)



class Solution:
    def inorderSuccessor(self, root: 'TreeNode', p: 'TreeNode') -> 'TreeNode':
        '''
        >>> printTree(Solution.inorderSuccessor(Solution, buildTree([5,3,6,2,4,None,None,1]) , buildTree([1]) ))
        2
        '''
        #1: find the node
        stack = []
        while root and root.val != p.val:
            if p.val < root.val:
                stack.append(root)
                root = root.left
            else:
                root = root.right
        
        if root.right:
            node = root.right
            while node.left:
                node = node.left
            return node
        
        if stack:
            return stack.pop()

        return None


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
