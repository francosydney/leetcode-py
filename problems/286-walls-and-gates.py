from types import *
from typing import List
from collections import deque

class Solution:
    def wallsAndGates(self, rooms: List[List[int]]) -> None:
        '''
        >>> Solution.wallsAndGates(Solution, [[2147483647,-1,0,2147483647],[2147483647,2147483647,2147483647,-1],[2147483647,-1,2147483647,-1],[0,-1,2147483647,2147483647]])
        [[3, -1, 0, 1], [2, 2, 1, -1], [1, -1, 2, -1], [0, -1, 3, 4]]
        >>> Solution.wallsAndGates(Solution, [[-1]])
        [[-1]]
        >>> Solution.wallsAndGates(Solution, [[0]])
        [[0]]
        >>> Solution.wallsAndGates(Solution, [[2147483647]])
        [[2147483647]]
        '''
        WALL = -1
        GATE = 0
        ROOM = 2147483647
        # count = {WALL: 0, GATE: 0, ROOM: 0}
        unvisitedRooms = set()

        nodes = deque([])
        for r, row in enumerate(rooms):
            for c, room in enumerate(row):
                if room == GATE:
                    nodes.append((r, c))
                elif room == ROOM:
                    unvisitedRooms.add((r, c))
        # print(unvisitedRooms)

        r = len(rooms)
        c = len(rooms[0])
        def inBound(pos):
            return 0 <= pos[0] < r and 0 <= pos[1] < c
        deltas = [(-1, 0), (1, 0), (0, -1), (0, 1)]
        def neighborsOf(pos):
            return filter(inBound, [(pos[0]+d[0], pos[1]+d[1]) for d in deltas])

        while nodes:
            node = nodes.popleft()
            nodeVal = rooms[node[0]][node[1]]

            for neighbor in neighborsOf(node):
                if neighbor in unvisitedRooms:
                    unvisitedRooms.remove(neighbor)
                    nodes.append(neighbor)
                    rooms[neighbor[0]][neighbor[1]] = nodeVal + 1
                    

        return rooms


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
