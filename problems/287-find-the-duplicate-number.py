from types import *
from typing import List

class Solution:
    def findDuplicate(self, nums: List[int]) -> int:
        '''
        >>> Solution.findDuplicate(Solution, [1,3,4,2,2])
        2
        >>> Solution.findDuplicate(Solution, [3,1,3,4,2])
        3
        >>> Solution.findDuplicate(Solution, [1,1])
        1
        >>> Solution.findDuplicate(Solution, [1,1,2])
        1
        '''
        bag = set()
        for num in nums:
            if num in bag:
                return num
            bag.add(num)
        
            


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
