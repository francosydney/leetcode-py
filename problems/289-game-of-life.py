from types import *
from typing import List
from collections import defaultdict

class Solution:
    def gameOfLife(self, board: List[List[int]]) -> None:
        '''
        >>> Solution.gameOfLife(Solution, [[0,1,0],[0,0,1],[1,1,1],[0,0,0]])
        [[0, 0, 0], [1, 0, 1], [0, 1, 1], [0, 1, 0]]
        >>> Solution.gameOfLife(Solution, [[1,1],[1,0]])
        [[1, 1], [1, 1]]
        '''
        rowNum = len(board)
        colNum = len(board[0])
        
        liveNeighborsCount = [[0] * colNum for _ in range(rowNum)]
        neighborsVectors = [(-1,0),(-1,-1),(-1,1),(0,-1),(0,1),(1,0),(1,-1),(1,1)]

        def withInBoundary(cell):
            return 0 <= cell[0] < rowNum and 0 <= cell[1] < colNum

        for i in range(rowNum):
            for j in range(colNum):
                neighbors = list(filter(withInBoundary, [(i + xd, j + yd) for xd, yd in neighborsVectors]))
                liveNeighborsCount[i][j] = sum(map(lambda c: board[c[0]][c[1]] , neighbors))

        for i in range(rowNum):
            for j in range(colNum):
                if liveNeighborsCount[i][j] == 3:
                    board[i][j] = 1
                elif liveNeighborsCount[i][j] == 2 and board[i][j] == 1:
                    pass
                else:
                    board[i][j] = 0

        print(board)

if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
