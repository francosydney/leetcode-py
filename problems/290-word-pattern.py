from types import *
from typing import List

class Solution:
    def wordPattern(self, pattern: str, s: str) -> bool:
        '''
        >>> Solution.wordPattern(Solution, "abba", "dog cat cat dog")
        True
        >>> Solution.wordPattern(Solution, "abba", "dog cat cat fish")
        False
        >>> Solution.wordPattern(Solution, "aaaa", "dog cat cat dog")
        False
        >>> Solution.wordPattern(Solution, "abba", "dog dog dog dog")
        False
        '''
        a = list(pattern)
        b = list(s.split(' '))

        if len(a) != len(b):
            return False

        a2b = {}
        b2a = {}

        for ch1, ch2 in zip(a, b):
            if ch1 not in a2b:
                if ch2 in b2a:
                    return False
                else:
                    a2b[ch1] = ch2
                    b2a[ch2] = ch1
            else:
                if a2b[ch1] != ch2:
                    return False

        return True
            


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
