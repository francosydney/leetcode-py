from types import *
from typing import List
from collections import Counter

class Solution:
    def wordPatternMatch(self, pattern: str, s: str) -> bool:
        '''
        >>> Solution.wordPatternMatch(Solution, "abab", "redblueredblue")
        True
        >>> Solution.wordPatternMatch(Solution, "aaaa", "asdasdasdasd")
        True
        >>> Solution.wordPatternMatch(Solution, "abab", "asdasdasdasd")
        True
        >>> Solution.wordPatternMatch(Solution, "aabb", "xyzabcxzyabc")
        False
        >>> Solution.wordPatternMatch(Solution, "sucks", "teezmmmmteez")
        False
        '''
        patternLength = len(pattern)
        stringLength = len(s)
        occurenceInPattern = Counter(pattern)

        flag = False

        def backtrack(mapping = {}, destinations = set(), patternIndex = 0, stringIndex = 0):
            # print(mapping, destinations, patternIndex, stringIndex)
            nonlocal flag

            if patternIndex == patternLength:
                if stringIndex == stringLength:
                    # print('!!!', mapping)
                    flag = True
                return
            
            for endIndex in range(stringIndex, min(stringIndex + stringLength // occurenceInPattern[pattern[patternIndex]], stringLength)):
                mappedTo = s[stringIndex:endIndex + 1]

                #1: mapping exist and matches
                if mapping.get(pattern[patternIndex]) == mappedTo:
                    backtrack(mapping, destinations, patternIndex + 1, endIndex + 1)
                #2: mapping doesn't exist 2-way
                if pattern[patternIndex] not in mapping and mappedTo not in destinations:
                    mapping[pattern[patternIndex]] = mappedTo
                    destinations.add(mappedTo)
                    backtrack(mapping, destinations, patternIndex + 1, endIndex + 1)
                    del mapping[pattern[patternIndex]]
                    destinations.remove(mappedTo)

                

        backtrack()

        return flag
        



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
