from types import *
from typing import List

class Solution:
    def generatePossibleNextMoves(self, currentState: str) -> List[str]:
        '''
        >>> Solution.generatePossibleNextMoves(Solution, "++++")
        ["--++","+--+","++--"]
        >>> Solution.generatePossibleNextMoves(Solution,  "+")
        []
        '''
        l = len(currentState)
        if l < 2:
            return []
        
        currentStateList = list(currentState)
        results = []

        for i in range(1, l):
            if currentStateList[i-1] == currentStateList[i] == '+':
                currentStateList[i-1] = currentStateList[i] = '-'
                results.append(''.join(currentStateList))
                currentStateList[i-1] = currentStateList[i] = '+'

        return results


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
