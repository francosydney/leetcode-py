from types import *
from typing import List

class Solution:
    def canWin(self, s: str) -> bool:
        '''
        >>> Solution.canWin(Solution, "++++")
        True
        >>> Solution.canWin(Solution, "+++++")
        False
        >>> Solution.canWin(Solution, "++++++")
        True
        >>> Solution.canWin(Solution, "++-++")
        False
        >>> Solution.canWin(Solution, "+")
        False
        >>> Solution.canWin(Solution, "+++++++++")
        False
        '''
        l = len(s)
        if l < 2:
            return False
        stringList = list(s)

        def getPossibleMoves():
            movesStartingAt = set()
            for i in range(l-1):
                if stringList[i] == stringList[i + 1] == '+':
                    movesStartingAt.add(i)
            return movesStartingAt

        

        '''
        round1Moves = getPossibleMoves()
        if len(round1Moves) == 0: # round 1 is not empty
            return False

        for move in round1Moves:
            stringList[move] = stringList[move + 1] = '-'

            round2moves = getPossibleMoves()
            if len(round2moves) == 0: # taken this move, and the opponent has no move to go
                return True
            for move2 in round2moves:
                stringList[move2] = stringList[move2 + 1] = '-'

                round3moves = getPossibleMoves()
                if len(round3moves):
                    return True

                stringList[move2] = stringList[move2 + 1] = '+'


            stringList[move] = stringList[move + 1] = '+'

        return False
        '''

if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
