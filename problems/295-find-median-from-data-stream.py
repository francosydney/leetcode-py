from types import *
from typing import List
import heapq

class MedianFinder:
    def __init__(self):
        """
        initialize your data structure here.
        """
        self.lower = [] # max heap (need reverted key!)
        self.higher = [] # min heap
        

    # def addNum(self, num: int) -> None:
    #     # heapq.heappush(self.sortedNums, num)
    #     heapq.heappush(self.lower, (-num, num))

    #     _, largestInLower = heapq.heappop(self.lower)
    #     heapq.heappush(self.higher, (largestInLower, largestInLower))

    #     if len(self.higher) > len(self.lower):
    #         _, smallestInHigher = heapq.heappop(self.higher)
    #         heapq.heappush(self.lower, (-smallestInHigher, smallestInHigher))


    # def findMedian(self) -> float:
    #     l_lower, l_higher = len(self.lower), len(self.higher)
    #     if l_lower > l_higher:
    #         return self.lower[0][1]
    #     else:
    #         return (self.lower[0][1] + self.higher[0][1]) / 2


    def addNum(self, num: int) -> None:
        small, big = self.two_heaps
        heapq.heappush(small, -heapq.heappushpop(big, num))
        if len(big) < len(small):
            heapq.heappush(big, -heapq.heappop(small))

    def findMedian(self) -> float:
        small, big = self.two_heaps
        if len(small) < len(big):
            return float(big[0])
        return (big[0] - small[0]) / 2
        

medianFinder = MedianFinder()
medianFinder.addNum(1)
medianFinder.addNum(5)
# print(medianFinder.lower[0][1], medianFinder.higher[0][1])

print(medianFinder.findMedian())
medianFinder.addNum(3)
medianFinder.addNum(4)

medianFinder.addNum(2)

medianFinder.addNum(-2)

medianFinder.addNum(7)

medianFinder.addNum(0)

medianFinder.addNum(1)
print(medianFinder.findMedian())

