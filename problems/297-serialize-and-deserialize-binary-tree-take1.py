from types import *
from typing import List
from collections import deque

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items) or items[index] is None:
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)


# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Codec:

    def serialize(self, root):
        '''
        >>> Codec.serialize(Codec, buildTree([1,2,3,4,5,None,7]))
        '1,2,4,None,None,5,None,None,3,None,7,None,None,'
        '''
        def serielise(root):
            if not root:
                return 'None,'
            s = str(root.val) + ','
            return s + serielise(root.left) + serielise(root.right)
        return serielise(root)
        
    def deserialize(self, data):
        """
        >>> printTree(Codec.deserialize(Codec, Codec.serialize(Codec, buildTree([1,2,3,4,5,None,7]))))
        2
        """
        dataList = deque(data.split(',')[:-1])

        def deserialise():
            nonlocal dataList
            if not dataList:
                return None
            val = dataList.popleft()
            if val == 'None':
                return None
            else:
                node = TreeNode(val)
                node.left = deserialise()
                node.right = deserialise()
                return node
        
        return deserialise()

            
        

# Your Codec object will be instantiated and called as such:
# ser = Codec()
# deser = Codec()
# ans = deser.deserialize(ser.serialize(root))


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
