from types import *
from typing import List
import base64
from heapq import heappush, heappop

# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Codec:
    '''
    >>> a1 = TreeNode(1)
    >>> b2 = TreeNode(2)
    >>> c3 = TreeNode(3)
    >>> d4 = TreeNode(4)
    >>> e5 = TreeNode(5)
    >>> f6 = TreeNode(6)
    >>> g7 = TreeNode(7)
    >>> a1.left = b2
    >>> b2.right = d4
    >>> d4.left = g7
    >>> a1.right = c3
    >>> c3.left = e5
    >>> c3.right = f6
    >>> Codec.serialize(Codec, Codec.deserialize(Codec, Codec.serialize(Codec, a1)))
    '1(2()(4(7()())()))(3(5()())(6()()))'
    '''
    def serialize(self, root):
        """Encodes a tree to a single string.
        
        :type root: TreeNode
        :rtype: str
        """
        if root is None:
            return ''
        else:
            a = f'{root.val}({self.serialize(self, root.left)})({self.serialize(self, root.right)})'
            return a

    def deserialize(self, data):
        """Decodes your encoded data to tree.
        
        :type data: str
        :rtype: TreeNode
        """
        if data is '':
            return None
        
        i = 0
        while data[i] is not '(':
            i += 1
        node = TreeNode(int(data[:i]))
        NumOneIndex = i + 1

        count = 1
        while count > 0:
            i += 1
            if data[i] is '(':
                count += 1
            elif data[i] is ')':
                count -= 1
        node.left = self.deserialize(self, data[NumOneIndex:i])
        node.right = self.deserialize(self, data[i+2:-1])
        return node


if __name__ == "__main__":
    import doctest
    doctest.testmod()
