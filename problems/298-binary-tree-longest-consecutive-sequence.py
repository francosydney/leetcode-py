class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items):
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)



class Solution:
    def longestConsecutive(self, root: TreeNode) -> int:
        '''
        >>> (Solution.longestConsecutive(Solution, buildTree([1,2,3])))
        2
        >>> (Solution.longestConsecutive(Solution, buildTree([1,3,3])))
        1
        '''
        maxLength = 0
        def findConsecutiveTil(root, pastValue = None, consecutiveLength = 0):
            nonlocal maxLength
            if root:
                newLength = 1 if (consecutiveLength == 0) else (consecutiveLength + 1 if (root.val == pastValue + 1) else 1)
                if newLength > maxLength:
                    maxLength = newLength
                if root.left:
                    findConsecutiveTil(root.left, root.val, newLength)
                if root.right:
                    findConsecutiveTil(root.right, root.val, newLength)
        findConsecutiveTil(root)
        return maxLength


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
