from types import *
from typing import List
from collections import defaultdict

class Solution:
    def getHint(self, secret: str, guess: str) -> str:
        '''
        >>> Solution.getHint(Solution, "1807", "7810")
        1A3B
        >>> Solution.getHint(Solution, "1123", "0111")
        1A1B
        >>> Solution.getHint(Solution, "1", "0")
        0A0B
        '''
        nonBullDigitsInSecret = defaultdict(int)
        nonBullDigitsInGuess = defaultdict(int)
        bullNumber = 0
        for i in range(len(secret)):
            if secret[i] == guess[i]:
                bullNumber += 1
            else:
                nonBullDigitsInSecret[(secret[i])] += 1
                nonBullDigitsInGuess[(guess[i])] += 1

        cowNumber = 0
        for key in nonBullDigitsInGuess:
            if key in nonBullDigitsInSecret:
                cowNumber += min(nonBullDigitsInSecret[key], nonBullDigitsInGuess[key])
        
        return f'{bullNumber}A{cowNumber}B'




if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
