from types import *
from typing import List

class Solution:
    def lengthOfLIS(self, nums: List[int]) -> int:
        '''
        >>> Solution.lengthOfLIS(Solution, [0,1,0,3,2,3])
        4
        >>> Solution.lengthOfLIS(Solution, [7,7,7,7,7,7,7])
        1
        >>> Solution.lengthOfLIS(Solution, [10,9,2,5,3,7,101,18])
        4
        >>> Solution.lengthOfLIS(Solution, [1,3,6,7,9,4,10,5,6])
        6
        '''
        l = len(nums)
        longestSizeUntil = [1] * l

        for i in range(1, l):
            longestSizeUntilThis = longestSizeUntil[i]
            for j in range(0, i):
                if nums[j] < nums[i]:
                    longestSizeUntilThis = max(longestSizeUntilThis, longestSizeUntil[j] + 1)
            longestSizeUntil[i] = longestSizeUntilThis

        return max(longestSizeUntil)





if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
