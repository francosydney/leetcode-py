from types import *
from typing import List

class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        '''
        >>> Solution.maxProfit(Solution, [1,2,3,0,2])
        3
        >>> Solution.maxProfit(Solution, [1])
        0
        '''
        l = len(prices)
        MP = [0] * (l + 2) # MP[i]: profit from i

        for i in range(l - 1, -1, -1):
            c1 = 0
            for sellIndex in range(i + 1, l):
                profit = (prices[sellIndex] - prices[i]) + MP[sellIndex+2]
                c1 = max(profit, c1)
            c2 = MP[i + 1]

            MP[i] = max(c1, c2)
            
        return MP[0]




if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
