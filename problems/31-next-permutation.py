from types import *
from typing import List
from math import inf

class Solution:
    def nextPermutation(self, nums: List[int]) -> None:
        '''
        >>> Solution.nextPermutation(Solution, [1,2,3])
        [1,3,2]
        >>> Solution.nextPermutation(Solution, [3,2,1])
        [1,2,3]
        >>> Solution.nextPermutation(Solution, [1,1,5])
        [1,5,1]
        >>> Solution.nextPermutation(Solution, [1])
        [1]
        >>> Solution.nextPermutation(Solution, [3,2,1,3,2,1])
        [1]
        '''
        l = len(nums)
        i = l - 2
        while i >= 0 and nums[i] >= nums[i + 1]:
            i -= 1

        if i >= 0:
            j = i
            while j < l - 1 and nums[j + 1] > nums[i]:
                j = j + 1
            nums[i], nums[j] = nums[j], nums[i]

        start = i + 1
        end = l - 1
        while start < end:
            nums[start], nums[end] = nums[end], nums[start]
            start += 1
            end -= 1
            
        return nums



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
