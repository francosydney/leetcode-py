from types import *
from typing import List
from heapq import heappush, heappop

class Solution:
    def nthSuperUglyNumber(self, n: int, primes: List[int]) -> int:
        '''
        >>> Solution.nthSuperUglyNumber(Solution, 12, [2,7,13,19])
        32
        >>> Solution.nthSuperUglyNumber(Solution, 1, [2,3,5])
        1
        '''
        smallest = 1
        inQ = {1}
        q = [1]

        for i in range(n):
            num = heappop(q)

            for nextNum in [prime * num for prime in primes]:
                if nextNum not in inQ:
                    inQ.add(nextNum)
                    heappush(q, nextNum)
            smallest = num

        return smallest
        



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
