from types import *
from typing import List
from collections import defaultdict
from heapq import heappush, heappop

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items) or items[index] is None:
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)



class Solution:
    def verticalOrder(self, root: TreeNode) -> List[List[int]]:
        '''
        >>> (Solution.verticalOrder(Solution, buildTree([3,9,8,4,0,1,7])))
        [[4], [9], [3, 0, 1], [8], [7]]
        >>> (Solution.verticalOrder(Solution, buildTree([3,9,8,4,0,1,7,None,None,None,2,5])))
        [[4], [9, 5], [3, 0, 1], [8, 2], [7]]
        '''
        if not root:
            return []
        visited = defaultdict(list)
        minCol = maxCol = 0
        
        nodes = [(root, 0)]
        while nodes:
            new_nodes = []
            for node, col in nodes:
                visited[col].append(node.val)
                minCol = min(minCol, col)
                maxCol = max(maxCol, col)
                if node.left:
                    new_nodes.append((node.left, col - 1))
                if node.right:
                    new_nodes.append((node.right, col + 1))
            nodes = new_nodes
        
        return [visited[col] for col in range(minCol, maxCol + 1)]




        




if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
