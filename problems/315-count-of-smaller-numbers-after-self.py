from types import *
from typing import List
from heapq import heappush, heappop
# from collections import defaultdict

class Solution:
    def countSmaller(self, nums: List[int]) -> List[int]:
        '''
        >>> Solution.countSmaller(Solution, [5,2,6,1])
        [2, 1, 1, 0]
        >>> Solution.countSmaller(Solution, [-1])
        [0]
        >>> Solution.countSmaller(Solution, [-1,-1])
        [0, 0]
        '''

        l = len(nums)
        result = [0] * l

        for i in range(l - 1, -1, -1):
            for j in range(0, i):
                if nums[j] > nums[i]:
                    result[j] += 1
        return result



        # use totalCount to store smaller
        # use hashMap to store { value -> [indices] }
        #  use a priority queue to store keyes from small to large

        # l = len(nums)
        # result = [0] * l
        # keys = []
        # indicesOfValue = {}

        # for index, num in enumerate(nums):
        #     if num in indicesOfValue:
        #         indicesOfValue[num].append(index)
        #     else:
        #         indicesOfValue[num] = [index]
        #         heappush(keys, index)
        
        # totalAmount = 0
        # for num in keys:
            

        # print(keys, indicesOfValue) 

        




if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
