from types import *
from typing import List

class Solution:
    def wiggleSort(self, nums: List[int]) -> None:
        '''
        >>> Solution.wiggleSort(Solution, [1,5,1,1,6,4])
        [1, 4, 1, 5, 1, 6]
        >>> Solution.wiggleSort(Solution, [1,3,2,2,3,1])
        [1, 2, 1, 3, 2, 3]
        >>> Solution.wiggleSort(Solution, [1,2,3,4,5])
        [1, 4, 2, 5, 3]
        >>> Solution.wiggleSort(Solution, [1])
        [1]
        >>> Solution.wiggleSort(Solution, [1,2])
        [1, 2]
        >>> Solution.wiggleSort(Solution, [4, 5, 5, 6])
        [1, 2]
        '''
        l = len(nums)
        sortedNums = sorted(nums)

        d = l // 2
        strictMedian = float(sortedNums[d]) if l % 2 else (sortedNums[d-1] + sortedNums[d]) / 2


        return strictMedian



# 1,2,3,4,5,6

# 1,6,2,5,3,4




if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
