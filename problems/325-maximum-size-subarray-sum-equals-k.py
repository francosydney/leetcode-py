from types import *
from typing import List

class Solution:
    def maxSubArrayLen(self, nums: List[int], k: int) -> int:
        '''
        >>> Solution.maxSubArrayLen(Solution, [1,-1,5,-2,3], 3)
        4
        >>> Solution.maxSubArrayLen(Solution, [-2,-1,2,1], 1)
        2
        '''
        sums = {0:-1}
        currentSum = 0
        maxLen = 0
        for index, num in enumerate(nums):
            currentSum += num

            if currentSum - k in sums:
                maxLen = max(maxLen, index - sums[currentSum - k])

            if currentSum not in sums:
                sums[currentSum] = index

        return maxLen

        # for index, num in enumerate(nums):
            # if 
        # print(sums)



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
