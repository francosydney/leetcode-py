from types import *
from typing import List
from binarytree import Node as TreeNode

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def makeList(arr):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def printList(l):
    arr = []
    p = l
    while p is not None:
        # print('### appending', p.val)
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))


class Solution:
    def oddEvenList(self, head: ListNode) -> ListNode:
        '''
        >>> printList(Solution.oddEvenList(Solution, makeList([1,2,3,4,5])))
        1,3,5,2,4
        >>> printList(Solution.oddEvenList(Solution, makeList([2,1,3,5,6,4,7])))
        2,3,6,7,1,5,4
        '''
        if not head:
            return None
        p1 = head
        p2 = evenHeader = head.next
        while p2 and p2.next:
            p1.next = p2.next
            p1 = p1.next
            p2.next = p1.next
            p2 = p2.next
        p1.next = evenHeader
        return head


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
