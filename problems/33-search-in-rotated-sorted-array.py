from types import *
from typing import List

class Solution:
    def search(self, nums: List[int], target: int) -> int:
        '''
        >>> Solution.search(Solution, [4,5,6,7,0,1,2], 0)
        4
        >>> Solution.search(Solution, [4,5,6,7,0,1,2], 3)
        -1
        >>> Solution.search(Solution, [1], 0)
        -1
        >>> Solution.search(Solution, [5,1,3], 3)
        2
        >>> Solution.search(Solution, [1,3], 3)
        1
        >>> Solution.search(Solution, [3,1], 1)
        1
        '''
        l = len(nums)
        low, high = 0, l - 1
        breakIndex = None
        
        while low <= high:
            mid = low + (high - low) // 2
            if nums[mid] == target:
                return mid
            if nums[low] == target:
                return low
            if nums[high] == target:
                return high
            
            if nums[mid] > nums[low] and nums[mid] > nums[high]:
                if target > nums[mid]:
                    low = mid + 1
                else:
                    if target >= nums[low]:
                        high = mid
                    else:
                        low = mid + 1
            elif nums[mid] < nums[low] and nums[mid] < nums[high]:
                if target < nums[mid]:
                    high = mid - 1
                else:
                    if target >= nums[low]:
                        high = mid
                    else:
                        low = mid + 1
            else:
                if target < nums[mid]:
                    high = mid - 1
                else:
                    low = mid + 1

        return -1
        

if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
