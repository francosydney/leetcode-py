from types import *
from typing import List
from math import inf

class Solution:
    def increasingTriplet(self, nums: List[int]) -> bool:
        '''
        >>> Solution.increasingTriplet(Solution, [0,4,1,-1,2])
        True
        >>> Solution.increasingTriplet(Solution, [1,2,3,4,5])
        True
        >>> Solution.increasingTriplet(Solution, [5,4,3,2,1])
        False
        >>> Solution.increasingTriplet(Solution, [2,1,5,0,4,6])
        True
        >>> Solution.increasingTriplet(Solution, [1,2,1,3])
        True
        >>> Solution.increasingTriplet(Solution, [1,1,-2,6])
        False
        '''
        smallest = secondSmallest = inf
        for num in nums:
            if num <= smallest:
                smallest = num
            elif num <= secondSmallest:
                secondSmallest = num
            else:
                return True
        return False
        



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
