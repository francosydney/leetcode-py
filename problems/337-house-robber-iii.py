from collections import deque

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items):
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)


class Solution:
    def rob(self, root: TreeNode) -> int:
        '''
        >>> (Solution.rob(Solution, buildTree([3,4,5,1,3,None,1])))
        to_replace
        '''
        def thiefEnter(root):
            if not root:
                return (0, 0)
            left = thiefEnter(root.left)
            right = thiefEnter(root.right)

            # (robThis, notRobThis)
            return (root.val + left[1] + right[1], max(left) + max(right))

            
        return max(*thiefEnter(root))


        # houses = deque([root])
        # moneyCount = [0, 0]
        # i = 1

        # while houses:
        #     i = 1 - i
            
        #     for _ in range(len(houses)):
        #         house = houses.popleft()
        #         moneyCount[i] += house.val
        #         for h in filter(bool, [house.left, house.right]):
        #             houses.append(h)
        # return max(*moneyCount)


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
