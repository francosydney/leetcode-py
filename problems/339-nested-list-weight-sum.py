from types import *
from typing import List

from collections import deque

class Solution:
    def depthSum(self, nestedList) -> int:
        '''
        >>> Solution.depthSum(Solution, [[1,1],2,[1,1]])
        10
        >>> Solution.depthSum(Solution, [1,[4,[6]]])
        27
        >>> Solution.depthSum(Solution, [0])
        0
        '''
        # replace isinstance(item, int) with item.isInteger()

        items = deque(nestedList)
        lv = 1
        total = 0
        while items:
            itemsLength = len(items)
            for i in range(itemsLength):
                item = items.popleft()
                if isinstance(item, int):
                    total += lv * item
                else:
                    items.extend(item)

            lv += 1

        return total

        # def countListWeightSum(l, level = 1):
        #     total = 0
        #     for item in l:
        #         total += item * level if isinstance(item, int) else countListWeightSum(item, level + 1)
        #     return total
        
        return countListWeightSum(nestedList)


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
