from types import *
from typing import List

class Solution:
    def searchRange(self, nums: List[int], target: int) -> List[int]:
        '''
        >>> Solution.searchRange(Solution, [5,7,7,8,8,10], 8)
        [3, 4]
        >>> Solution.searchRange(Solution, [5,7,7,8,8,10], 6)
        [-1, -1]
        '''
        def findFirstIndex(target, l, r):  # rightmost is the target
            left, right = l, r
            while left + 1 < right:
                mid = left + (right - left) // 2
                if target == nums[mid]:
                    right = mid
                else:
                    left = mid + 1

            if nums[left] == target:
                return left
            if nums[right] == target:
                return right
            
        def findLastIndex(target, l, r): # leftmost is the target
            left, right = l, r
            while left + 1 < right:
                mid = left + (right - left) // 2
                if target == nums[mid]:
                    left = mid
                else:
                    right = mid - 1

            if nums[right] == target:
                return right
            if nums[left] == target:
                return left
            

        left, right = 0, len(nums) - 1
        while left <= right:
            mid = left + (right - left) // 2
            if target == nums[mid]:
                return [findFirstIndex(target, left, mid), findLastIndex(target, mid, right)]
            elif target > nums[mid]:
                left = mid + 1
            else:
                right = mid - 1
        return [-1, -1]


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
