from types import *
from typing import List

class Solution:
    def reverseVowels(self, s: str) -> str:
        '''
        >>> Solution.reverseVowels(Solution, "hello")
        "holle"
        >>> Solution.reverseVowels(Solution, "leetcode")
        "leotcede"
        '''
        indices = []

        vowelSet = {'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'}
        def isVowel(ch):
            return ch in vowelSet
        for i, ch in enumerate(s):
            if isVowel(ch):
                indices.append(i)
                vowels.append(ch)

if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
