from types import *
from typing import List
from collections import Counter

class Solution:
    def topKFrequent(self, nums: List[int], k: int) -> List[int]:
        '''
        >>> Solution.topKFrequent(Solution, [1,1,1,2,2,3], 2)
        [1, 2]
        >>> Solution.topKFrequent(Solution, [1], 1)
        [1]
        '''
        counter = Counter(nums)
        topK = sorted(counter, key=lambda x: counter[x], reverse=True)[:k]

        return topK




if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
