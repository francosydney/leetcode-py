from types import *
from typing import List

class Solution:
    def intersection(self, nums1: List[int], nums2: List[int]) -> List[int]:
        '''
        >>> Solution.intersection(Solution, [1,2,2,1], [2,2])
        [2]
        >>> Solution.intersection(Solution, [4,9,5], [9,4,9,8,4])
        [9, 4]
        '''
        return list(set(nums1).intersection(set(nums2)))


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
