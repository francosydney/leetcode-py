from types import *
from typing import List

class Solution:
    def searchInsert(self, nums: List[int], target: int) -> int:
        '''
        >>> Solution.searchInsert(Solution, [1,3,5,6], 5)
        2
        >>> Solution.searchInsert(Solution, [1,3,5,6], 2)
        1
        >>> Solution.searchInsert(Solution, [1,3,5,6], 0)
        0
        '''
        low = 0
        high = len(nums) - 1

        if target > nums[high]:
            return high + 1

        while low <= high:
            mid = low + (high - low) // 2
            if nums[mid] > target:
                if mid == 0 or nums[mid - 1] < target:
                    return mid
                high = mid - 1
            elif nums[mid] < target:
                low = mid + 1
            else:
                return mid
        return high


        # find the first value that's >= target, insert before it (take its place)
        



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
