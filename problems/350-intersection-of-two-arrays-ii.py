from types import *
from typing import List
from collections import defaultdict

class Solution:
    def intersect(self, nums1: List[int], nums2: List[int]) -> List[int]:
        '''
        >>> Solution.intersect(Solution, [1,2,2,1], [2,2])
        [2,2]
        >>> Solution.intersect(Solution, [4,9,5], [9,4,9,8,4])
        [4,9]
        '''
        r = []
        dict1 = defaultdict(int)
        for num in nums1:
            dict1[num] += 1

        for num in nums2:
            if dict1[num] > 0:
                r.append(num)
                dict1[num] -= 1
        return r


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
