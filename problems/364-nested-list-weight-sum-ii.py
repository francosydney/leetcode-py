from types import *
from typing import List
from collections import deque

class Solution:
    def depthSumInverse(self, nestedList) -> int:
        '''
        >>> Solution.depthSumInverse(Solution, [[1,1],2,[1,1]])
        8
        >>> Solution.depthSumInverse(Solution, [1,[4,[6]]])
        17
        '''
        items = deque(nestedList)
        leveledItems = []
        lv = 1
        
        while items:
            itemSize = len(items)
            for i in range(itemSize):
                item = items.popleft()
                if isinstance(item, int):
                    leveledItems.append((item, lv))
                else:
                    items.extend(item)

            lv += 1

        deepestLv = lv
        total = 0
        for val, level in leveledItems:
            total += (deepestLv - level) * val

        return total
        # print(leveledItems, lv)



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
