from collections import defaultdict

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items):
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)



class Solution:
    def findLeaves(self, root: TreeNode):
        '''
        >>> (Solution.findLeaves(Solution, buildTree([1,2,3,4,5,None,None])))
        to_replace
        '''
        d = defaultdict(list)
        def getNodeLv(root):
            if not root:
                return 0
            if not (root.left or root.right):
                d[1].append(root.val)
                return 1
            
            nodeLv = max(getNodeLv(root.left), getNodeLv(root.right)) + 1
            d[nodeLv].append(root.val)
            return nodeLv

        getNodeLv(root)

        return d.values()
            
            


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
