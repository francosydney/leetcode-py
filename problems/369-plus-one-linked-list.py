from types import *
from typing import List
from binarytree import Node as TreeNode

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def makeList(arr):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def printList(l: ListNode):
    arr = []
    p = l
    while p is not None:
        # print('### appending', p.val)
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))


class Solution:
    def plusOne(self, head: ListNode) -> ListNode:
        '''
        >>> printList(Solution.plusOne(Solution, makeList([1,2,3])))
        1,2,4
        >>> printList(Solution.plusOne(Solution, makeList([0])))
        1
        >>> printList(Solution.plusOne(Solution, makeList([])))
        
        >>> printList(Solution.plusOne(Solution, makeList([9])))
        1,0
        '''
        sentinel = ListNode(0)
        sentinel.next = head

        p = head
        not9 = sentinel

        while p:
            if p.val != 9:
                not9 = p
            p = p.next

        not9.val += 1
        p = not9.next
        while p:
            p.val = 0
            p = p.next

        return sentinel if sentinel.val else sentinel.next


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
