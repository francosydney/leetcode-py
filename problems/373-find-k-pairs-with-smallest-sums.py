from types import *
from typing import List
from heapq import heappush, heappop

class Solution:
    def kSmallestPairs(self, nums1: List[int], nums2: List[int], k: int) -> List[List[int]]:
        '''
        >>> Solution.kSmallestPairs(Solution, [1,7,10], [2,4,6], 3)
        [[1, 2], [1, 4], [1, 6]]
        >>> Solution.kSmallestPairs(Solution, [1,1,2], [1,2,3], 2)
        [[1, 1], [1, 1]]
        >>> Solution.kSmallestPairs(Solution, [1,2], [3], 3)
        [[1, 3], [2, 3]]
        >>> Solution.kSmallestPairs(Solution, [1], [], 1)
        []
        >>> Solution.kSmallestPairs(Solution, [1,1,2], [1,2,3], 10)
        [[1, 1], [1, 1], [1, 2], [1, 2], [2, 1], [1, 3], [1, 3], [2, 2], [2, 3]]
        '''
        results = []
        sortedSums = []

        for i in range(min(k, len(nums2))):
            n2 = nums2[i]
            for n1 in nums1:
                heappush(sortedSums, (n1 + n2, [n1, n2]))
            results.append(heappop(sortedSums))
        
        for i in range(k - len(nums2)):
            results.append(heappop(sortedSums))

        return [val for _, val in results]



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
