from types import *
from typing import List

class Solution:
    def guessNumber(self, n: int) -> int:
        '''
        >>> Solution.to_replace(Solution, to_replace)
        to_replace
        >>> Solution.to_replace(Solution, to_replace)
        to_replace
        >>> Solution.to_replace(Solution, to_replace)
        to_replace
        '''

        left, right = 1, n
        while left <= right:
            mid = left + (right - left) // 2
            guessResult = guess(mid)

            if guessResult == 0:
                return mid
            elif guessResult > 0:
                left = mid + 1
            else:
                right = mid - 1


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
