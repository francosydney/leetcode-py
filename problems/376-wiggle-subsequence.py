from types import *
from typing import List

class Solution:
    def wiggleMaxLength(self, nums: List[int]) -> int:
        '''
        >>> Solution.wiggleMaxLength(Solution, [1,7,4,9,2,5])
        6
        >>> Solution.wiggleMaxLength(Solution, [1,17,5,10,13,15,10,5,16,8])
        7
        >>> Solution.wiggleMaxLength(Solution, [3,3,3,2,5])
        3
        >>> Solution.wiggleMaxLength(Solution, [1,2,3,4,5,6,7,8,9])
        2
        '''
        if len(nums) < 2:
            return len(nums)
        
        oldDelta = nums[1] - nums[0]
        maxLen = 2 if oldDelta != 0 else 1

        for i in range(2, len(nums)):
            delta = nums[i] - nums[i - 1]
            
            if (delta < 0 and oldDelta >= 0) or (delta > 0 and oldDelta <= 0):
                maxLen += 1
            
            if delta != 0:
                oldDelta = delta
            
        return maxLen



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
