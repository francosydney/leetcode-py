from types import *
from typing import List
from functools import lru_cache

class Solution:
    def combinationSum4(self, nums: List[int], target: int) -> int:
        '''
        >>> Solution.combinationSum4(Solution, [1,2,3], 4)
        7
        >>> Solution.combinationSum4(Solution, [9], 3)
        0
        >>> Solution.combinationSum4(Solution, [4,2,1], 32)
        39882198
        '''

        @lru_cache(maxsize = None)
        def combs(remain):
            if remain == 0:
                return 1

            result = 0
            for num in nums:
                if remain - num >= 0:
                    result += combs(remain - num)
                # potential optimization
                # else:
                #     break

            return result

        return combs(target)


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
