from types import *
from typing import List

class Solution:
    def countAndSay(self, n: int) -> str:
        '''
        >>> Solution.countAndSay(Solution, 4)
        '1211'
        >>> Solution.countAndSay(Solution, 1)
        '1'
        '''
        def say(numString):
            start = end = 0
            l = len(numString)
            result = ''
            while start < l:
                while end < l and numString[end] == numString[start]:
                    end += 1
                result += f'{str(end - start)}{numString[start]}'
                start = end
            return result

        saying = '1'
        for i in range(n-1):
            saying = say(saying)

        return saying
        # return say('3322251')


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
