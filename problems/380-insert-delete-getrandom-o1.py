from types import *
from typing import List
from random import randint

class RandomizedSet:
    '''
    >>> randomSet = RandomizedSet();
    >>> randomSet.remove(0);
    False
    >>> randomSet.remove(0);
    False
    >>> randomSet.insert(0);
    True
    >>> randomSet.getRandom();
    0
    >>> randomSet.remove(0);
    True
    >>> randomSet.insert(0);
    True
    '''
    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.map = {}
        self.inventory = []

    def insert(self, val: int) -> bool:
        """
        Inserts a value to the set. Returns true if the set did not already contain the specified element.
        """
        if val in self.map:
            return False
        else:
            self.inventory.append(val)
            self.map[val] = len(self.inventory) - 1
            return True

    def remove(self, val: int) -> bool:
        """
        Removes a value from the set. Returns true if the set contained the specified element.
        """
        if val in self.map:
            index = self.map[val]
            del self.map[val]

            if index != len(self.inventory) - 1:
                lastVal = self.inventory[-1]
                self.inventory[index] = lastVal
                self.map[lastVal] = index

            self.inventory.pop()
            return True
        else:
            return False

    def getRandom(self) -> int:
        """
        Get a random element from the set.
        """
        return self.inventory[randint(0, len(self.inventory) - 1)]


# Your RandomizedSet object will be instantiated and called as such:
# obj = RandomizedSet()
# param_1 = obj.insert(val)
# param_2 = obj.remove(val)
# param_3 = obj.getRandom()

if __name__ == "__main__":
    import doctest
    doctest.testmod()
