from types import *
from typing import List

class Solution:
    def canConstruct(self, ransomNote: str, magazine: str) -> bool:
        words = Counter(magazine)
        for ch in ransomNote:
            if words[ch] < 1:
                return False
            words[ch] -= 1
        return True

if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
