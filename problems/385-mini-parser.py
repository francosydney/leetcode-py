from types import *
from typing import List

#class NestedInteger:
#    def __init__(self, value=None):
#        """
#        If value is not specified, initializes an empty list.
#        Otherwise initializes a single integer equal to value.
#        """
#
#    def isInteger(self):
#        """
#        @return True if this NestedInteger holds a single integer, rather than a nested list.
#        :rtype bool
#        """
#
#    def add(self, elem):
#        """
#        Set this NestedInteger to hold a nested list and adds a nested integer elem to it.
#        :rtype void
#        """
#
#    def setInteger(self, value):
#        """
#        Set this NestedInteger to hold a single integer equal to value.
#        :rtype void
#        """
#
#    def getInteger(self):
#        """
#        @return the single integer that this NestedInteger holds, if it holds a single integer
#        Return None if this NestedInteger holds a nested list
#        :rtype int
#        """
#
#    def getList(self):
#        """
#        @return the nested list that this NestedInteger holds, if it holds a nested list
#        Return None if this NestedInteger holds a single integer
#        :rtype List[NestedInteger]
#        """



class Solution:
    def deserialize(self, s: str) -> NestedInteger:
        
        stack = [NestedInteger()] # NestedInteger only
        currentNum = 0
        result = None

        for ch in s:
            if ch.isdigit():
                currentNum = currentNum * 10 + int(ch)
            else:
                if currentNum:
                    stack[-1].add(currentNum)
                    currentNum = 0

                if ch == '[':
                    newNI = NestedInteger()
                    if stack:
                        stack[-1].add(newNI)
                    stack.append(newNI)
                elif ch == ']':
                    result = stack.pop()

        return result or stack[0]




if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
