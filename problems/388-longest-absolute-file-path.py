from types import *
from typing import List

class Solution:
    def lengthLongestPath(self, input: str):
        currentFolder = []
        maxFileLength = 0

        for fileOrDirSectionsRaw in input.split('\n'):
            fileOrDirSections = fileOrDirSectionsRaw.split('\t')

            fileOrDir = fileOrDirSections[-1]
            isDir = '.' not in fileOrDir

            while len(currentFolder) >= len(fileOrDirSections):
                currentFolder.pop()

            if isDir:
                currentFolder.append(fileOrDir)
            else:
                fileLength = len('/'.join(currentFolder) + ('/' if currentFolder else '') + fileOrDir)
                maxFileLength = max(maxFileLength, fileLength)

        return maxFileLength

print(Solution.lengthLongestPath(Solution, "dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext"))
print(Solution.lengthLongestPath(Solution, "a"))
print(Solution.lengthLongestPath(Solution, "file1.txt\nfile2.txt\nlongfile.txt"))



# if __name__ == "__main__":
#     import time
#     start_time = time.time()

#     import doctest
#     doctest.testmod()
#     print("--- %s seconds ---" % (time.time() - start_time))
