from types import *
from typing import List

class Solution:
    def combinationSum(self, candidates: List[int], target: int) -> List[List[int]]:
        '''
        >>> Solution.combinationSum(Solution, [2,3,6,7], 7)
        [[2,2,3],[7]]
        >>> Solution.combinationSum(Solution, [2,3,5], 8)
        [[2,2,2,2],[2,3,3],[3,5]]
        >>> Solution.combinationSum(Solution, [2], 1)
        []
        >>> Solution.combinationSum(Solution, [1], 1)
        [[1]]
        >>> Solution.combinationSum(Solution, [1], 2)
        [[1,1]]
        '''
        combinations = []
        l = len(candidates)

        def backtrack(start = 0, summ = 0, nums = []):
            if summ == target:
                combinations.append(nums[:])
                return
            if summ > target:
                return
            
            for i in range(start, l):
                nums.append(candidates[i])
                backtrack(i, summ + candidates[i], nums)
                nums.pop()
            
        backtrack()

        return combinations

        



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
