from types import *
from typing import List

class Solution:
    def decodeString(self, s: str) -> str:
        '''
        >>> Solution.decodeString(Solution, "3[a]2[bc]")
        'aaabcbc'
        >>> Solution.decodeString(Solution, "3[a2[c]]")
        'accaccacc'
        >>> Solution.decodeString(Solution, "2[abc]3[cd]ef")
        'abcabccdcdcdef'
        >>> Solution.decodeString(Solution, "abc3[cd]xyz")
        'abccdcdcdxyz'
        '''
        stringStack = []

        for ch in s:
            if ch != ']':
                stringStack.append(ch)
            else:
                strSections = []
                while stringStack[-1] != '[':
                    strSections.append(stringStack.pop())
                stringStack.pop()

                numSections = []
                while stringStack and stringStack[-1].isdigit():
                    numSections.append(stringStack.pop())
                
                stringStack.append( ''.join(reversed(strSections)) * int( ''.join(reversed(numSections)) ) )
        
        return ''.join(stringStack)






if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
