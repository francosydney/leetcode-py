from types import *
from typing import List

class Solution:
    def combinationSum2(self, candidates: List[int], target: int) -> List[List[int]]:
        '''
        >>> Solution.combinationSum2(Solution, [10,1,2,7,6,1,5], 8)
        [ [1,1,6], [1,2,5], [1,7], [2,6] ]
        >>> Solution.combinationSum2(Solution, [2,5,2,1,2], 5)
        [ [1,2,2], [5] ]
        >>> Solution.combinationSum2(Solution, [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1], 27)
        []
        '''
        results = []
        candidates.sort()
        l = len(candidates)
        
        def backtrack(start = 0, distanceToTarget = target, nums = []):
            if distanceToTarget == 0:
                results.append(nums[:])
                return
            
            for i in range(start, l):
                if i > start and candidates[i] == candidates[i-1]:
                    continue
                # sumOfRemains -= candidates[i]
                if distanceToTarget - candidates[i] < 0:
                    break
                
                nums.append(candidates[i])
                backtrack(i + 1, distanceToTarget - candidates[i], nums)
                nums.pop()

        backtrack()

        return results


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
