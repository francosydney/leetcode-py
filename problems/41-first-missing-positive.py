from types import *
from typing import List

class Solution:
    def firstMissingPositive(self, nums: List[int]) -> int:
        '''
        >>> Solution.firstMissingPositive(Solution, [3,4,-1,1])
        2
        >>> Solution.firstMissingPositive(Solution, [1,2,0])
        3
        >>> Solution.firstMissingPositive(Solution, [7,8,9,11,12])
        1
        '''
        maxItem = l = len(nums)
        ifOneExist = False
        for i in range(l):
            if nums[i] == 1:
                ifOneExist = True
            if nums[i] > maxItem or nums[i] < 1:
                nums[i] = 1

        if not ifOneExist:
            return 1
        else:
            # print(123123)
            for i in range(l): # nums[i]
                index = abs(nums[i]) - 1
                nums[index] = - abs(nums[index])
            
            # print(nums, '###')
            for i in range(l):
                if nums[i] > 0:
                    return i + 1
            return maxItem + 1

    



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
