from types import *
from typing import List

class Solution:
    def trap(self, height: List[int]) -> int:
        '''
        >>> Solution.trap(Solution, [0,1,0,2,1,0,1,3,2,1,2,1])
        6
        >>> Solution.trap(Solution, [4,2,0,3,2,5])
        9
        '''
        l = len(height)
        highestBarOnLeft = [0] * l
        highestBarOnRight = [0] * l
        
        highestOnLeft = 0
        for i in range(0, l):
            highestOnLeft = max(highestOnLeft, height[i])
            highestBarOnLeft[i] = highestOnLeft

        highestOnRight = 0
        for i in range(l - 1, -1, -1):
            highestOnRight = max(highestOnRight, height[i])
            highestBarOnRight[i] = highestOnRight
        
        totalWater = 0
        for i in range(1, l-1):
            # print(f'i={i} highestBarOnLeft={highestBarOnLeft[i-1]} highestBarOnRight={highestBarOnRight[i+1]} height={height[i]}')
            totalWater += max(0, min(highestBarOnLeft[i-1], highestBarOnRight[i+1]) - height[i])
        return totalWater
            



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
