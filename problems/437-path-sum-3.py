from types import *
from typing import List
from collections import defaultdict

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def pathSum(self, root: TreeNode, sum: int) -> int:
        '''
        >>> t = TreeNode(10, TreeNode(5, TreeNode(3, TreeNode(3), TreeNode(-2)), TreeNode(2, None, TreeNode(1))), TreeNode(-3, None, TreeNode(11)))
        >>> Solution.pathSum(Solution, t, 8)
        3
        >>> t = TreeNode(8)
        >>> Solution.pathSum(Solution, t, 8)
        1
        >>> t = TreeNode(8)
        >>> Solution.pathSum(Solution, t, 5)
        0
        >>> t = TreeNode(3, TreeNode(5), TreeNode(8))
        >>> Solution.pathSum(Solution, t, 8)
        2
        '''
        count = 0
        h = defaultdict(int)

        def preorderTranverse(node: TreeNode, current_sum):
            nonlocal count
            if not node:
                return
            current_sum += node.val
            if current_sum == sum:
                count += 1
            count += h[current_sum - sum]

            h[current_sum] += 1
            preorderTranverse(node.left, current_sum)
            preorderTranverse(node.right, current_sum)
            h[current_sum] -= 1
    
        preorderTranverse(root, 0)
        return count


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
