from types import *
from typing import List
import math

class Solution:
    def jump(self, nums: List[int]) -> int:
        jumpCount = 0
        jumpRange = 0
        farthest = 0
        
        for i in range(0, len(nums) - 1):
            farthest = max(farthest, i + nums[i])
            if i == jumpRange:
                jumpCount += 1
                jumpRange = farthest
        return jumpCount



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
