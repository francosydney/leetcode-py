from types import *
from typing import List

class Solution:
    def permute(self, nums: List[int]) -> List[List[int]]:
        '''
        >>> Solution.permute(Solution, [1,2,3])
        [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
        >>> Solution.permute(Solution, [0,1])
        [[0,1],[1,0]]
        >>> Solution.permute(Solution, [1])
        [[1]]
        '''
        results = []
        l = len(nums)

        def backtrack(permutation = [], selectable = set(nums)):
            if len(permutation) == l:
                results.append(permutation[:])
                return
            
            for num in list(selectable):
                permutation.append(num)
                selectable.remove(num)

                backtrack(permutation, selectable)

                permutation.pop()
                selectable.add(num)



        backtrack()

        return results




if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
