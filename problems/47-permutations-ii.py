from types import *
from typing import List

class Solution:
    def permuteUnique(self, nums: List[int]) -> List[List[int]]:
        '''
        >>> Solution.permuteUnique(Solution, [1,2,3])
        [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
        '''
        results = set()
        l = len(nums)

        def backtrack(first = 0):
            # print(f'current nums: { nums }, first = {first}')
            if first == l:
                results.add(tuple(nums))
                return
            for i in range(first, l):
                nums[first], nums[i] = nums[i], nums[first]
                backtrack(first + 1)
                nums[first], nums[i] = nums[i], nums[first]


        backtrack()

        return [list(permutation) for permutation in results]



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
