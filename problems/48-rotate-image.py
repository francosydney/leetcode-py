from types import *
from typing import List


class Solution:
    def rotate(self, matrix: List[List[int]]) -> None:
        '''
        >>> a = [[1,2,3],[4,5,6],[7,8,9]]
        >>> Solution.rotate(Solution, a)
        >>> a
        [[7, 4, 1], [8, 5, 2], [9, 6, 3]]
        >>> a = [ [ 5, 1, 9,11], [ 2, 4, 8,10], [13, 3, 6, 7], [15,14,12,16] ]
        >>> Solution.rotate(Solution, a)
        >>> a
        [[15, 13, 2, 5], [14, 3, 4, 1], [12, 6, 8, 9], [16, 7, 10, 11]]
        >>> a = []
        >>> Solution.rotate(Solution, a)
        >>> a
        []
        >>> a = [[5]]
        >>> Solution.rotate(Solution, a)
        >>> a
        [[5]]
        '''
        
        def rotateCircle(cood1, cood2):
            x1, y1 = cood1
            x2, y2 = cood2
            for delta in range(y2 - y1):
                matrix[x1+delta][y2], matrix[x2][y2-delta], matrix[x2-delta][y1], matrix[x1][y1+delta] = \
                    matrix[x1][y1+delta], matrix[x1 + delta][y2], matrix[x2][y2-delta], matrix[x2-delta][y1]
        
        if len(matrix) < 2:
            return
        i = 0
        j = len(matrix) - 1
        while j > i:
            rotateCircle((i, i), (j, j))
            i += 1
            j -= 1
        

if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
