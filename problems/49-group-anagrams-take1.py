from types import *
from typing import List
from collections import defaultdict

class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        '''
        >>> Solution.groupAnagrams(Solution, ["eat","tea","tan","ate","nat","bat"])
        [["bat"],["nat","tan"],["ate","eat","tea"]]
        >>> Solution.groupAnagrams(Solution, [""])
        [[""]]
        >>> Solution.groupAnagrams(Solution, ["a"])
        [["a"]]
        '''
        mappedWords = defaultdict(list)
        for s in strs:
            mappedWords[''.join(sorted(list(s)))].append(s)

        return list(mappedWords.values())




if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
