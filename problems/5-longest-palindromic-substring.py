from types import *

def LongestCommonSubstr(str1, str2):
    l1 = len(str1)
    l2 = len(str2)
    dp = [[0] * (l2 + 1) for j in range(l1 + 1)]
    maxL = 0
    subStr1 = (0, 0)

    for i in range(l1 - 1, -1, -1):
        for j in range(l2 - 1, -1, -1):
            if str1[i] == str2[j]:
                dp[i][j] = dp[i + 1][j + 1] + 1
                if dp[i][j] > maxL and i + j + dp[i][j] == l1:
                    maxL = dp[i][j]
                    subStr1 = (i, maxL)
            else:
                dp[i][j] = 0

    offset, length = subStr1
    return str1[offset: (offset + length)]


class Solution:
    def longestPalindrome(self, s: str) -> str:
        '''
        >>> Solution.longestPalindrome(Solution, "aacdefcaa")
        'aa'
        >>> Solution.longestPalindrome(Solution, "cbbd")
        'bb'
        '''
        s2 = ''.join(list(s)[::-1])
        return LongestCommonSubstr(s, s2)


if __name__ == "__main__":
    import doctest
    doctest.testmod()
