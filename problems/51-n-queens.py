from types import *
from typing import List
from collections import defaultdict

class Solution:
    def solveNQueens(self, n: int) -> List[List[str]]:
        '''
        >>> Solution.solveNQueens(Solution, 4)
        [['.Q..', '...Q', 'Q...', '..Q.'], ['..Q.', 'Q...', '...Q', '.Q..']]
        >>> Solution.solveNQueens(Solution, 1)
        [['Q']]
        >>> Solution.solveNQueens(Solution, 6)
        []
        '''
        queensAtLines = [] # [2,3,4]: 1st line - 2, 2nd line - 3, 3rd line - 6,
        invalidCriterias = {'y': set(), 'x+y': set(), 'y-x': set()}
        def isLastQueenValid(queenPosition):
            x, y = queenPosition
            return not (y in invalidCriterias['y'] or (x+y) in invalidCriterias['x+y'] or (y-x) in invalidCriterias['y-x'])

        def updateCriteriaWith(queenPosition, restricted):
            x, y = queenPosition
            if restricted:
                invalidCriterias['y'].add(y)
                invalidCriterias['x+y'].add(x+y)
                invalidCriterias['y-x'].add(y-x)
            else:
                invalidCriterias['y'].remove(y)
                invalidCriterias['x+y'].remove(x+y)
                invalidCriterias['y-x'].remove(y-x)

        results = []
        def recurse():
            nonlocal queensAtLines
            nonlocal invalidCriterias
            
            l = len(queensAtLines)
            if l >= n:
                results.append(queensAtLines.copy())
                return

            for i in range(n):
                if isLastQueenValid( (l, i) ):
                    queensAtLines.append(i)
                    updateCriteriaWith( (l, i), True )
                    recurse()
                    queensAtLines.pop()
                    updateCriteriaWith( (l, i), False )

        recurse()

        return [[pos * '.' + 'Q' + (n - pos - 1) * '.' for pos in board] for board in results]


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
