from types import *
from typing import List
from math import inf

class Solution:
    def maxSubArray(self, nums: List[int]) -> int:
        '''
        >>> Solution.maxSubArray(Solution, [-2,1,-3,4,-1,2,1,-5,4])
        6
        >>> Solution.maxSubArray(Solution, [1])
        1
        >>> Solution.maxSubArray(Solution, [5,4,-1,7,8])
        23
        '''
        # f[x]: largest sum ending at x (must take nums[x])

        f = [0] * len(nums)
        f[0] = nums[0]

        for i in range(1, len(nums)):
            f[i] = max(f[i-1] + nums[i], nums[i])

        return max(f)
            



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
