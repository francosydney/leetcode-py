from types import *
from typing import List

class Solution:
    def canJump(self, nums: List[int]) -> bool:
        '''
        >>> Solution.canJump(Solution, [2,3,1,1,4])
        True
        >>> Solution.canJump(Solution, [3,2,1,0,4])
        False
        '''
        reach = 0
        l = len(nums)
        
        for index, num in enumerate(nums):
            if index > reach:
                return False
            reach = max(reach, index + num)
        
        return True


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
