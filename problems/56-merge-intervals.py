from types import *
from typing import List

class Solution:
    def merge(self, intervals: List[List[int]]) -> List[List[int]]:
        '''
        >>> Solution.merge(Solution, [[1,4],[4,5]])
        [[1, 5]]
        >>> Solution.merge(Solution, [[1,4]])
        [[1, 4]]
        >>> Solution.merge(Solution, [[1,3],[2,6],[8,10],[15,18]])
        [[1, 6], [8, 10], [15, 18]]
        '''
        intervals.sort(key=lambda x: x[0])
        merged = []

        for interval in intervals:
            if not merged or merged[-1][1] < interval[0]:
                merged.append(interval)
            else:
                merged[-1][1] = max(merged[-1][1], interval[1])
        return merged

        # while i < len(intervals):
        #     if sortedIntervals[i][0] > sortedIntervals[currentIndex][1]:
        #         mergedIntervals.append(sortedIntervals[currentIndex])
        #         currentIndex = i
        #     else:
        #         sortedIntervals[currentIndex][1] = max(sortedIntervals[currentIndex][1], sortedIntervals[i][1])

        #     i += 1
        # mergedIntervals.append(sortedIntervals[currentIndex])
        # return mergedIntervals
        


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
