from types import *
from typing import List
from math import inf

class Solution:
    def insert(self, intervals: List[List[int]], newInterval: List[int]) -> List[List[int]]:
        '''
        >>> Solution.insert(Solution, [[1,3],[6,9]], [2,5])
        [[1, 5], [6, 9]]
        >>> Solution.insert(Solution, [[1,2],[6,7]], [4,5])
        [[1, 2], [4, 5], [6, 7]]
        >>> Solution.insert(Solution, [[1,2],[5,7]], [4,5])
        [[1, 2], [4, 7]]
        >>> Solution.insert(Solution, [], [5,7])
        [[5, 7]]
        >>> Solution.insert(Solution, [[1,5]], [2,3])
        [[1, 5]]
        >>> Solution.insert(Solution, [[1,5]], [2,7])
        [[1, 7]]
        >>> Solution.insert(Solution, [[1,5]], [6,8])
        [[1, 5], [6, 8]]
        '''
        if len(intervals) < 1:
            return [newInterval]

        intervals = [[-inf,-inf]] + intervals + [[inf,inf]]
        l = len(intervals)
        intervalIndexBeforeNew, intervalIndexAfterNew = -1, l

        i = 0
        while i < l:
            if intervals[i][1] < newInterval[0]:
                intervalIndexBeforeNew = i
            if intervals[i][0] > newInterval[1]:
                intervalIndexAfterNew = i
                break
            i += 1

        mergedInterval = [min(intervals[intervalIndexBeforeNew + 1][0], newInterval[0]), max(intervals[intervalIndexAfterNew - 1][1], newInterval[1])]
        return intervals[:intervalIndexBeforeNew+1][1:] + [mergedInterval] + intervals[intervalIndexAfterNew:][:-1]





if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
