from types import *
from typing import List
from itertools import chain
from collections import defaultdict

class Solution:
    def convert(self, s: str, numRows: int) -> str:
        '''
        >>> Solution.convert(Solution, "PAYPALISHIRING", 3)
        "PAHNAPLSIIGYIR"
        >>> Solution.convert(Solution, "PAYPALISHIRING", 4)
        "PINALSIGYAHRPI"
        >>> Solution.convert(Solution, "A", 1)
        "A"
        '''
        rows = defaultdict(list)
        charIndex = 0
        l = len(s)

        while True:
            for rowIndex in chain(range(numRows), range(numRows - 2, 0, -1)):
                rows[rowIndex].append(s[charIndex])

                charIndex += 1
                if charIndex == l:
                    return ''.join([''.join(rows[i]) for i in range(numRows)])
                



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
