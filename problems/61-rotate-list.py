from types import *
from typing import List
from binarytree import Node as TreeNode

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def makeList(arr):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def printList(l: ListNode):
    arr = []
    p = l
    while p is not None:
        # print('### appending', p.val)
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))


class Solution:
    def rotateRight(self, head: ListNode, k: int) -> ListNode:
        '''
        >>> printList(Solution.rotateRight(Solution, makeList([1,2,3,4,5]), 2))
        4,5,1,2,3
        >>> printList(Solution.rotateRight(Solution, makeList([0,1,2]), 4))
        2,0,1
        >>> printList(Solution.rotateRight(Solution, makeList([]), 2))
        <BLANKLINE>
        >>> printList(Solution.rotateRight(Solution, makeList([1,2,3]), 3))
        1,2,3
        '''
        #1 last to first
        #2 find start
        #3 cut the middle
        def getLengthAndLastNode(l):
            length = 1
            while l.next:
                length += 1
                l = l.next
            return (length, l)
        
        if not head:
            return head
        
        length, lastNode = getLengthAndLastNode(head)
        if not (k % length):
            return head
        p = head
        lastNode.next = head
        for _ in range(length - (k % length) - 1):
            p = p.next
        start, p.next = p.next, None
        
        return start




if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
