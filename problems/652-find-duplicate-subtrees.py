from types import *
from typing import List
from binarytree import Node as TreeNode
from collections import Counter

class Solution:
    def findDuplicateSubtrees(self, root: TreeNode) -> TreeNode:
        '''
        >>> root = TreeNode(1)
        >>> root.left = TreeNode(2)
        >>> root.right = TreeNode(3)
        >>> root.left.left = TreeNode(4)
        >>> root.left.right = TreeNode(5)
        >>> root.right.left = TreeNode(2)
        >>> root.right.right = TreeNode(4)
        >>> root.right.left.left = TreeNode(4)
        >>> root.right.left.right = TreeNode(5)
        >>> Solution.findDuplicateSubtrees(Solution, root)
        to_replace
        '''
        count = Counter()
        ans = []
        def findDuplicate(node):
            if not node: return "#"
            serial = "{},{},{}".format(node.val, findDuplicate(node.left), findDuplicate(node.right))
            count[serial] += 1
            if count[serial] == 2:
                ans.append(node)
            return serial

        findDuplicate(root)
        
        return ans
        

        # return serialize(root)


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
