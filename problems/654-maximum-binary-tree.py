from types import *
from typing import List
from binarytree import Node as TreeNode

def constructMaximumBinaryTree(nums: List[int]) -> TreeNode:
    l = len(nums)
    if l == 0:
        return None
    maxValIsAt = nums.index(max(nums))
    root = TreeNode(nums[maxValIsAt])
    root.left = constructMaximumBinaryTree(nums[0: maxValIsAt])
    root.right = constructMaximumBinaryTree(nums[maxValIsAt + 1: l])
    return root


class Solution:
    def constructMaximumBinaryTree(self, nums: List[int]) -> TreeNode:
        '''
        >>> print(Solution.constructMaximumBinaryTree(Solution, [3,2,1,6,0,5]))
        11111
        '''
        return constructMaximumBinaryTree(nums)


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
