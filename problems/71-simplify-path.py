from types import *
from typing import List

class Solution:
    def simplifyPath(self, path: str) -> str:
        '''
        >>> Solution.simplifyPath(Solution, "/home/")
        "/home"
        >>> Solution.simplifyPath(Solution, "/../")
        "/"
        >>> Solution.simplifyPath(Solution, "/home//foo/")
        "/home/foo"
        >>> Solution.simplifyPath(Solution, "/a/./b/../../c/")
        "/c"
        '''
        lastSlashIndex = 0
        pathSectionStack = []

        if not path.endswith('/'):
            path = path + '/'

        for i in range(1, len(path)):
            if path[i] == '/':
                if lastSlashIndex == i - 1:
                    pass
                elif lastSlashIndex == i - 2 and path[i-1] == '.':
                    pass
                elif lastSlashIndex == i - 3 and path[i-1] == '.' and path[i-2] == '.':
                    if pathSectionStack:
                        pathSectionStack.pop()
                else:
                    pathSectionStack.append(path[lastSlashIndex + 1: i])

                lastSlashIndex = i

        return '/' + '/'.join(pathSectionStack)


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
