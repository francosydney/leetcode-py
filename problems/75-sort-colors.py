from types import *
from typing import List

class Solution:
    def sortColors(self, nums: List[int]) -> None:
        '''
        >>> Solution.sortColors(Solution, [2,0,2,1,1,0])
        [0, 0, 1, 1, 2, 2]
        >>> Solution.sortColors(Solution, [2,0,1])
        [0, 1, 2]
        >>> Solution.sortColors(Solution, [0])
        [0]
        >>> Solution.sortColors(Solution, [1])
        [1]
        '''
        l = len(nums)
        leftBorder = 0
        rightBorder = l - 1
        i = 0

        while i <= rightBorder:
            if nums[i] == 0:
                nums[leftBorder], nums[i] = nums[i], nums[leftBorder]
                i += 1
                leftBorder += 1
            elif nums[i] == 1:
                i += 1
            else: # nums[i] == 2
                # print('????', nums[i])
                nums[rightBorder], nums[i] = nums[i], nums[rightBorder]
                rightBorder -= 1
            # print(f'i={i} leftBorder={leftBorder} rightBorder={rightBorder} -> {nums}')
        return nums

        


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
