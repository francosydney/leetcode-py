from types import *
from typing import List
from collections import defaultdict

class Solution:
    def partitionLabels(self, S: str) -> List[int]:
        '''
        >>> Solution.partitionLabels(Solution, 'ababcbacadefegdehijhklij')
        [9, 7, 8]
        '''
        current_color = 1
        allocation = [0] * len(S)
        def color(start, end, targetColor = None):
            nonlocal current_color
            for i in range(start, end + 1):
                allocation[i] = targetColor or current_color
            if not targetColor:
                current_color += 1
        
        occurance = defaultdict(list)
        for index, ch in enumerate(S):
            occurance[ch].append(index)
        for indexes in occurance.values():
            i, j = indexes[0], indexes[-1]
            if not allocation[i] and not allocation[j]:
                color(i, j)
            elif allocation[i] and allocation[j]:
                if allocation[i] != allocation[j]:
                    k = j
                    while k < len(S) - 1 and allocation[k + 1] == allocation[j]:
                        k += 1
                    color(i, k, allocation[i])
            else:
                color(i, j, allocation[i] or allocation[j])

        count = []
        current_color = allocation[0]
        current_count = 1
        for c in allocation[1:]:
            if c == current_color:
                current_count += 1
            else:
                count.append(current_count)
                current_count = 1
                current_color = c
        count.append(current_count)
        
        return count
                    


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
