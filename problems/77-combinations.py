from types import *
from typing import List

class Solution:
    def combine(self, n: int, k: int) -> List[List[int]]:
        '''
        >>> Solution.combine(Solution, 4, 2)
        [[2,4],[3,4],[2,3],[1,2],[1,3],[1,4]]
        >>> Solution.combine(Solution, 1, 1)
        [[1]]
        '''
        results = []

        def backtrack(start = 1, combines = []):
            if len(combines) == k:
                results.append(combines[:])
                return
            
            for num in range(start, n + 1):
                combines.append(num)
                backtrack(num + 1, combines)
                combines.pop()
        

        backtrack()

        return results



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
