from types import *
from typing import List

class Solution:
    def subsets(self, nums: List[int]) -> List[List[int]]:
        '''
        >>> Solution.subsets(Solution, [1,2,3])
        [[], [1], [2], [3], [1, 2], [1, 3], [2, 3], [1, 2, 3]]
        >>> Solution.subsets(Solution, [0])
        [[], [0]]
        '''
        l = len(nums)
        results = []
        for i in range(2**l, 2**(l+1)):
            bitmask = bin(i)[3:]
            results.append([nums[j] for j in range(l) if bitmask[j] == '1'])

        return results


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
