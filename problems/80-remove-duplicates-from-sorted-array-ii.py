from types import *
from typing import List

class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        '''
        >>> Solution.removeDuplicates(Solution, [0,0,1,1,1,1,2,3,3])
        7
        '''
        if len(nums) <= 2:
            return len(nums) - 1

        i = 0
        for j in range(2, len(nums)):
            if nums[j] != nums[i]:
                i += 1
                nums[i + 1] = nums[j]
        return i + 2

        


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))


