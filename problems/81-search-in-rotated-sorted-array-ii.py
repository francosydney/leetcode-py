from types import *
from typing import List

class Solution:
    def search(self, nums: List[int], target: int) -> bool:
        '''
        >>> Solution.search(Solution, [1,0,1,1,1], 0)
        True
        '''
        left, right = 0, len(nums) - 1

        while left <= right:
            mid = left + (right - left) // 2
            print(f'leftIndex={left} rightIndex={right} midIndex={mid}')
            if target == nums[mid] or target == nums[left]:
                return True

            if nums[right] > nums[left]:
                if target > nums[mid]:
                    left = mid + 1
                else:
                    right = mid - 1
            elif nums[mid] <= nums[right]:
                if target > nums[left] or target < nums[mid]:
                    right = mid - 1
                else:
                    left = mid + 1
            else:
                print(f'111111', target, left, nums[left])
                if target > nums[mid] or target < nums[left]:
                    left = mid + 1
                else:
                    right = mid - 1
        
        return False



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
