from types import *
from typing import List
from binarytree import Node as TreeNode

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def makeList(arr):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def printList(l: ListNode):
    arr = []
    p = l
    while p is not None:
        # print('### appending', p.val)
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))


class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        '''
        >>> printList(Solution.deleteDuplicates(Solution, makeList([1,2,3,3,4,4,5])))
        1,2,5
        >>> printList(Solution.deleteDuplicates(Solution, makeList([1,1,1,2,3])))
        2,3
        '''
        sentinel = ListNode(0)
        sentinel.next = head
        
        p = head
        prev = sentinel
        while p:
            if p.next and p.val == p.next.val:
                while p.next and p.val == p.next.val:
                    p = p.next
                prev.next = p.next
            else:
                prev = p
            p = p.next

        return sentinel.next

if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
