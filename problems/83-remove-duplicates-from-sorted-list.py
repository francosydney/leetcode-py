from types import *
from typing import List
from binarytree import Node as TreeNode

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def makeList(arr: ListNode):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def printList(l: ListNode):
    arr = []
    p = l
    while p is not None:
        # print('### appending', p.val)
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))


class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        '''
        >>> printList(Solution.deleteDuplicates(Solution, makeList([1,1,2,3,3])))
        1,2,3
        >>> printList(Solution.deleteDuplicates(Solution, makeList([1,1,2])))
        1,2
        >>> printList(Solution.deleteDuplicates(Solution, makeList([1,1,1,1,1])))
        1
        '''
        p = head
        while p and p.next:
            if p.next.val == p.val:
                p.next = p.next.next
            else:
                p = p.next
        return head
                


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
