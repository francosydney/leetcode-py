from types import *
from typing import List

class Solution:
    def largestRectangleArea(self, heights: List[int]) -> int:
        '''
        >>> Solution.largestRectangleArea(Solution, [2,1,5,6,2,3])
        10
        >>> Solution.largestRectangleArea(Solution, [2,4])
        4
        '''
        maxHeight = max(heights)
        maxArea = 0
        l = len(heights)

        for h in range(maxHeight, 0, -1):
            seq = 0
            maxSeq = 0
            for i, height in enumerate(heights):
                if height >= h:
                    seq += 1
                if height < h or i == l - 1:
                    maxSeq = max(maxSeq, seq)
                    seq = 0
            maxArea = max(maxArea, maxSeq * h)


        return maxArea



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
