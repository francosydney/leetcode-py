from types import *
from typing import List
from binarytree import Node as TreeNode

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def makeList(arr):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def printList(l: ListNode):
    arr = []
    p = l
    while p is not None:
        # print('### appending', p.val)
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))


class Solution:
    def partition(self, head: ListNode, x: int) -> ListNode:
        '''
        >>> printList(Solution.partition(Solution, makeList([1,4,3,2,5,2]), 3))
        1,2,2,4,3,5
        >>> printList(Solution.partition(Solution, makeList([2,1]), 2))
        1,2
        '''
        largerNodes = p = ListNode(-1)
        sentinel = l = ListNode(-1)
        l.next = head
        while l.next:
            if l.next.val >= x:
                p.next = l.next
                p = p.next
                l.next = l.next.next
            else:
                l = l.next
        p.next = None
        l.next = largerNodes.next
        return sentinel.next




if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
