from types import *
from typing import List

class Solution:
    def isScramble(self, s1: str, s2: str) -> bool:
        '''
        >>> Solution.isScramble(Solution, "great", "rgeat")
        True
        >>> Solution.isScramble(Solution, "abcde", "caebd")
        False
        >>> Solution.isScramble(Solution, "a", "a")
        True
        '''
        cache = {}
        def isScrableMatch(s1, s2): # 10: [0 - 9]
            if s1 == s2:
                return True

            l = len(s1)
            if l == 1:
                return False
            key = f'{s1}_{s2}'
            if key in cache:
                return cache[key]

            flag = False
            for i in range(1, l):
                if isScrableMatch(s1[:i], s2[:i]) and isScrableMatch(s1[i:], s2[i:]):
                    flag = True
                    break
                if isScrableMatch(s1[:i], s2[l-i:]) and isScrableMatch(s1[i:], s2[:l-i]):
                    flag = True
                    break
            
            cache[key] = flag
            return flag

        return isScrableMatch(s1, s2)
        





if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
