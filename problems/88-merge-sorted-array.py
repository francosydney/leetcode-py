from types import *
from typing import List

class Solution:
    def merge(self, nums1: List[int], m: int, nums2: List[int], n: int) -> None:
        '''
        >>> Solution.merge(Solution, [1,2,3,0,0,0], 3, [2,5,6], 3)
        [1, 2, 2, 3, 5, 6]
        >>> Solution.merge(Solution, [1], 1, [], 0)
        [1]
        >>> Solution.merge(Solution, [0], 0, [1], 1)
        [1]
        '''
        copy = [None] * (m+n)
        i = j = k = 0
        while i < m and j < n:
            if nums1[i] < nums2[j]:
                copy[k] = nums1[i]
                i += 1
            else:
                copy[k] = nums2[j]
                j += 1
            k += 1

        while i < m:
            copy[k] = nums1[i]
            k += 1
            i += 1
        while j < n:
            copy[k] = nums2[j]
            k += 1
            j += 1
        
        for i in range(m+n):
            nums1[i] = copy[i]

        return copy


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
