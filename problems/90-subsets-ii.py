from types import *
from typing import List

class Solution:
    def subsetsWithDup(self, nums: List[int]) -> List[List[int]]:
        '''
        >>> Solution.subsetsWithDup(Solution, [1,2,2])
        [[],[1],[1,2],[1,2,2],[2],[2,2]]
        >>> Solution.subsetsWithDup(Solution, [0])
        [[],[0]]
        >>> Solution.subsetsWithDup(Solution, [4,4,4,1,4])
        [[],[1],[1,4],[1,4,4],[1,4,4,4],[1,4,4,4,4],[4],[4,4],[4,4,4],[4,4,4,4]]
        '''
        added = set()
        result = []
        l = len(nums)

        def hashSubset(subset):
            return tuple(sorted(subset))

        def backtrack(first = 0, subset = []):
            if len(subset) == k:
                subsetIndex = hashSubset(subset)
                if subsetIndex not in added:
                    result.append(subset[:])
                    added.add(subsetIndex)
                return

            for i in range(first, l):
                subset.append(nums[i])
                backtrack(i + 1, subset)
                subset.pop()
            

        for k in range(l + 1):
            backtrack()

        return result
        


if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
