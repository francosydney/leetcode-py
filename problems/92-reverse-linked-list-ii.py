from types import *
from typing import List


# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
        

def makeList(arr):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def printList(l: ListNode):
    arr = []
    p = l
    while p is not None:
        # print('### appending', p.val)
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))


# def reverse(p: ListNode):
#     if p.next is None:
#         return p
#     rest = reverse(p.next)
#     p.next.next = p
#     p.next = None
#     return rest
    
# printList(reverse(makeList([1,10,11,12,2])))

nextNode = None
def reverseTil(p: ListNode, TilEnd: int):
    global nextNode
    if TilEnd == 1:
        nextNode = p.next
        return p
    rest = reverseTil(p.next, TilEnd - 1)
    p.next.next = p
    p.next = nextNode

    return rest

def reverseBetween(head: ListNode, m: int, n: int):
    if m == 1:
        return reverseTil(head, n - m + 1)
    
    rest = reverseBetween(head.next, m - 1, n - 1)
    head.next = rest
    
    return head


# class Solution:
#     def reverseBetween(self, head: ListNode, m: int, n: int) -> ListNode:
#         return reverseBetween(head)

# printList(makeList([1,2,3,4,5]))
printList(reverseBetween(makeList([1,2,3,4,5]), 2, 4))
# printList(reverseTil(makeList([1,2,3,4,5]), 3))