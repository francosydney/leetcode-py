from types import *
from typing import List

from collections import defaultdict


class Solution:
    def reorderLogFiles(self, logs: List[str]) -> List[str]:
        '''
        >>> Solution.reorderLogFiles(Solution, ["dig1 8 1 5 1","let1 art can","dig2 3 6","let2 own kit dig","let3 art zero"])
        ['let1 art can', 'let3 art zero', 'let2 own kit dig', 'dig1 8 1 5 1', 'dig2 3 6']
        '''
        digit_logs = []
        letter_logs = defaultdict(list)
        for log in logs:
            split_index = log.find(' ')
            identifier, log_contet = log[:split_index], log[split_index + 1:]
            if log_contet[0].isnumeric():
                digit_logs.append(log)
            else:
                letter_logs[log_contet].append(identifier)

        sorted_letter_logs = []
        for log_contet in sorted(letter_logs.keys()):
            for identifier in sorted(letter_logs[log_contet]):
                sorted_letter_logs.append(f'{identifier} {log_contet}')
        return sorted_letter_logs + digit_logs



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
