from types import *
from typing import List

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items) or items[index] is None:
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)



class Solution:
    def numTrees(self, n: int) -> int:
        '''
        >>> (Solution.numTrees(Solution, 3))
        5
        >>> (Solution.numTrees(Solution, 1))
        1
        '''
        cache = {0: 1, 1: 1, 2: 2} # #nodes in tree -> #variation

        def count(n):
            # for a tree with n nodes
            if n <= 2:
                return cache[n]

            total = 0
            for nodes_on_left in range(0, n):
                total += count(nodes_on_left) * count(n - nodes_on_left - 1)
            cache[n] = total
            return total

        return count(n)



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
