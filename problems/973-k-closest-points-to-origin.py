from types import *
from typing import List
from heapq import heappush, heappushpop

class Solution:
    def kClosest(self, points: List[List[int]], K: int) -> List[List[int]]:
        '''
        >>> Solution.kClosest(Solution, [[1,3],[-2,2]], 1)
        [[-2,2]]
        >>> Solution.kClosest(Solution, [[3,3],[5,-1],[-2,4]], 2)
        [[3,3],[-2,4]]
        '''
        # h = []
        # for point in points:
        #     heappush(h, (point[0] ** 2 + point[1] ** 2, point))

        # result = []
        # for point in nsmallest(K, h):
        #     result.append(point[1])
        KSmallest = []
        size = 0
        for point in points:
            distance = point[0] ** 2 + point[1] ** 2
            if size < K:
                heappush(KSmallest, (-distance, point))
                size += 1
            elif KSmallest[0][0] + distance < 0:
                heappushpop(KSmallest, (-distance, point))

        return list(map(lambda x: x[1], KSmallest))


if __name__ == "__main__":
    import doctest
    doctest.testmod()
