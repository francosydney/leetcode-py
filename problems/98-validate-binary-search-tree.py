from types import *
from typing import List
import math

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(items, index = 0):
    if index >= len(items) or items[index] is None:
        return None
    node = TreeNode(items[index])
    node.left = buildTree(items, index * 2 + 1)
    node.right = buildTree(items, index * 2 + 2)
    return node

INDENT_SIZE = 4
def printTree(root, lv = 0):
    if not root:
        return

    printTree(root.right, lv + 1)
    if root.val:
        print(' ' * INDENT_SIZE * lv + str(root.val))
    printTree(root.left, lv + 1)



class Solution:
    def isValidBST(self, root: TreeNode) -> bool:
        '''
        >>> (Solution.isValidBST(Solution, buildTree([5,1,4,None,None,3,6])))
        False
        '''
        def isNodeValidBST(root, low = -math.inf, high=math.inf):
            if not root:
                return True
            if root.val < low or root.val > high:
                return False

            return isNodeValidBST(root.left, low, root.val) and isNodeValidBST(root.right, root.val, high)

        return isNodeValidBST(root)



if __name__ == "__main__":
    import time
    start_time = time.time()

    import doctest
    doctest.testmod()
    print("--- %s seconds ---" % (time.time() - start_time))
