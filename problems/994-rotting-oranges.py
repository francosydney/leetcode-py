from types import *
from typing import List

class Solution:
    def orangesRotting(self, grid: List[List[int]]) -> int:
        '''
        >>> Solution.orangesRotting(Solution, [[2,1,1],[1,1,0],[0,1,1]])
        4
        >>> Solution.orangesRotting(Solution, [[2,1,1],[0,1,1],[1,0,1]])
        -1
        >>> Solution.orangesRotting(Solution, [[0,2]])
        0
        '''
        freshSet = set()
        rottenSet = set()
        time = 0
        affecting = True
        neighborVector = [(-1, 0), (1, 0), (0, -1), (0, 1)]

        for i in range(len(grid)):
            for j in range(len(grid[i])):
                if grid[i][j] is 1:
                    freshSet.add((i, j))
                elif grid[i][j] is 2:
                    rottenSet.add((i, j))

        while affecting:
            affecting = False
            affected = set()
            for rotten in rottenSet:
                for affectable in [(rotten[0] + dx, rotten[1] + dy) for dx, dy in neighborVector]:
                    if affectable in freshSet:
                        affected.add(affectable)
                        freshSet.remove(affectable)
                        affecting = True
            if affecting:
                time += 1
                rottenSet = rottenSet.union(affected)


        return time if len(freshSet) is 0 else -1


if __name__ == "__main__":
    import doctest
    doctest.testmod()
