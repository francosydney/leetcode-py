from types import *
from typing import List
from collections import defaultdict


class Solution:
    def criticalConnections(self, n: int, connections: List[List[int]]) -> List[List[int]]:
        '''
        >>> Solution.criticalConnections(Solution, 4, [[0,1],[1,2],[2,0],[1,3]])
        [[1,3]]
        '''
        adjacencyList = [[] for i in range(n)]
        for x, y in connections:
            adjacencyList[x].append(y)
            adjacencyList[y].append(x)
        visited = [False] * n
        criticals = [ ]


        def dfs(node, rank):
            visited[node] = True
            
            for neighbor in adjacencyList[node]:
                if not visited[neighbor]:
                    dfs(neighbor, rank + 1)


        dfs(connections[0][1], 0)
        
if __name__ == "__main__":
    import doctest
    doctest.testmod()
