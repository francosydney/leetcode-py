from types import *
import re


class Solution:
    def isValid(self, text: str) -> bool:
        '''
        >>> Solution.isValid(Solution, "&amp; is an HTML entity but &ambassador; is not.")
        '& is an HTML entity but &ambassador; is not.'
        >>> Solution.isValid(Solution, "and I quote: &quot;...&quot;")
        'and I quote: \"...\"'
        >>> Solution.isValid(Solution, "x &gt; y &amp;&amp; x &lt; y is always false")
        'x > y && x < y is always false'
        >>> Solution.isValid(Solution, "&amp; is an HTML entity but &ambassador; is not.")
        '& is an HTML entity but &ambassador; is not.'
        '''
        text = re.sub('&quot;', "\"", text)
        text = re.sub('&apos;', "'", text)
        text = re.sub('&amp;', "&", text)
        text = re.sub('&gt;', ">", text)
        text = re.sub('&lt;', "<", text)
        text = re.sub('&frasl;', "/", text)
        return text

if __name__ == "__main__":
    import doctest
    doctest.testmod()
