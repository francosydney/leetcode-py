from types import *


class Solution:
    def numOfWays(self, n: int) -> int:
        '''
        >>> Solution.numOfWays(Solution, 2)
        54
        >>> Solution.numOfWays(Solution, 3)
        246
        >>> Solution.numOfWays(Solution, 7)
        106494
        >>> Solution.numOfWays(Solution, 5000)
        30228214
        '''
        a = 6
        b = 6
        for i in range(n - 1):
            (a, b) = (a * 3 + b * 2, a * 2 + b * 2)
        return (a + b) % (10 ** 9 + 7)

if __name__ == "__main__":
    import doctest
    doctest.testmod()
