from types import *


class Solution:
    def processQueries(self, queries, m):
        '''
        >>> Solution.processQueries(Solution, [3,1,2,1], 5)
        [2,1,2,1] 
        >>> Solution.processQueries(Solution, [7,5,5,8,3], 8)
        [6,5,0,7,5]
        '''
        result = []
        perm = [i + 1 for i in range(m)]

        for query in queries:
            index = perm.index(query)
            result.append(index)
            perm.insert(0, perm.pop(index))
        return result


if __name__ == "__main__":
    import doctest
    doctest.testmod()
