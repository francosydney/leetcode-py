from types import *
from typing import List

class Solution:
    def spiralOrder(self, matrix: List[List[int]]) -> List[int]:
        '''
        >>> Solution.spiralOrder(Solution, [ [ 1, 2, 3 ], [ 4, 5, 6 ], [ 7, 8, 9 ] ])
        [1, 2, 3, 6, 9, 8, 7, 4, 5]
        >>> Solution.spiralOrder(Solution, [ [1, 2, 3, 4], [5, 6, 7, 8], [9,10,11,12] ])
        [1, 2, 3, 4, 8, 12, 11, 10, 9, 5, 6, 7]
        >>> Solution.spiralOrder(Solution, [[1,2,3,4,5,6,7,8,9,10],[11,12,13,14,15,16,17,18,19,20]])
        [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11]
        '''
        m = len(matrix)
        if m < 1:
            return []
        n = len(matrix[0])
        if n < 1:
            return []
        
        trace = []
        tl = (0, 0) # top left
        br = (m - 1, n - 1) # bottom right
        
        while br[0] - tl[0] >= 1 and br[1] - tl[1] >= 1:
            for i in range(tl[1], br[1]):
                trace.append( matrix[tl[0]][i] )
            for i in range(tl[0], br[0]):
                trace.append( matrix[i][br[1]] )
            for i in range(br[1], tl[1], -1):
                trace.append(matrix[br[0]][i])
            for i in range(br[0], tl[0], -1):
                trace.append(matrix[i][tl[1]])

            tl = (tl[0] + 1, tl[1] + 1)
            br = (br[0] - 1, br[1] - 1)
        
        if br[0] >= tl[0] and br[1] >= tl[1]:
            if br[0] > tl[0]:
                for i in range(tl[0], br[0] + 1):
                    trace.append(matrix[i][tl[1]])
            elif br[1] > tl[1]:
                for i in range(tl[1], br[1] + 1):
                    trace.append(matrix[tl[0]][i])
            elif br[0] is tl[0] and br[1] is tl[1]:
                trace.append(matrix[tl[0]][tl[1]])
        
        
        return trace

if __name__ == "__main__":
    import doctest
    doctest.testmod()
