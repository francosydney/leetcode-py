from types import *


class Solution:
    def stringMatching(self, words):
        '''
        >>> Solution.stringMatching(Solution, ["mass","as","hero","superhero"])
        ['as', 'hero']
        >>> Solution.stringMatching(Solution, ["leetcode","et","code"]) 
        ["et","code"]
        >>> Solution.stringMatching(Solution, ["blue","green","bu"])
        []
        '''
        results = []
        words.sort(key=lambda x: len(x))
        i = j = 0
        for i in range(len(words)):
            for j in range(i + 1, len(words)):
                if words[i] in words[j]:
                    results.append(words[i])
                    break
        return results


if __name__ == "__main__":
    import doctest
    doctest.testmod()
