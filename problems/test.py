from types import *
from typing import List
from binarytree import Node as TreeNode

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def makeList(arr):
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next
    return header.next

def printList(l: ListNode):
    arr = []
    p = l
    while p is not None:
        arr.append(str(p.val))
        p = p.next
    print(','.join(arr))

head = p = ListNode(0)
for val in range(1, 11):
    p.next = ListNode(val)
    p = p.next

start = 3
end = 8

head = makeList([1,2,3,4,5,6,7,8]) # -> [1,2,6,5,4,3,7,8]


def reverseBetween(head, start, end):
    left = right = head
    stop = False
    def recurseAndReverse(right, m, n):
        nonlocal stop, left
        if n == 1:
            return
        right = right.next
        if m > 1:
            left = left.next
        
        recurseAndReverse(right, m - 1, n - 1)

        if stop:
            return
        if left == right or right.next == left:
            stop = True
        left.val, right.val = right.val, left.val
        left = left.next
        
    
    recurseAndReverse(right, start, end)
    return head


printList(reverseBetween(head, 3, 6))
