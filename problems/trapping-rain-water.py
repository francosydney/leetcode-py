from types import *
from typing import List
# print(
#     f"Adding to total: \npeak1 h={lastPeakHeight} index={lastPeakIndex}\npeak2 h={thisPeakHeight} index={thisPeakIndex}\n")
# print('Blocks:', ', '.join(map(lambda x: str(x), blocks)))
# print(blocks, waterHeight, sum(
#     map(lambda x: x if x < waterHeight else waterHeight, blocks)))


class Solution:
    def trap(self, height: List[int]) -> int:
        '''
        >>> Solution.trap(Solution, [0,1,0,2,1,0,1,3,2,1,2,1])
        6
        >>> Solution.trap(Solution, [0, 2, 0, 0, 0, 3, 3, 3, 2])
        6
        >>> Solution.trap(Solution, [0, 1, 2, 2, 2, 0, 0, 0, 3, 3, 3, 2])
        6
        >>> Solution.trap(Solution, [2,0,2])
        2
        >>> Solution.trap(Solution, [5,4,1,2])
        1
        >>> Solution.trap(Solution, [5,2,1,2,1,5])
        14
        '''
        total = 0

        highestL = []
        barL = 0
        highestR = []  # reversed highestR
        barR = 0
        
        for h in height:
            barL = max(barL, h)
            highestL.append(barL)
        for h in reversed(height):
            barR = max(barR, h)
            highestR.append(barR)
        highestR.reverse()

        for index, h in enumerate(height):
            total += min(highestL[index], highestR[index]) - h
        return total


# class Solution:
#     def trap(self, height: List[int]) -> int:
#         '''
#         >>> Solution.trap(Solution, [0,1,0,2,1,0,1,3,2,1,2,1])
#         6
#         >>> Solution.trap(Solution, [0, 2, 0, 0, 0, 3, 3, 3, 2])
#         6
#         >>> Solution.trap(Solution, [0, 1, 2, 2, 2, 0, 0, 0, 3, 3, 3, 2])
#         6
#         >>> Solution.trap(Solution, [2,0,2])
#         2
#         >>> Solution.trap(Solution, [5,4,1,2])
#         1
#         >>> Solution.trap(Solution, [5,2,1,2,1,5])
#         14
#         '''
#         l = len(height)
#         total = 0
#         if (len(height) < 3):
#             return 0
#         for h in range(1, max(height) + 1):
#             i = 0
#             while height[i] < h:
#                 i += 1
#             j = l - 1
#             while height[j] < h and j > i:
#                 j -= 1
#             if i is not j:
#                 # room for water at this level
#                 total += sum([1 if height[x] < h else 0 for x in range(i + 1, j)])
#         return total


# class Solution:
#     def trap(self, height: List[int]) -> int:
#         '''
#         >>> Solution.trap(Solution, [0,1,0,2,1,0,1,3,2,1,2,1])
#         6
#         >>> Solution.trap(Solution, [0, 2, 0, 0, 0, 3, 3, 3, 2])
#         6
#         >>> Solution.trap(Solution, [0, 1, 2, 2, 2, 0, 0, 0, 3, 3, 3, 2])
#         6
#         >>> Solution.trap(Solution, [2,0,2])
#         2
#         >>> Solution.trap(Solution, [5,4,1,2])
#         1
#         >>> Solution.trap(Solution, [5,2,1,2,1,5])
#         14
#         '''
#         total = 0
#         blocks = []
#         rising = True
#         lastPeak = None
#         lastH = -1
#         for index, h in enumerate(height):
#             if rising and h < lastH:
#                 thisPeakIndex = index - 1
#                 thisPeakHeight = blocks[-1]

#                 if lastPeak is not None:
#                     lastPeakIndex, lastPeakHeight = lastPeak
                    
#                     waterHeight = min(lastPeakHeight, thisPeakHeight)
#                     subtotal = (thisPeakIndex - lastPeakIndex - 1) * waterHeight - \
#                         sum(map(lambda x: x if x < waterHeight else waterHeight, blocks[:-1]))
                        
#                     total += subtotal
                
#                 lastPeak = (thisPeakIndex, thisPeakHeight)
#                 rising = False
#                 blocks = []

#             elif not rising and h > lastH:
#                 rising = True

#             blocks.append(h)
#             lastH = h

#         if rising and lastPeak:
#             lastPeakIndex, lastPeakHeight = lastPeak
#             waterLevel = min(lastPeakHeight, blocks.pop(len(blocks) - 1))
#             total += len(blocks) * waterLevel -  \
#                 sum(map(lambda x: x if x < waterLevel else waterLevel, blocks))
            

#         return total


if __name__ == "__main__":
    import doctest
    doctest.testmod()
