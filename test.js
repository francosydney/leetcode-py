Function.prototype.before = function(beforeFn) {
    const f = this
    return function() {
        beforeFn.apply(this, arguments)
        return f.apply(this, arguments)
    }
}