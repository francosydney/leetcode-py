class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

def printList(l: ListNode):
    '''
    >>> a = ListNode(1)
    >>> a.next = ListNode(2)
    >>> a.next.next = ListNode(4)
    >>> printList(a)
    124
    '''

    arr = []
    p = l
    while p is not None:
        arr.append(str(p.val))
        p = p.next
    print(''.join(arr))

def makeList(arr):
    '''
    >>> a = makeList([1, 4, 5])
    >>> printList(a)
    145
    '''
    header = p = ListNode(0)
    for item in arr:
        p.next = ListNode(item)
        p = p.next

    return header.next

def palindrom_check(listnode):
    '''
    >>> palindrom_check(makeList([1,2,3,3,2,1]))
    True
    >>> palindrom_check(makeList([]))
    True
    >>> palindrom_check(makeList([1]))
    True
    >>> palindrom_check(makeList([1,2,3,4,3,2,1]))
    True
    >>> palindrom_check(makeList([1,2,3,3,2,1,1]))
    False
    >>> palindrom_check(makeList([1,2,3,4,4]))
    False
    '''
    left = listnode
    
    def palindromCompareWithLeft(node):
        if node is None:
            return True
        
        if not palindromCompareWithLeft(node.next):
            return False
        # after pop/reverse
        nonlocal left
        result = node.val == left.val
        left = left.next
        return result

    return palindromCompareWithLeft(listnode)



if __name__ == '__main__':
    import doctest
    doctest.testmod()
